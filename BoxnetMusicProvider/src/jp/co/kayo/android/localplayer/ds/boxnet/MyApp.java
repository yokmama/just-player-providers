package jp.co.kayo.android.localplayer.ds.boxnet;

import jp.co.kayo.android.localplayer.secret.Keys;

import com.box.androidlib.Box;

import android.app.Application;

public class MyApp extends Application {
    private Box mBox;
    private static MyApp _app;

    
    public MyApp(){
        mBox = Box.getInstance(Keys.API_KEY);
        _app = this;
    }
    
    public static MyApp getInstance(){
        return _app;
    }
    
    public Box getBoxnet(){
        return mBox;
    }
}
