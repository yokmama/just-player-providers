
package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import jp.co.kayo.android.localplayer.consts.MediaConsts.FileType;
import jp.co.kayo.android.localplayer.ds.boxnet.MediaFile.MediaFileType;
import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LocalFileListViewAdapter extends ArrayAdapter<File> {
    LayoutInflater inflator;
    java.text.DateFormat format;
    HashMap<String, String> names = new HashMap<String, String>();

    public LocalFileListViewAdapter(Context context, List<File> objects) {
        super(context, -1, objects);
        format = DateFormat.getMediumDateFormat(context);
        
        names.put("folder", context.getString(R.string.txt_type_folder));
        names.put("audio", context.getString(R.string.txt_type_audio));
        names.put("zip", context.getString(R.string.txt_type_zip));
        inflator = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = inflator.inflate(R.layout.text_list_row, parent,
                false);
            holder = new ViewHolder();
            holder.textView1 = (TextView) convertView.findViewById(R.id.text1);
            holder.textView2 = (TextView) convertView.findViewById(R.id.text2);
            holder.textView3 = (TextView) convertView.findViewById(R.id.text3);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder)convertView.getTag();
        }
        
        File file = getItem(position);
        
        String title = file.getName();
        String type = getType(file);
        long modified = file.lastModified();

        holder.textView1.setText(title);
        holder.textView2.setText(names.get(type));
        holder.textView3.setText(format.format(modified));
        
        return convertView;
    }
    
    class ViewHolder {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        
    }
    
    
    public static String getType(File file) {
        if (file.isDirectory()) {
            return FileType.FOLDER;
        }
        MediaFileType type = MediaFile.getFileType(file.getName());
        if (type != null) {
            if (MediaFile.isAudioFileType(type.fileType)) {
                return FileType.AUDIO;
            //} else if (MediaFile.FILE_TYPE_ZIP == type.fileType) {
            //    return FileType.ZIP;
            } else if (MediaFile.isVideoFileType(type.fileType)) {
                return FileType.VIDEO;
            }
        }
        return null;
    }

}
