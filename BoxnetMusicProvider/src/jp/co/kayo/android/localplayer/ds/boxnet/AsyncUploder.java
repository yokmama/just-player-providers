package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.util.Logger;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.box.androidlib.Box;
import com.box.androidlib.BoxSynchronous;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.FileTransfer.BoxFileUpload;
import com.box.androidlib.ResponseListeners.FileUploadListener;
import com.box.androidlib.ResponseParsers.FolderResponseParser;

public class AsyncUploder extends AsyncTask<Void, Void, String> implements OnCancelListener{
    Context context;
    ProgressDialog dialog;
    File file;
    SharedPreferences prefs;
    boolean cancel = false;
    private int index;
    Handler handler = new Handler();
    
    public AsyncUploder(Context context, File file){
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.file = file;
    }
    
    @Override
    protected void onPostExecute(String result) {
        if(dialog!=null){
            dialog.dismiss();
        }
        if(result!=null){
            showDialog(context, context.getString(R.string.txt_notify), result);
        }
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage(context.getString(R.string.msg_start));
        dialog.setCancelable(true);
        dialog.setOnCancelListener(this);
        dialog.show();
    }

    @Override
    protected void onCancelled() {
        if(dialog!=null){
            dialog.dismiss();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        String result = null;
        // ファイルのアップロード
        BoxnetHelper boxnetHelper = new BoxnetHelper(context);
        try {
            String path = prefs.getString(SystemConsts.CNF_TARGETDIR, "/");
            BoxFileInfo entry = boxnetHelper.findBoxFileInfo(path);
            if(entry!=null && entry.TYPE == 1){
                uploadFile(boxnetHelper, entry, file, new UploadProgress());
            }else{
                result = context.getString(R.string.txt_error_targetdir);
            }
        } catch (Exception e) {
            Logger.e("アップロードエラー", e);
            result = e.getMessage();
            if(result == null){
                result = context.getString(R.string.txt_error_targetdir);
            }
        } finally {
        }
        return result;
    }
    
    private void uploadFile(BoxnetHelper boxnetHelper, BoxFileInfo target, File file, FileUploadListener listener) throws IOException{
        ArrayList<UploadFile> list = new ArrayList<UploadFile>();
        collectFiles(boxnetHelper, target.BOXNETID, file, list);
        Logger.d("file size "+list.size());
        if(list.size()>0){
            final int size = list.size();
            BoxFileUpload uploader = new BoxFileUpload(boxnetHelper.getAuthToken());
            uploader.setListener(listener, handler);
            for(int i=0; i<size; i++){
                index = i;
                final UploadFile ufile = list.get(i);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setMessage(ufile.file.getName()+" ["+index+"/"+size+"]");
                        dialog.setMax((int)(ufile.file.length()/1000));
                    }
                });
                InputStream is = null;
                try{
                    is = new FileInputStream(ufile.file);
                    uploader.execute(Box.UPLOAD_ACTION_OVERWRITE, is, ufile.file.getName(), target.BOXNETID);
                    if(cancel){
                        break;
                    }
                }finally{
                    if(is!=null){
                        is.close();
                    }
                }
            }
        }
    }
    
    private void collectFiles(BoxnetHelper boxnetHelper, long boxid, File file, ArrayList<UploadFile> list) throws IOException {
        if(file.isDirectory()){
            //フォルダ生成
            FolderResponseParser response = BoxSynchronous.getInstance(boxnetHelper.getAuthToken()).createFolder(boxnetHelper.getAuthToken(), boxid, file.getName(), false);
            for(File f : file.listFiles()){
                BoxFolder folder = response.getFolder();
                collectFiles(boxnetHelper, folder.getId(), f, list);
            }
        }
        else{
            list.add(new UploadFile(boxid, file));
        } 
    }
    
    private class UploadFile{
        long boxid;
        File file;
        UploadFile(long boxid, File file){
            this.boxid = boxid;
            this.file = file;
        }
    }
    
    private void showDialog(Context context, String title, String message){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.txt_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                
            }
        });
        alertDialogBuilder.create().show();
    }
    
    private class UploadProgress implements FileUploadListener{
        @Override
        public void onIOException(IOException e) {
        }

        @Override
        public void onProgress(long bytesTransferredCumulative) {
            dialog.setProgress((int)(bytesTransferredCumulative/1000));
        }

        @Override
        public void onComplete(BoxFile boxFile, String status) {
        }

        @Override
        public void onFileNotFoundException(FileNotFoundException e) {
        }

        @Override
        public void onMalformedURLException(MalformedURLException e) {
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        cancel = true;
    }
}
