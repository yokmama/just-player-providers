package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.secret.Keys;
import jp.co.kayo.android.localplayer.util.Logger;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.box.androidlib.Box;
import com.box.androidlib.BoxSynchronous;
import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;
import com.box.androidlib.DAO.User;
import com.box.androidlib.ResponseListeners.GetAccountInfoListener;
import com.box.androidlib.ResponseListeners.GetAccountTreeListener;
import com.box.androidlib.ResponseParsers.AccountTreeResponseParser;
import com.box.androidlib.ResponseParsers.UserResponseParser;

public class MediaScanner extends IntentService {
    boolean isProc = false;
    boolean isStop = false;
    SharedPreferences pref;
    BoxnetHelper boxnetHelper;
    private String[] mediaextensions = null;
    private String[] videoextensions = null;
    private String[] playlistextensions = null;
    FileNameRetriever retriver = new FileNameRetriever();
    String editFolderPattern = null;
    String editFilePattern = null;

    public MediaScanner() {
        super("MediaScanner");
    }

    public MediaScanner(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        if (Build.VERSION.SDK_INT > 10) {
            StrictHelper.registStrictMode();
        }

        try {
            if (!isProc) {
                if (intent != null && intent.getAction() != null) {
                    isProc = true;
                    startScan();
                }
            } else if (intent != null && intent.getAction() != null
                    && intent.getAction().equals("stop")) {
                isStop = true;
            }
        } finally {
            isProc = false;
            stopSelf();
        }
    }

    private String[] getMediaExtensionList() {
        if (mediaextensions == null) {
            String editMediaExt = pref.getString("editMediaExt",
                    SystemConsts.MEDIA_EXT);
            if (editMediaExt.trim().length() > 0) {
                String[] ext = editMediaExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    mediaextensions = ret;
                    return mediaextensions;
                }
            }
            mediaextensions = SystemConsts.MEDIA_EXT.split(",");
        }
        return mediaextensions;
    }

    private String[] getVideoExtensionList() {
        if (videoextensions == null) {
            String editVideoExt = pref.getString("editVideoExt",
                    SystemConsts.VIDEO_EXT);
            if (editVideoExt.trim().length() > 0) {
                String[] ext = editVideoExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    videoextensions = ret;
                    return videoextensions;
                }
            }
            videoextensions = SystemConsts.VIDEO_EXT.split(",");
        }
        return videoextensions;
    }
    
    private String[] getPlaylistExtensionList() {
        if (playlistextensions == null) {
            String editPlaylistExt = pref.getString(SystemConsts.CNF_EDITPLAYLISTEXT,
                    SystemConsts.PLAYLIST_EXT);
            if (editPlaylistExt.trim().length() > 0) {
                String[] ext = editPlaylistExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    playlistextensions = ret;
                    return playlistextensions;
                }
            }
            playlistextensions = SystemConsts.PLAYLIST_EXT.split(",");
        }
        return playlistextensions;
    }

    private String getFolderPattern() {
        if (editFolderPattern == null) {
            editFolderPattern = pref.getString("editFolderPattern", "%a/%A")
                    .trim();
            if (editFolderPattern.length() == 0) {
                editFolderPattern = "%a - %T - %t";
            }
        }
        return editFolderPattern;
    }

    private String getFilePattern() {
        if (editFilePattern == null) {
            editFilePattern = pref.getString("editFilePattern", "%T - %a - %t")
                    .trim();
            if (editFilePattern.length() == 0) {
                editFilePattern = "%a/%A";
            }
        }
        return editFilePattern;
    }

    public static boolean checkExt(String fname, String[] exts) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            for (int i = 0; i < exts.length; i++) {
                if (ext.toLowerCase().equals(exts[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isJPG(String fname) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            if (ext.toLowerCase().equals(".jpg")) {
                return true;
            }
        }
        return false;
    }

    public static long scanAlbumArt(Iterator<? extends BoxFile> filesIterator) {
        long albumArt = 0;
        while (filesIterator.hasNext()) {
            BoxFile boxFile = filesIterator.next();
            String name = boxFile.getFileName();
            Logger.d("scan album fname = "+name);
            if (isJPG(name)) {
                albumArt = boxFile.getId();
                if (name.toLowerCase().indexOf("cover") != -1) {
                    break;
                }
            }
        }
        return albumArt;
    }

    private void notifyOn() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification;
        notification = new Notification(R.drawable.ic_stat_name,
                getString(R.string.prg_name), System.currentTimeMillis());
        notification.flags = Notification.FLAG_NO_CLEAR
                | Notification.FLAG_ONGOING_EVENT;

        Intent intent = new Intent(this, StopActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, Intent.FLAG_ACTIVITY_NEW_TASK);
        notification.setLatestEventInfo(this, getString(R.string.prg_name),
                getString(R.string.msg_start), contentIntent);

        nm.notify(R.string.app_name, notification);
    }

    private void notifyOff() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(R.string.app_name);
    }

    private void startScan() {
        ContentProviderClient client = null;
        try {
            client = getContentResolver()
                    .acquireContentProviderClient(MediaConsts.MEDIA_CONTENT_URI);
            
            isStop = false;

            boxnetHelper = new BoxnetHelper(this);
            boolean isLoggedIn = boxnetHelper.isLoggdIn();
            if (isLoggedIn) {
                int mediaSum = pref.getInt("mediaSum", -1);
                
                final String authToken = boxnetHelper.getAuthToken();

                final Box box = MyApp.getInstance().getBoxnet();
                Logger.d("getAccountInfo ");
                UserResponseParser response = BoxSynchronous.getInstance(box.getApiKey()).getAccountInfo(authToken);
                if(response!=null){
                    User boxUser = response.getUser();
                    String status = response.getStatus();
                    if (status
                            .equals(GetAccountInfoListener.STATUS_GET_ACCOUNT_INFO_OK)
                            && boxUser != null) {
                        int sum = checkSum(0l, 0);
                        if (sum != mediaSum) {
                            Editor editor = pref.edit();
                            editor.putInt("mediaSum", sum);
                            editor.commit();
                            
                            notifyOn();
                            startScan(client, "", 0l, 0);
                        }
                    }
                }
            }
        } catch (IOException e) {
        } finally {
            if(client!=null){
                client.release();
            }
            notifyOff();
        }
    }
    
    private int checkSum(long folderId, int count) throws IOException {
        final Box box = MyApp.getInstance().getBoxnet();
        AccountTreeResponseParser response = BoxSynchronous.getInstance(box.getApiKey()).getAccountTree(boxnetHelper.getAuthToken(), folderId, new String[] { Box.PARAM_ONELEVEL });
        if(response!=null){
            String status = response.getStatus();
            if (status.equals(GetAccountTreeListener.STATUS_LISTING_OK)) {
                BoxFolder boxFolder = response.getFolder();
                return boxFolder.getFoldersInFolder().size();
            }
        }

        return 0;
    }
    
    private String makePath(String rootPath, String name){
        if(rootPath!=null && rootPath.length()>0){
            return rootPath+"/"+name;
        }
        return name;
    }

    private void startScan(ContentProviderClient client, String rootPath, long folderId, int curLevel) {
        final Box box = MyApp.getInstance().getBoxnet();
        List<PlaylistMetaInfo> playlists = new ArrayList<PlaylistMetaInfo>();
        try{
            Logger.d("getAccountTree "+folderId + " "+ curLevel);
            AccountTreeResponseParser response = BoxSynchronous.getInstance(box.getApiKey()).getAccountTree(boxnetHelper.getAuthToken(), folderId, new String[] { Box.PARAM_ONELEVEL });
            if(response!=null){
                BoxFolder boxFolder = response.getFolder();
                String status = response.getStatus();
                if (!status
                        .equals(GetAccountTreeListener.STATUS_LISTING_OK)) {
                    Toast.makeText(getApplicationContext(),
                            "There was an error.", Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                BoxFileInfo boxfile = boxnetHelper.findBoxFileInfo(client, folderId);
                if(boxfile==null){
                    boxnetHelper.registerBoxFileInfo(client, rootPath, boxFolder);
                }else {//if(boxfile.DATE_MODIFIED != boxFolder.getUpdated()){
                    boxnetHelper.updateBoxFileInfo(client, rootPath, boxFolder);
                }
    
                // Folder
                Iterator<? extends BoxFolder> foldersIterator = boxFolder
                        .getFoldersInFolder().iterator();
                while (foldersIterator.hasNext()) {
                    BoxFolder subfolder = foldersIterator.next();
                    Logger.d("folder = "+subfolder.getFolderName());
                    startScan(client, makePath(rootPath,subfolder.getFolderName()), subfolder.getId(), curLevel + 1);
                }
    
                // File
                int track = 0;
                long albumArt = -1;
                Iterator<? extends BoxFile> filesIterator = boxFolder
                        .getFilesInFolder().iterator();
                while (filesIterator.hasNext()) {
                    BoxFile boxFile = filesIterator.next();
                    Logger.d("fname = "+boxFile.getFileName());
                    if (checkExt(boxFile.getFileName(),
                            getMediaExtensionList())) {
                        Logger.d("this is MediaFile:"+boxFile.getFileName());
                        track++;
                        if (albumArt == -1) {
                            albumArt = scanAlbumArt(boxFolder
                                    .getFilesInFolder().iterator());
                        }
                        
                        FileMetaInfo ret = retriver.get(boxnetHelper, boxFile);
                        if (ret != null) {
                            ret.albumArt = albumArt>0?albumArt:-1;
                            ret.track = Integer.toString(track);
                            boxnetHelper.registMetaInfo(ret);
                            BoxFileInfo mediaboxfile = boxnetHelper.findBoxFileInfo(client, boxFile.getId());
                            if(mediaboxfile==null){
                                boxnetHelper.registerBoxFileInfo(client, rootPath, boxFile);
                            }else {
                                boxnetHelper.updateBoxFileInfo(client, rootPath, boxFile);
                            }
                        }
                    }else if(checkExt(boxFile.getFileName(),
                            getPlaylistExtensionList())){
                        Logger.d("this is PlaylistFile:"+boxFile.getFileName());
                        PlaylistMetaInfo ret = new PlaylistMetaInfo();
                        ret.folderId = boxFile.getFolderId();
                        ret.fileId = boxFile.getId();
                        ret.name = boxFile.getFileName();
                        ret.folder = rootPath;
                        ret.filepath = rootPath + "/" + boxFile.getFileName(); 
                        ret.lastModifiedDate = System.currentTimeMillis();
                        playlists.add(ret);
                    }
                }
            }
        }
        catch(Exception e){
            Logger.e("scan error", e);
        }
        finally{
            for(PlaylistMetaInfo meta : playlists){
                boxnetHelper.registPlaylistInfo(client, meta);
            }
            
            getContentResolver().notifyChange(MediaConsts.ALBUM_CONTENT_URI, null);
            getContentResolver().notifyChange(MediaConsts.ARTIST_CONTENT_URI, null);
            getContentResolver().notifyChange(MediaConsts.MEDIA_CONTENT_URI, null);
        }
    }
}
