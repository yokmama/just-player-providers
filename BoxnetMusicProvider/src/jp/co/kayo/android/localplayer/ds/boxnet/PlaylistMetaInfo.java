package jp.co.kayo.android.localplayer.ds.boxnet;

public class PlaylistMetaInfo {
    //%t = プレイリスト
    public long folderId;
    public long fileId;
    public String name;
    public String folder;
    public String filepath;
    public long lastModifiedDate;
}
