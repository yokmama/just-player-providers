package jp.co.kayo.android.localplayer.ds.boxnet;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.SdCardAccessHelper;
import android.annotation.TargetApi;
import android.media.MediaMetadataRetriever;

import com.box.androidlib.DAO.BoxFile;

public class FileNameRetriever {
    
    public String getTitleWithoutExtention(String fname){
        int pos = fname.lastIndexOf('.');
        if(pos!=-1){
            return fname.substring(0, pos-1);
        }
        return null;
    }
    
    public String getExtention(String fname){
        int pos = fname.lastIndexOf('.');
        if(pos!=-1){
            return fname.substring(pos+1);
        }
        return null;
    }

    public FileMetaInfo get(BoxFile boxFile, String folderpattern, String filepattern){
        
        FileMetaInfo ret = new FileMetaInfo();
        ret.folderId = boxFile.getFolderId();
        ret.fileId = boxFile.getId();
        ret.name = boxFile.getFileName();
        retrievMetaInfo(ret, filepattern, ret.name);
        
        return ret;
    }
    
    public String getName(String path) {
        int lastSlash = path.lastIndexOf('/');
        int lastDot = path.lastIndexOf('.');
        if (lastSlash < 0) {
            String name = (lastDot < 0 ? path : path.substring(0, lastDot));
            return name;
        }
        else {
            String name = (lastDot < 0 ? path.substring(lastSlash + 1)
                    : path.substring(lastSlash + 1, lastDot));
            return name;
        }

    }
    
    @TargetApi(14)
    public FileMetaInfo get(BoxnetHelper boxnetHelper, BoxFile boxFile){
        //FileNameから判定もしくは、Fileから取得
        
        MediaMetadataRetriever mmr = null;
        try {
            FileMetaInfo ret = new FileMetaInfo();
            ret.folderId = boxFile.getFolderId();
            ret.fileId = boxFile.getId();
            ret.name = boxFile.getFileName();
            
            File cacheFile = new File(SdCardAccessHelper.cachedMusicDir, 
                    "media" + Integer.toString(Long.toString(ret.fileId).hashCode())+".dat");
            if(cacheFile.exists()){
                Logger.d("cache = "+cacheFile.getAbsolutePath());
                ret.cacheFile = cacheFile.getName();
                mmr = new MediaMetadataRetriever();
                mmr.setDataSource(cacheFile.getPath());
            }
            else{
                Logger.d("getting authpath");
                String url = boxnetHelper.getAuthrizedURL(Long.toString(ret.fileId));
                Logger.d("url = "+url);
                ret.title = ret.name;
                ret.album = boxFile.getFolder().getFolderName();
                ret.artist = boxnetHelper.getContext().getString(R.string.txt_unknown_artist);
                if(ret.name!=null && ret.name.indexOf("-")>0){
                    retrievMetaInfo(ret, "%t", ret.name);
                }
                return ret;
            }
            ret.album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            ret.title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            ret.artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            ret.year = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR);
            ret.track = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER);
            ret.genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
            ret.duration = Funcs.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            
            String tmp_album = null;
            String tmp_title = null;
            String folder_name = boxFile.getFolder()!=null?boxFile.getFolder().getFolderName():null;
            if(folder_name!=null && folder_name.length()>1){
            	tmp_album = folder_name;
            	tmp_title = ret.name;
            }
            
            if(ret.album == null){
            	if(tmp_album!=null){
            		ret.album = tmp_album;
            	}
            	else{
	            	ret.album = boxnetHelper.getContext().getString(R.string.txt_unknown_album);
            	}
            }
            if(ret.title == null){
            	if(tmp_title!=null){
            		ret.title = tmp_title;
            	}
            	else{
	            	ret.title = ret.name;
            	}
            }
            if(ret.artist == null){
            	ret.artist = boxnetHelper.getContext().getString(R.string.txt_unknown_artist);
            }
            return ret;
        } catch (IllegalArgumentException e) {
            Logger.e("不正な引数", e);
        }
        return null;
    }
    
    
    public String rplRegx(String regx, String ptn, String repl){
        regx = regx.replace(" "+ptn+" ", "(.+)");
        regx = regx.replace(" "+ptn, "(.+)");
        regx = regx.replace(ptn+" ", "(.+)");
        regx = regx.replace(ptn, "(.+)");
        return regx;
    }
    
    private String checkStringAB(String a, String b){
        if(b!=null && b.length()>0){
            return b;
        }else{
            return a;
        }
    }
    
    public void retrievMetaInfo(FileMetaInfo metainfo, String pattern, String fname){
        String regx = pattern;
        regx = rplRegx(regx, "%A", "(.+)");
        regx = rplRegx(regx, "%a", "(.+)");
        regx = rplRegx(regx, "%c", "(.+)");
        regx = rplRegx(regx, "%T", "(.+)");
        regx = rplRegx(regx, "%t", "(.+)");
        regx = rplRegx(regx, "%y", "(.+)");
        regx = rplRegx(regx, "%o", "(.+)");
        
        Pattern pat = Pattern.compile(regx);
        Matcher matcher = pat.matcher(fname);
        if(matcher!=null){
            int pos = 0;
            if (matcher.find()) {
                for(int i=1; i<=matcher.groupCount(); i++){
                    String str = matcher.group(i);
                    int p = pattern.indexOf("%", pos);
                    String sub = pattern.substring(p);
                    if(sub.startsWith("%A")){
                        metainfo.album = checkStringAB(metainfo.album, str.trim());
                    }
                    else if(sub.startsWith("%a")){
                        metainfo.artist = checkStringAB(metainfo.artist, str.trim());
                    }
                    else if(sub.startsWith("%c")){
                        metainfo.comment = checkStringAB(metainfo.comment, str.trim());
                    }
                    else if(sub.startsWith("%T")){
                        metainfo.track = checkStringAB(metainfo.track, str.trim());
                    }
                    else if(sub.startsWith("%t")){
                        int ext = str.lastIndexOf(".");
                        if(ext>0){
                            metainfo.title = checkStringAB(metainfo.title, str.substring(0, ext).trim());
                        }
                        else{
                            metainfo.title = checkStringAB(metainfo.title, str.trim());
                        }
                    }
                    else if(sub.startsWith("%y")){
                        metainfo.year = checkStringAB(metainfo.year, str.trim());
                    }
                    else if(sub.startsWith("%o")){
                        metainfo.desc = checkStringAB(metainfo.desc, str.trim());
                    }
                    pos = p+1;
                }
            }
        }
        if(metainfo.title == null || metainfo.title.length() == 0){
            metainfo.title = metainfo.name;
        }
    }
    
}
