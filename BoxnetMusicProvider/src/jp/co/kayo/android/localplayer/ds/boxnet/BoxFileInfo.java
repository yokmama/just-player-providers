package jp.co.kayo.android.localplayer.ds.boxnet;


public class BoxFileInfo {
    public long _ID;
    public long PARENTID;
    public long BOXNETID;
    public String TITLE;
    public int TYPE;
    public String PATH;
    public long SIZE;
    public long DATE_MODIFIED;
}
