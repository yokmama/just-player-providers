package jp.co.kayo.android.localplayer.ds.boxnet;

import com.box.androidlib.DAO.BoxFile;
import com.box.androidlib.DAO.BoxFolder;

public class TreeListItem {
    public static final int TYPE_FILE = 1;
    public static final int TYPE_FOLDER = 2;
    public int type;
    public long id;
    public String name;
    public BoxFile file;
    @SuppressWarnings("unused")
    public BoxFolder folder;
    public long updated;
}
