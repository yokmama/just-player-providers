package jp.co.kayo.android.localplayer.ds.boxnet;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.FileType;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RemoteFileBrowser extends FragmentActivity implements LoaderCallbacks<Cursor>, OnItemClickListener {

    private SharedPreferences prefs;
    private RemoteFileListViewAdapter adapter;
    private ListView listView;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_remote_tree);
        
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        
        listView = (ListView)findViewById(android.R.id.list);
        listView.setOnItemClickListener(this);
        
        getSupportLoaderManager().initLoader(R.layout.main_remote_tree, null, this);
    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        Cursor cursor = (Cursor) adapter.getItem(position);
        if (cursor != null) {
            String type = cursor.getString(cursor
                    .getColumnIndex(FileMedia.TYPE));
            String data = cursor.getString(cursor
                    .getColumnIndex(FileMedia.DATA));
            if (type != null && type.equals(FileType.FOLDER)) {
                Bundle bundle = new Bundle();
                bundle.putString("path", data);
                getSupportLoaderManager().restartLoader(R.layout.main_remote_tree, bundle, this);
            }
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle args) {
        if (adapter == null) {
            adapter = new RemoteFileListViewAdapter(this, null);
            listView.setAdapter(adapter);
        } else {
            Cursor cur = adapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }
        
        String selection = null;
        String[] selectionArgs = null;
        if (args != null) {
            String path = args.getString("path");
            if (path != null) {
                selection = FileMedia.DATA + " = ?";
                selectionArgs = new String[] { path };
            }
        }

        return new CursorLoader(this, MediaConsts.FOLDER_CONTENT_URI,
                null, selection, selectionArgs, FileMedia.TITLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor data) {
        try {
            if (adapter != null) {
                Cursor cur = adapter.swapCursor(data);
            }
        } finally {
        }
        
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        
    }
}
