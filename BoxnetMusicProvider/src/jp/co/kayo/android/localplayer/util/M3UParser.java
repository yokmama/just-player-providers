package jp.co.kayo.android.localplayer.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.boxnet.BoxnetHelper;
import jp.co.kayo.android.localplayer.ds.boxnet.MediaFile;
import jp.co.kayo.android.localplayer.ds.boxnet.PlaylistMetaInfo;
import android.os.Build;
import android.os.RemoteException;
import android.util.Log;

public class M3UParser {
    private static final String TAG = "JUSTPLAYER.PLAYLIST";
    private BoxnetHelper mBoxnetHelper;

    public static class PlaylistEntry {
        public String filepath;
        public String fname;
        public long bestmatchid;
        public int bestmatchlevel;
    }

    public M3UParser(BoxnetHelper helper) {
        mBoxnetHelper = helper;
    }

    private ArrayList<PlaylistEntry> mPlaylistEntries = new ArrayList<PlaylistEntry>();

    public ArrayList<PlaylistEntry> getPlaylistEntries() {
        return mPlaylistEntries;
    }

    private void cachePlaylistEntry(String line, String playListDirectory) {
        PlaylistEntry entry = new PlaylistEntry();
        entry.fname = new File(line).getName();
        entry.filepath = playListDirectory + "/" + entry.fname;
        mPlaylistEntries.add(entry);
    }

    private void processM3uPlayList(long fileId, String playListDirectory) {
        BufferedReader reader = null;
        try {
            String uri = mBoxnetHelper.getAuthrizedURL(Long.toString(fileId));
            if (uri != null) {
                URL url = new URL(uri);
                reader = new BufferedReader(
                        new InputStreamReader(url.openStream()), 8192);
                String line = reader.readLine();
                mPlaylistEntries.clear();
                while (line != null) {
                    // ignore comment lines, which begin with '#'
                    if (line.length() > 0 && line.charAt(0) != '#') {
                        cachePlaylistEntry(line, playListDirectory);
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processM3uPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException in MediaScanner.processM3uPlayList()", e);
            }
        }
    }

    private void processPlsPlayList(long fileId, String playListDirectory) {
        BufferedReader reader = null;
        try {
            String uri = mBoxnetHelper.getAuthrizedURL(Long.toString(fileId));
            if (uri != null) {
                URL url = new URL(mBoxnetHelper.getAuthrizedURL(uri));

                reader = new BufferedReader(
                        new InputStreamReader(url.openStream()), 8192);
                mPlaylistEntries.clear();
                String line = reader.readLine();
                while (line != null) {
                    // ignore comment lines, which begin with '#'
                    if (line.startsWith("File")) {
                        int equals = line.indexOf('=');
                        if (equals > 0) {
                            cachePlaylistEntry(line.substring(equals + 1), playListDirectory);
                        }
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processPlsPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException in MediaScanner.processPlsPlayList()", e);
            }
        }
    }

    private void processTxtPlayList(long fileId, String playListDirectory) {
        BufferedReader reader = null;
        HttpURLConnection urlConnection = null;
        try {
            String uri = mBoxnetHelper.getAuthrizedURL(Long.toString(fileId));
            Logger.d("text uri=" + uri);
            if (uri != null) {
                urlConnection = (HttpURLConnection) new URL(uri).openConnection();
                if (Build.VERSION.SDK != null
                        && Build.VERSION.SDK_INT > 13) {
                    urlConnection.setRequestProperty("Connection", "close");
                }
                reader = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()), 4);
                mPlaylistEntries.clear();
                String line = reader.readLine();
                while (line != null) {
                    Logger.d("line ="+line);
                    // ignore comment lines, which begin with '#'
                    if (line.length() > 0 && line.charAt(0) != '#' && line.indexOf(".") > 0) {
                        cachePlaylistEntry(line, playListDirectory);
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processTxtPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    public String getPlaylistName(String path) {
        int lastSlash = path.lastIndexOf('/');
        int lastDot = path.lastIndexOf('.');
        if (lastSlash < 0) {
            String name = (lastDot < 0 ? path : path.substring(0, lastDot));
            return name;
        }
        else {
            String name = (lastDot < 0 ? path.substring(lastSlash + 1)
                    : path.substring(lastSlash + 1, lastDot));
            return name;
        }

    }

    public void processPlayList(PlaylistMetaInfo entry) throws RemoteException {
        // make sure we have a name
        int lastDot = entry.name.lastIndexOf('.');
        entry.name = (lastDot < 0 ? entry.name : entry.name.substring(0, lastDot));

        String playListDirectory = entry.folder;
        MediaFile.MediaFileType mediaFileType = MediaFile.getFileType(entry.filepath);
        int fileType = (mediaFileType == null ? 0 : mediaFileType.fileType);

        if (fileType == MediaFile.FILE_TYPE_M3U) {
            processM3uPlayList(entry.fileId, playListDirectory);
        } else if (fileType == MediaFile.FILE_TYPE_PLS) {
            processPlsPlayList(entry.fileId, playListDirectory);
        } else if (fileType == MediaFile.FILE_TYPE_TEXT) {
            processTxtPlayList(entry.fileId, playListDirectory);
        }
    }
}
