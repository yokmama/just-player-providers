
package jp.co.kayo.android.localplayer.ds.dropbox;

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;

public class MediaScanner extends Service {
    boolean isProc = false;
    boolean isStop = false;
    SharedPreferences pref;
    DropboxHelper dropboxHelper;
    private String[] mediaextensions = null;
    private String[] videoextensions = null;
    private String[] playlistextensions = null;
    FileNameRetriever retriver = new FileNameRetriever();

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (Build.VERSION.SDK_INT > 10) {
            StrictHelper.registStrictMode();
        }

        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals("sync")) {
                if (!isProc) {
                    startScan();
                }
            }
            else if (intent.getAction().equals("stop")) {
                isStop = true;
            }
        }

        return Service.START_NOT_STICKY;
    }

    private String[] getMediaExtensionList() {
        if (mediaextensions == null) {
            /*String editMediaExt = pref.getString(SystemConsts.CNF_EDITMEDIAEXT,
                    SystemConsts.MEDIA_EXT);
            if (editMediaExt.trim().length() > 0) {
                String[] ext = editMediaExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    mediaextensions = ret;
                    return mediaextensions;
                }
            }*/
            mediaextensions = SystemConsts.MEDIA_EXT.split(",");
        }
        return mediaextensions;
    }

    private String[] getVideoExtensionList() {
        if (videoextensions == null) {
            /*String editVideoExt = pref.getString(SystemConsts.CNF_EDITVIDEOEXT,
                    SystemConsts.VIDEO_EXT);
            if (editVideoExt.trim().length() > 0) {
                String[] ext = editVideoExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    videoextensions = ret;
                    return videoextensions;
                }
            }*/
            videoextensions = SystemConsts.VIDEO_EXT.split(",");
        }
        return videoextensions;
    }
    
    private String[] getPlaylistExtensionList() {
        if (playlistextensions == null) {
            /*String editPlaylistExt = pref.getString(SystemConsts.CNF_EDITPLAYLISTEXT,
                    SystemConsts.PLAYLIST_EXT);
            if (editPlaylistExt.trim().length() > 0) {
                String[] ext = editPlaylistExt.split(",");
                if (ext != null && ext.length > 0) {
                    String[] ret = new String[ext.length];
                    for (int i = 0; i < ext.length; i++) {
                        ret[i] = ext[i].trim().toLowerCase();
                    }
                    playlistextensions = ret;
                    return playlistextensions;
                }
            }*/
            playlistextensions = SystemConsts.PLAYLIST_EXT.split(",");
        }
        return playlistextensions;
    }

    public static boolean checkExt(String fname, String[] exts) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            for (int i = 0; i < exts.length; i++) {
                if (ext.toLowerCase().equals(exts[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isJPG(String fname) {
        int s1 = fname.lastIndexOf('.');
        if (s1 > 0) {
            String ext = fname.substring(s1);
            if (ext.toLowerCase().equals(".jpg")) {
                return true;
            }
        }
        return false;
    }

    public static String scanAlbumArt(Entry entry) {
        String albumArt = "";
        if (entry.contents != null) {
            for (Entry child : entry.contents) {
                if (child.isDeleted != true && child.isDir != true) {
                    String name = child.fileName();
                    if (isJPG(name)) {
                        albumArt = child.path;
                        if (albumArt.toLowerCase().indexOf("cover") != -1) {
                            break;
                        }
                    }
                }
            }
        }
        return albumArt;
    }

    private void notifyOn() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification;
        notification = new Notification(
                R.drawable.ic_stat_name,
                getString(R.string.prg_name),
                System.currentTimeMillis());
        notification.flags = Notification.FLAG_NO_CLEAR
                | Notification.FLAG_ONGOING_EVENT;

        Intent intent = new Intent(this, StopActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, Intent.FLAG_ACTIVITY_NEW_TASK);
        notification.setLatestEventInfo(this,
                getString(R.string.prg_name),
                getString(R.string.msg_start),
                contentIntent);

        nm.notify(R.string.app_name, notification);
    }

    private void notifyOff() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(R.string.app_name);
    }

    private class Counter {
        int count;
        int maxCount;

        boolean isOver() {
            return maxCount > 0 && count > maxCount;
        }

        void inc() {
            count++;
        }
    }

    private void startScan() {
        // Entry entry = null;
        isStop = false;

        String path = pref.getString(SystemConsts.CNF_TARGETDIR, "/").trim();
        if (path.length() == 0) {
            path = "/";
        }

        final String runPath = path;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    isProc = true;
                    runTask(runPath);
                } finally {
                    isProc = false;
                    stopSelf();
                }
            }
        });

        t.start();
    }

    private void runTask(String path) {
        Counter counter = new Counter();
        counter.maxCount = Funcs.parseInt(pref.getString(SystemConsts.CNF_SCANCOUNT, "5000"));
        counter.count = 0;
        dropboxHelper = new DropboxHelper(getApplicationContext());
        try {
            boolean isLoggedIn = dropboxHelper.isLoggdIn();
            if (isLoggedIn) {
                int mediaSum = pref.getInt("mediaSum", -1);

                try {
                    int sum = checkSum(path, 0);
                    Logger.d("sum = " + sum + "/" + mediaSum);
                    if (sum != mediaSum) {
                        Editor editor = pref.edit();
                        editor.putInt("mediaSum", sum);
                        editor.commit();

                        notifyOn();
                        if (!startScan(path, counter)) {
                            Toast.makeText(this, getString(R.string.txt_limit_over),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                } finally {
                    notifyOff();
                }
            }
        } finally {

        }
    }

    private int checkSum(String path, int count) {
        try {
            Entry entry = dropboxHelper.metadata(path, 0, null, true, null);
            if (entry != null && !entry.isDeleted && entry.isDir && entry.contents != null) {
                count += entry.contents.size();
            }
        } catch (DropboxException e) {
            Logger.e("metadata", e);
            // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return count;
    }

    private boolean startScan(String directory, Counter counter) {
        Logger.d("path=" + directory + " " + counter.count + "/" + counter.maxCount);
        ContentProviderClient client = null;
        try {
            client = getContentResolver()
                    .acquireContentProviderClient(MediaConsts.MEDIA_CONTENT_URI);
            Entry entry = dropboxHelper.metadata(directory, 0, null, true, null);

            if (entry.isDir && entry.isDeleted != true) {
                mediaScan(client, entry, counter);
            }

        } catch (DropboxException e) {
            Logger.e("metadata", e);
            // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        } finally{
            if(client!=null){
                client.release();
            }
        }
        return true;
    }

    private boolean mediaScan(ContentProviderClient client, Entry entry, Counter counter) {
        if (counter.isOver()) {
            return false;
        }
        Logger.d("*********** startScan : "+ entry.path + " ***");
        if (entry.contents != null) {
            String albumArt = null;
            List<PlaylistMetaInfo> playlists = new ArrayList<PlaylistMetaInfo>();
            try{
                for (Entry child : entry.contents) {
                    if (isStop) {
                        break;
                    }
                    String name = child.fileName();
                    if (name.startsWith(".") != true && !child.isDeleted) {
                        if (child.isDir) {
                            counter.inc();
                            Entry childentry = dropboxHelper.metadata(child.path, 0, null, true, null);
                            if (!mediaScan(client, childentry, counter)) {
                                return false;
                            }
                        }
                        else {
                            // media file
                            if (checkExt(name, getMediaExtensionList())) {
                                if (albumArt == null) {
                                    albumArt = scanAlbumArt(entry);
                                }
                                FileMetaInfo ret = retriver.get(dropboxHelper, child, false);
                                ret.albumArt = albumArt;
    
                                dropboxHelper.registMetaInfo(client, ret);
                                Logger.d("pass Media Count="+ getCount(this, MediaConsts.MEDIA_CONTENT_URI, null, null));
                            }
                            else if (checkExt(name, getVideoExtensionList())) {
                                VideoMetaInfo ret = new VideoMetaInfo();
                                ret.path = child.parentPath();
                                ret.filepath = child.path;
                                ret.name = child.fileName();
                                ret.title = retriver.getTitleWithoutExtention(ret.name);
                                ret.mime = "video/" + retriver.getExtention(ret.name);// child.mimeType;
                                ret.size = child.bytes;
                                dropboxHelper.registVideoInfo(client, ret);
                            }
                            else if (checkExt(name, getPlaylistExtensionList())) {
                                PlaylistMetaInfo ret = new PlaylistMetaInfo();
                                ret.path = child.parentPath();
                                ret.filepath = child.path;
                                ret.name = child.fileName();
                                ret.lastModifiedDate = System.currentTimeMillis();
                                playlists.add(ret);
                            }
                        }
                    }
                }
            } catch (DropboxException e) {
                e.printStackTrace();
            }
            finally{
                //Playlist登録
                for(PlaylistMetaInfo meta : playlists){
                    dropboxHelper.registPlaylistInfo(client, meta);
                }
                
                Logger.d("Media Count="+ getCount(this, MediaConsts.MEDIA_CONTENT_URI, null, null));
                Logger.d("Playlist Count="+ getCount(this, MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, null, null));
                
                getContentResolver().notifyChange(MediaConsts.ALBUM_CONTENT_URI, null);
                getContentResolver().notifyChange(MediaConsts.ARTIST_CONTENT_URI, null);
                getContentResolver().notifyChange(MediaConsts.MEDIA_CONTENT_URI, null);
            }
        }
        return true;
    }
    
    public static int getCount(Context context, Uri uri, String where, String[] whereArgs){
        Cursor cursor = null;
        try{
            cursor = context.getContentResolver().query(uri, new String[]{"count(*)"}, where, whereArgs, null);
            if(cursor!=null && cursor.moveToFirst()){
                return cursor.getInt(0);
            }
            return 0;
        }finally{
            if(cursor!=null){
                cursor.close();
            }
        }
    }
}
