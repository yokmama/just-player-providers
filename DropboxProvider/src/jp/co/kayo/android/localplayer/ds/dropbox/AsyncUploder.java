package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.consts.SystemConsts;

import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;

public class AsyncUploder extends AsyncTask<Void, Void, String> implements OnCancelListener{
    Context context;
    ProgressDialog dialog;
    File file;
    SharedPreferences prefs;
    boolean cancel = false;
    private int index;
    Handler handler = new Handler();
    
    public AsyncUploder(Context context, File file){
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.file = file;
    }
    
    @Override
    protected void onPostExecute(String result) {
        if(dialog!=null){
            dialog.dismiss();
        }
        if(result!=null){
            showDialog(context, context.getString(R.string.txt_notify), result);
        }
    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setMessage(context.getString(R.string.msg_start));
        dialog.setCancelable(true);
        dialog.setOnCancelListener(this);
        dialog.show();
    }

    @Override
    protected void onCancelled() {
        if(dialog!=null){
            dialog.dismiss();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        String result = null;
        // ファイルのアップロード
        DropboxHelper dropboxHelper = new DropboxHelper(context);
        try {
            String path = prefs.getString(SystemConsts.CNF_TARGETDIR, "/");
            Entry entry = dropboxHelper.metadata(path, 0, null, true, null);
            if(entry!=null && !entry.isDeleted && entry.isDir){
                uploadFile(dropboxHelper, path, file, new UploadProgress());
            }else{
                result = context.getString(R.string.txt_error_targetdir);
            }
        } catch (Exception e) {
            Logger.e("アップロードエラー", e);
            result = e.getMessage();
            if(result == null){
                result = context.getString(R.string.txt_error_targetdir);
            }
        } finally {
        }
        return result;
    }
    
    private void uploadFile(DropboxHelper dropboxHelper, String path, File file, ProgressListener listener) throws FileNotFoundException, DropboxException{
        ArrayList<UploadFile> list = new ArrayList<UploadFile>();
        collectFiles(dropboxHelper, path, file, list);
        Logger.d("file size "+list.size());
        if(list.size()>0){
            final int size = list.size();
            for(int i=0; i<size; i++){
                index = i;
                final UploadFile ufile = list.get(i);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setMessage(ufile.file.getName()+" ["+index+"/"+size+"]");
                        dialog.setMax((int)(ufile.file.length()/1000));
                    }
                });
                dropboxHelper.uploadFile(ufile.path, ufile.file, listener);
                if(cancel){
                    break;
                }
            }
        }
    }
    
    private void collectFiles(DropboxHelper dropboxHelper, String path, File file, ArrayList<UploadFile> list) throws DropboxException{
        if(file.isDirectory()){
            //フォルダ生成
            String newdir = dropboxHelper.createFolder(path, file);
            for(File f : file.listFiles()){
                collectFiles(dropboxHelper, newdir, f, list);
            }
        }
        else{
            list.add(new UploadFile(path, file));
        } 
    }
    
    private class UploadFile{
        String path;
        File file;
        UploadFile(String path, File file){
            this.path = path;
            this.file = file;
        }
    }
    
    private void showDialog(Context context, String title, String message){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.txt_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                
            }
        });
        alertDialogBuilder.create().show();
    }
    
    private class UploadProgress extends ProgressListener{
        @Override
        public void onProgress(long bytes, long total) {
            dialog.setProgress((int)(bytes/1000));
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        cancel = true;
    }
}
