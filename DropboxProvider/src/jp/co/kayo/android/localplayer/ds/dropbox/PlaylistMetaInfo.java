package jp.co.kayo.android.localplayer.ds.dropbox;

public class PlaylistMetaInfo {
    //%t = プレイリスト
    public String path;
    public String filepath;
    public String name;
    public long lastModifiedDate;
}
