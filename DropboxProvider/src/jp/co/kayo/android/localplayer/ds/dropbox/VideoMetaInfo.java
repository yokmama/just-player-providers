package jp.co.kayo.android.localplayer.ds.dropbox;

public class VideoMetaInfo {
    //%t = ビデオ
    public String title;
    public long size;
    
    public String mime;
    public String path;
    public String filepath;
    public String name;
}
