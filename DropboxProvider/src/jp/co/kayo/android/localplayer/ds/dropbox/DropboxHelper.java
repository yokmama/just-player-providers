package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.consts.MediaConsts.VideoMedia;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.secret.Keys;
import jp.co.kayo.android.localplayer.util.M3UParser;
import jp.co.kayo.android.localplayer.util.M3UParser.PlaylistEntry;
import android.content.ContentProviderClient;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Account;
import com.dropbox.client2.DropboxAPI.DropboxLink;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.android.AuthActivity;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;

public class DropboxHelper {
    final static private AccessType ACCESS_TYPE = AccessType.DROPBOX;

    final static private String ACCOUNT_PREFS_NAME = "prefs";
    final static private String ACCESS_KEY_NAME = "ACCESS_KEY";
    final static private String ACCESS_SECRET_NAME = "ACCESS_SECRET";

    public static final String AUTHORITY = "jp.co.kayo.android.localplayer";
    public static final String CONTENT_AUTHORITY_SLASH = "content://" + AUTHORITY + "/";
    public static final Uri MEDIA_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY_SLASH + "audio/media");
    public static final Uri ALBUM_CONTENT_URI = Uri.parse(CONTENT_AUTHORITY_SLASH + "audio/albums");
    public static final Uri ARTIST_CONTENT_URI = Uri
            .parse(CONTENT_AUTHORITY_SLASH + "audio/artist");

    private Context context;
    private DropboxAPI<AndroidAuthSession> mApi;

    public DropboxHelper(Context context) {
        this.context = context;
        AndroidAuthSession session = buildSession();
        mApi = new DropboxAPI<AndroidAuthSession>(session);
    }

    /*
    public ContentProviderClient getDropboxDatabaseClient() {
        if (mDatabaseClient == null) {
            mDatabaseClient = context.getContentResolver().acquireContentProviderClient(Uri.parse(MediaConsts.DROPBOX_CONTENT_AUTHORITY_SLASH + "audio/*"));
        }
        return mDatabaseClient;
    }*/


    public Context getContext() {
        return context;
    }

    public boolean isLoggdIn() {
        return mApi.getSession().isLinked();
    }

    public void startAuthentication() {
        mApi.getSession().startAuthentication(context);
    }

    public AndroidAuthSession getSession() {
        return mApi.getSession();
    }

    public DropboxLink media(String path) throws DropboxException {
        return mApi.media(path, false);
    }

    public void storeKeys(String key, String secret) {
        SharedPreferences prefs = context.getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
        Editor edit = prefs.edit();
        edit.putString(ACCESS_KEY_NAME, key);
        edit.putString(ACCESS_SECRET_NAME, secret);
        edit.commit();
    }

    public void clearKeys() {
        SharedPreferences prefs = context.getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
        Editor edit = prefs.edit();
        edit.clear();
        edit.commit();
    }

    private boolean checkAppKeySetup() {
        // Check to make sure that we have a valid app key
        if (Keys.APP_KEY.startsWith("CHANGE") || Keys.APP_SECRET.startsWith("CHANGE")) {
            showToast("You must apply for an app key and secret from developers.dropbox.com, and add them to the DBRoulette ap before trying it.");
            return false;
        }

        // Check if the app has set up its manifest properly.
        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        String scheme = "db-" + Keys.APP_KEY;
        String uri = scheme + "://" + AuthActivity.AUTH_VERSION + "/test";
        testIntent.setData(Uri.parse(uri));
        PackageManager pm = context.getPackageManager();
        if (0 == pm.queryIntentActivities(testIntent, 0).size()) {
            showToast("URL scheme in your app's "
                    + "manifest is not set up correctly. You should have a "
                    + "com.dropbox.client2.android.AuthActivity with the " + "scheme: " + scheme);
            return false;
        }

        return true;
    }

    private void showToast(String msg) {
        Logger.d(msg);
        // Toast error = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        // error.show();
    }

    private AndroidAuthSession buildSession() {
        AppKeyPair appKeyPair = new AppKeyPair(Keys.APP_KEY, Keys.APP_SECRET);
        AndroidAuthSession session;

        String[] stored = getKeys();
        if (stored != null) {
            AccessTokenPair accessToken = new AccessTokenPair(stored[0], stored[1]);
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE, accessToken);
        } else {
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE);
        }

        return session;
    }

    private String[] getKeys() {
        SharedPreferences prefs = context.getSharedPreferences(ACCOUNT_PREFS_NAME, 0);
        String key = prefs.getString(ACCESS_KEY_NAME, null);
        String secret = prefs.getString(ACCESS_SECRET_NAME, null);
        if (key != null && secret != null) {
            String[] ret = new String[2];
            ret[0] = key;
            ret[1] = secret;
            return ret;
        } else {
            return null;
        }
    }

    public Entry metadata(String directory, int limit, String s1, boolean b,
            String s2) throws DropboxException {
        return mApi.metadata(directory, limit, s1, b, s2);
    }

    public Account getAccountInfo() throws DropboxException {
        return mApi.accountInfo();
    }

    public Uri registMetaInfo(ContentProviderClient client, FileMetaInfo metainf) {
        boolean ret;
        boolean hasRegist = isRegisterdSong(client, (metainf.path + metainf.name).hashCode());
        if (!hasRegist) {
            Logger.d("metainf.path=" + metainf.path);
            Logger.d("metainf.name=" + metainf.name);
            Logger.d("metainf.title=" + metainf.title);
            Logger.d("metainf.album=" + metainf.album);
            Logger.d("metainf.artist=" + metainf.artist);

            // アルバム情報の登録
            ret = isRegisterdAlbum(client, Funcs.createHashId(metainf.path));
            if (!ret) {
                long albumId = insertAlbum(client, metainf);
            }
            // アーティスト情報の登録
            if (metainf.artist != null && metainf.artist.length() > 0) {
                ret = isRegisterdArtist(client, metainf.artist.hashCode());
                if (!ret) {
                    long artistId = insertArtist(client, metainf);
                }
            }
            // 楽曲情報の登録
            long mediaId = insertSong(client, metainf);
        }
        return null;
    }

    public Uri registVideoInfo(ContentProviderClient client, VideoMetaInfo metainf) {
        boolean ret;
        // 楽曲情報の登録
        ret = isRegisterdVideo(client, Funcs.createHashId(metainf.path+metainf.name));
        if (!ret) {
            long videoId = insertVideo(client, metainf);
        }
        return null;
    }

    public Uri registPlaylistInfo(ContentProviderClient client, PlaylistMetaInfo metainf) {
        long playlsitId;
        // 楽曲情報の登録
        playlsitId = findRegisterdPlaylist(client, metainf.filepath);
        if (playlsitId == -1) {
            long playlistid = insertPlaylist(client, metainf);
        } else {
            long playlistid = updatePlaylist(client, playlsitId, metainf);
        }
        return null;
    }

    public boolean isRegisterdAlbum(ContentProviderClient client, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(
                    MediaConsts.ALBUM_CONTENT_URI,
                    new String[] {
                            BaseColumns._ID, AudioAlbum.ALBUM
                    },
                    AudioAlbum.ALBUM_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return false;
    }

    public boolean isRegisterdArtist(ContentProviderClient client, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(
                    MediaConsts.ARTIST_CONTENT_URI,
                    new String[] {
                            BaseColumns._ID, AudioArtist.ARTIST
                    },
                    AudioArtist.ARTIST_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return false;
    }

    public boolean isRegisterdSong(ContentProviderClient client, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(MediaConsts.MEDIA_CONTENT_URI,
                    new String[] {
                        AudioMedia.MEDIA_KEY
                    },
                    AudioMedia.MEDIA_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return false;
    }

    public boolean isRegisterdVideo(ContentProviderClient client, long id) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(
                    MediaConsts.VIDEO_CONTENT_URI,
                    new String[] {
                        AudioMedia.MEDIA_KEY
                    },
                    VideoMedia.MEDIA_KEY + " = ?",
                    new String[] {
                        Long.toString(id)
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return false;
    }

    public long findRegisterdPlaylist(ContentProviderClient client, String path) {
        // 有無をチェック
        Cursor cur = null;
        try {
            cur = client.query(
                    MediaConsts.PLAYLIST_CONTENT_URI,
                    new String[] {AudioPlaylist._ID},
                    AudioPlaylist.DATA + " = ?",
                    new String[] {
                            path
                    }, null);
            if (cur != null && cur.moveToFirst()) {
                return cur.getLong(0);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    public long insertAlbum(ContentProviderClient client, FileMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();
            values.put(AudioAlbum.ALBUM_KEY, Funcs.createHashId(metainf.path));
            if (metainf.album != null && metainf.album.length() > 0) {
                values.put(AudioAlbum.ALBUM, metainf.album);
            }
            else {
                values.put(AudioAlbum.ALBUM, "unknown");
            }
            values.put(AudioAlbum.ALBUM_ART, metainf.albumArt);
            if (metainf.artist != null && metainf.artist.length() > 0) {
                values.put(AudioAlbum.ARTIST, metainf.artist);
            }
            values.put(AudioAlbum.FIRST_YEAR, "");
            values.put(AudioAlbum.LAST_YEAR, "");
            values.put(AudioAlbum.NUMBER_OF_SONGS, "0");
            values.put(AudioAlbum.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioAlbum.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.ALBUM_DEL_FLG, 0);

            Uri uri = client.insert(MediaConsts.ALBUM_CONTENT_URI, values);
            return ContentUris.parseId(uri);
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    public long insertArtist(ContentProviderClient client, FileMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            ContentValues values = new ContentValues();
            values.put(AudioArtist.ARTIST_KEY, metainf.artist.hashCode());
            values.put(AudioArtist.ARTIST, metainf.artist);
            values.put(AudioArtist.NUMBER_OF_ALBUMS, "0");
            values.put(AudioArtist.NUMBER_OF_TRACKS, "0");
            values.put(AudioArtist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioArtist.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.ARTIST_DEL_FLG, 0);

            Uri uri = client.insert(MediaConsts.ARTIST_CONTENT_URI, values);
            return ContentUris.parseId(uri);
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    public long insertSong(ContentProviderClient client, FileMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();
        try {
            String fullpath = metainf.path+metainf.name;
            ContentValues values = new ContentValues();
            
            values.put(AudioMedia.MEDIA_KEY, Long.toString(Funcs.createHashId(fullpath)));
            values.put(AudioMedia.TITLE, metainf.title);
            values.put(AudioMedia.TITLE_KEY, metainf.title.hashCode());
            values.put(AudioMedia.DURATION, 0);
            if (metainf.artist != null && metainf.artist.length() > 0) {
                values.put(AudioMedia.ARTIST, metainf.artist);
                values.put(AudioMedia.ARTIST_KEY, metainf.artist.hashCode());
            }
            if (metainf.album != null && metainf.album.length() > 0) {
                values.put(AudioMedia.ALBUM, metainf.album);
            }
            else {
                values.put(AudioMedia.ALBUM, "unknown");
            }
            values.put(AudioMedia.ALBUM_KEY, Funcs.createHashId(metainf.path));
            // values.put(AudioMedia.ALBUM_ART, "");
            values.put(AudioMedia.DATA, metainf.filepath);
            Logger.d("audio data = "+ metainf.filepath);
            if (metainf.track == null || metainf.track.length() == 0) {
                values.put(AudioMedia.TRACK, "9999");
            }
            else {
                values.put(AudioMedia.TRACK, metainf.track);
            }
            values.put(AudioMedia.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioMedia.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(AudioMedia.YEAR, metainf.year);
            if (metainf.cacheFile != null) {
                values.put(TableConsts.AUDIO_CACHE_FILE, metainf.cacheFile);
            }

            Uri uri = client.insert(MediaConsts.MEDIA_CONTENT_URI, values);
            Logger.d("add new audio "+fullpath+ " key="+values.getAsString(AudioMedia.MEDIA_KEY));
            return ContentUris.parseId(uri);
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
        }
        return -1;
    }

    public long insertVideo(ContentProviderClient client, VideoMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            String fullpath = metainf.path+metainf.name;
            ContentValues values = new ContentValues();
            values.put(VideoMedia.MEDIA_KEY, Funcs.createHashId(fullpath));
            values.put(VideoMedia.TITLE, metainf.title);
            values.put(VideoMedia.MIME_TYPE, metainf.mime);
            values.put(VideoMedia.RESOLUTION, "");
            values.put(VideoMedia.SIZE, metainf.size);
            values.put(VideoMedia.DATA, metainf.filepath);
            values.put(VideoMedia.DATE_ADDED, cal.getTimeInMillis());
            values.put(VideoMedia.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.VIDEO_DEL_FLG, 0);

            Uri uri = client.insert(MediaConsts.VIDEO_CONTENT_URI, values);
            return ContentUris.parseId(uri);
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    private long findAudioId(ContentProviderClient client, String path, String fname) {
        Cursor cursor = null;
        try{
            String fullpath = path+fname;
            Logger.d("query audio key="+fullpath+" "+Funcs.createHashId(fullpath));
            cursor = client.query(MediaConsts.MEDIA_CONTENT_URI, new String[]{AudioMedia._ID}, AudioMedia.MEDIA_KEY + " = ?",
                    new String[]{Long.toString(Funcs.createHashId(fullpath))}, null);
            if(cursor!=null && cursor.moveToFirst()){
                return cursor.getLong(0);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        finally{
            
        }
        return -1;
    }

    public long insertPlaylist(ContentProviderClient client, PlaylistMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();

        M3UParser parser = new M3UParser(this);

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            if(parser.getPlaylistEntries() != null && parser.getPlaylistEntries().size()>0){
                ContentValues values = new ContentValues();
                values.put(AudioPlaylist.NAME, metainf.name);
                values.put(AudioPlaylist.DATA, metainf.filepath);
                values.put(AudioPlaylist.DATE_ADDED, cal.getTimeInMillis());
                values.put(AudioPlaylist.DATE_MODIFIED, cal.getTimeInMillis());
    
                Uri insertedUri = client.insert(MediaConsts.PLAYLIST_CONTENT_URI, values);
                long playlistId = ContentUris.parseId(insertedUri);
                
                Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);
    
                int index = 0;
                for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                    long audioId = findAudioId(client, metainf.path, entry.fname);
                    Logger.d("add new playlist audio "+audioId);
                    if(audioId != -1){
                        ContentValues values2 = new ContentValues();
                        values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                        values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                        values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                        client.insert(playlisturi, values2);
                        index++;
                    }
                }
                return playlistId;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    public long updatePlaylist(ContentProviderClient client, long playlistId, PlaylistMetaInfo metainf) {
        Calendar cal = Calendar.getInstance();

        M3UParser parser = new M3UParser(this);

        Cursor cur = null;
        try {
            parser.processPlayList(metainf);

            if(parser.getPlaylistEntries() != null && parser.getPlaylistEntries().size()>0){
                client.delete(MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, AudioPlaylistMember.PLAYLIST_ID + " = ?",
                        new String[] {
                            Long.toString(playlistId)
                        });
                Uri playlisturi = ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId);
    
                int index = 0;
                for (PlaylistEntry entry : parser.getPlaylistEntries()) {
                    long audioId = findAudioId(client, metainf.path, entry.fname);
                    if(audioId != -1){
                        ContentValues values2 = new ContentValues();
                        values2.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
                        values2.put(MediaStore.Audio.Playlists.Members.PLAYLIST_ID, playlistId);
                        values2.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, index + 1);
                        client.insert(playlisturi, values2);
                        index++;
                    }
                }
                return playlistId;
            }else{
                client.delete(ContentUris.withAppendedId(MediaConsts.PLAYLIST_CONTENT_URI, playlistId), null, null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return -1;
    }

    public void reset() {
        context.getContentResolver().query(
                MediaConsts.RESET_CONTENT_URI,
                null,
                null,
                null, null);
    }

    public String getAuthrizedURL(String lalbum_art) {
        try {
            DropboxLink link = mApi.media(lalbum_art, false);
            return link.url;
        } catch (DropboxException e) {
            Logger.e("media url cant not get", e);
        }
        return null;
    }

    public String createFolder(String targetPath, File f) throws DropboxException {
        String newdir = createPath(targetPath) + f.getName();
        if (checkFile(newdir)) {
            mApi.createFolder(newdir);
        }
        return newdir;
    }

    public boolean checkFile(String path) {
        try {
            Entry entry = metadata(path, 0, null, false, null);
            if (entry != null && !entry.isDeleted) {
                return true;
            }
        } catch (DropboxException e) {
        }
        return false;
    }

    public void uploadFile(String targetPath, File f, ProgressListener listener)
            throws FileNotFoundException, DropboxException {
        if (f.isDirectory()) {

        } else {
            InputStream is = null;
            try {
                is = new FileInputStream(f);
                mApi.putFile(createPath(targetPath) + f.getName(), is, f.length(), null, listener);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
    }

    private String createPath(String path) {
        if (path.charAt(path.length() - 1) == '/') {
            return path;
        } else {
            return path + "/";
        }
    }
}
