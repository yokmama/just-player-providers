package jp.co.kayo.android.localplayer.ds.dropbox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.media.MediaMetadataRetriever;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;

public class FileNameRetriever {

    public FileMetaInfo get(Entry entry, String folderpattern, String filepattern){
        
        FileMetaInfo ret = new FileMetaInfo();
        ret.path = entry.parentPath();
        ret.filepath = entry.path;
        ret.name = entry.fileName();
        retrievFolderMetaInfo(ret, folderpattern, ret.path);
        retrievMetaInfo(ret, filepattern, ret.name);
        
        return ret;
    }
    
    public FileMetaInfo get(DropboxHelper dropboxHelper, Entry entry){
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            FileMetaInfo ret = new FileMetaInfo();
            ret.path = entry.parentPath();
            ret.filepath = entry.path;
            ret.name = entry.fileName();
            mmr.setDataSource(dropboxHelper.media(entry.path).url);
            ret.album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            ret.title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            ret.artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            ret.year = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR);
            ret.track = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER);
            ret.genre = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE);
            ret.duration = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            return ret;
        } catch (IllegalArgumentException e) {
            Logger.e("不正な引数", e);
        } catch (DropboxException e) {
            Logger.e("Dropboxで例外", e);
        }
        return null;
    }
    
    
    public String rplRegx(String regx, String ptn, String repl){
        regx = regx.replace(" "+ptn+" ", "(.+)");
        regx = regx.replace(" "+ptn, "(.+)");
        regx = regx.replace(ptn+" ", "(.+)");
        regx = regx.replace(ptn, "(.+)");
        return regx;
    }
    
    public void retrievFolderMetaInfo(FileMetaInfo metainfo, String pattern, String path){
        String[] dirs1 = pattern.split("/");
        String[] dirs2 = path.split("/");
        if(dirs1.length != dirs2.length){
            
            StringBuilder buf = new StringBuilder();
            for(int i=0; i<dirs1.length; i++){
                int n = dirs2.length - i;
                if(n>0){
                    buf.insert(0, dirs2[n-1]);
                    buf.insert(0, "/");
                }
            }
            retrievMetaInfo(metainfo, pattern, buf.toString());
        }
        else{
            retrievMetaInfo(metainfo, pattern, path);
        }
    }
    
    public void retrievMetaInfo(FileMetaInfo metainfo, String pattern, String fname){
        String regx = pattern;
        regx = rplRegx(regx, "%A", "(.+)");
        regx = rplRegx(regx, "%a", "(.+)");
        regx = rplRegx(regx, "%c", "(.+)");
        regx = rplRegx(regx, "%T", "(.+)");
        regx = rplRegx(regx, "%t", "(.+)");
        regx = rplRegx(regx, "%y", "(.+)");
        regx = rplRegx(regx, "%o", "(.+)");
        
        Pattern pat = Pattern.compile(regx);
        Matcher matcher = pat.matcher(fname);
        if(matcher!=null){
            int pos = 0;
            if (matcher.find()) {
                for(int i=1; i<=matcher.groupCount(); i++){
                    String str = matcher.group(i);
                    int p = pattern.indexOf("%", pos);
                    String sub = pattern.substring(p);
                    if(sub.startsWith("%A")){
                        metainfo.album = str.replace("/", "").trim();
                    }
                    else if(sub.startsWith("%a")){
                        metainfo.artist = str.replace("/", "").trim();
                    }
                    else if(sub.startsWith("%c")){
                        metainfo.comment = str.trim();
                    }
                    else if(sub.startsWith("%T")){
                        metainfo.track = str.trim();
                    }
                    else if(sub.startsWith("%t")){
                        int ext = str.lastIndexOf(".");
                        if(ext>0){
                            metainfo.title = str.substring(0, ext).trim();
                        }
                        else{
                            metainfo.title = str.trim();
                        }
                    }
                    else if(sub.startsWith("%y")){
                        metainfo.year = str.trim();
                    }
                    else if(sub.startsWith("%o")){
                        metainfo.desc = str.trim();
                    }
                    pos = p+1;
                }
            }
        }
    }
    
}
