package jp.co.kayo.android.localplayer.ds.dropbox;

import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.consts.MediaConsts.VideoMedia;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/***
 * このクラスはJustPlayerが独自に管理するデータへアクセスするためのクラスです
 * 
 * @author yokmama
 * 
 */
public class DropboxDatabaseHelper extends SQLiteOpenHelper {
    // データベースのバージョン（スキーマが変わったときにインクリメントする）
    private static final int DATABASE_VERSION = 4;
    private static final String DBNAME = "dropbox.db";

    /***
     * デフォルトのコンストラクタ
     * 
     * @param context
     * @param databasename
     */
    public DropboxDatabaseHelper(Context context) {
        super(context, DBNAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            // Albumのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_ALBUM)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_ALBUM)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioAlbum.ALBUM).append(" TEXT");
                sql.append(",").append(AudioAlbum.ALBUM_KEY).append(" TEXT");
                sql.append(",").append(AudioAlbum.ALBUM_ART).append(" TEXT");
                sql.append(",").append(AudioAlbum.FIRST_YEAR).append(" TEXT");
                sql.append(",").append(AudioAlbum.LAST_YEAR).append(" TEXT");
                sql.append(",").append(AudioAlbum.NUMBER_OF_SONGS)
                        .append(" INTEGER");
                sql.append(",").append(AudioAlbum.ARTIST).append(" TEXT");
                sql.append(",").append(AudioAlbum.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioAlbum.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ALBUM_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.ALBUM_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Artistのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_ARTIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_ARTIST)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioArtist.ARTIST).append(" TEXT");
                sql.append(",").append(AudioArtist.ARTIST_KEY).append(" TEXT");
                sql.append(",").append(AudioArtist.NUMBER_OF_ALBUMS)
                        .append(" INTEGER");
                sql.append(",").append(AudioArtist.NUMBER_OF_TRACKS)
                        .append(" INTEGER");
                sql.append(",").append(AudioArtist.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioArtist.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ARTIST_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.ARTIST_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Playlistのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_PLAYLIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_PLAYLIST)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioPlaylist.NAME).append(" TEXT");
                sql.append(",").append(AudioPlaylist.DATE_ADDED)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylist.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.PLAYLIST_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.PLAYLIST_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylist.DATA)
                .append(" TEXT");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // PlaylistAudioのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_PLAYLIST_AUDIO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ")
                        .append(TableConsts.TBNAME_PLAYLIST_AUDIO).append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioPlaylistMember.AUDIO_ID)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.PLAYLIST_ID)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.PLAY_ORDER)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylistMember.MEDIA_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TITLE)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TITLE_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DURATION)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.ARTIST)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ARTIST_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ALBUM)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.ALBUM_KEY)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DATA)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.TRACK)
                        .append(" INTEGER");
                sql.append(",").append(AudioPlaylistMember.YEAR)
                        .append(" TEXT");
                sql.append(",").append(AudioPlaylistMember.DATE_ADDED)
                        .append(" LONG");
                sql.append(",").append(AudioPlaylistMember.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Audioのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_AUDIO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_AUDIO)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioMedia.MEDIA_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.TITLE).append(" TEXT");
                sql.append(",").append(AudioMedia.TITLE_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.DURATION).append(" LONG");
                sql.append(",").append(AudioMedia.ARTIST).append(" TEXT");
                sql.append(",").append(AudioMedia.ARTIST_KEY).append(" TEXT");
                sql.append(",").append(AudioMedia.ALBUM).append(" TEXT");
                sql.append(",").append(AudioMedia.ALBUM_KEY).append(" TEXT");
                // sql.append(",").append(AudioMedia.ALBUM_ART).append(" TEXT");
                sql.append(",").append(AudioMedia.DATA).append(" TEXT");
                sql.append(",").append(AudioMedia.TRACK).append(" INTEGER");
                sql.append(",").append(AudioMedia.YEAR).append(" TEXT");
                sql.append(",").append(AudioMedia.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioMedia.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.AUDIO_DEL_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.AUDIO_CACHE_FILE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.GENRES_TAGS).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // orderlist
            if (!findTable(db, TableConsts.TBNAME_ORDERLIST)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ")
                        .append(TableConsts.TBNAME_ORDERLIST).append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.ORDER_TITLE).append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ARTIST)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ALBUM).append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_MEDIA_ID)
                        .append(" LONG");
                sql.append(",").append(TableConsts.ORDER_ALBUM_KEY)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_ARTIST_KEY)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.ORDER_DURATION)
                        .append(" LONG");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // video
            if (!findTable(db, TableConsts.TBNAME_VIDEO)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_VIDEO)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(VideoMedia.MEDIA_KEY).append(" TEXT");
                sql.append(",").append(VideoMedia.TITLE).append(" TEXT");
                sql.append(",").append(VideoMedia.MIME_TYPE).append(" TEXT");
                sql.append(",").append(VideoMedia.RESOLUTION).append(" TEXT");
                sql.append(",").append(VideoMedia.SIZE).append(" TEXT");
                sql.append(",").append(VideoMedia.DURATION).append(" LONG");
                sql.append(",").append(VideoMedia.DATE_ADDED).append(" LONG");
                sql.append(",").append(VideoMedia.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(VideoMedia.DATA).append(" INTEGER");
                sql.append(",").append(TableConsts.VIDEO_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.VIDEO_DEL_FLG)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // download
            if (!findTable(db, TableConsts.TBNAME_DOWNLOAD)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_DOWNLOAD)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.DOWNLOAD_ID).append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_MEDIA_ID)
                        .append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_TITLE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_LOCAL_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_REMOTE_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_TYPE)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.DOWNLOAD_STATUS)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // Genresのテーブルを生成
            if (!findTable(db, TableConsts.TBNAME_GENRES)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_GENRES)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(AudioGenres.NAME).append(" TEXT");
                sql.append(",").append(AudioGenres.GENRES_KEY).append(" TEXT");
                sql.append(",").append(AudioGenres.DATE_ADDED).append(" LONG");
                sql.append(",").append(AudioGenres.DATE_MODIFIED)
                        .append(" LONG");
                sql.append(",").append(TableConsts.GENRES_INIT_FLG)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.GENRES_DEL_FLG)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.d("Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        if (oldVersion < 3) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST_AUDIO);
            onCreate(db);
        } else if(oldVersion < 4){
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
            onCreate(db);
        } else {
            onCreate(db);
        }
    }

    public void rebuild(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ARTIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST_AUDIO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ORDERLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_VIDEO);
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_GENRES);
        onCreate(db);
    }

    public void reset(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ALBUM);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ARTIST);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_AUDIO);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_ORDERLIST);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_VIDEO);
        db.execSQL("DELETE FROM " + TableConsts.TBNAME_GENRES);
        Cursor cursor = null, cursor2 = null;
        try {
            cursor = db.query(TableConsts.TBNAME_PLAYLIST, new String[] {
                    AudioPlaylist._ID
            }, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    long playlistId = cursor.getLong(0);
                    cursor2 = db.rawQuery("select count(*) from "
                            + TableConsts.TBNAME_PLAYLIST_AUDIO + " where "
                            + AudioPlaylistMember.PLAYLIST_ID + " = ?", new String[] {
                            Long.toString(playlistId)
                    });
                    if (cursor2 != null && cursor2.moveToFirst()) {
                        long count = cursor2.getLong(0);
                        if (count == 0) {
                            db.delete(TableConsts.TBNAME_PLAYLIST,
                                    AudioPlaylist._ID + " = ?", new String[] {
                                        Long.toString(playlistId)
                                    });
                        }
                        cursor2.close();
                    }
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void rebuildSelect(SQLiteDatabase db, String[] projection) {
        if (containParams(projection, TableConsts.TBNAME_ALBUM)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ALBUM);
        }
        if (containParams(projection, TableConsts.TBNAME_ARTIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ARTIST);
        }
        if (containParams(projection, TableConsts.TBNAME_GENRES)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_GENRES);
        }
        if (containParams(projection, TableConsts.TBNAME_AUDIO)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_AUDIO);
        }
        if (containParams(projection, TableConsts.TBNAME_PLAYLIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYLIST);
        }
        if (containParams(projection, TableConsts.TBNAME_PLAYLIST_AUDIO)) {
            db.execSQL("DROP TABLE IF EXISTS "
                    + TableConsts.TBNAME_PLAYLIST_AUDIO);
        }
        if (containParams(projection, TableConsts.TBNAME_VIDEO)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_VIDEO);
        }
        if (containParams(projection, TableConsts.TBNAME_ORDERLIST)) {
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_ORDERLIST);
        }
        onCreate(db);
    }

    private boolean containParams(String[] tables, String checkTable) {
        for (String table : tables) {
            if (table == checkTable) {
                return true;
            }
        }
        return false;
    }

    public long getCount(SQLiteDatabase db, String tbl) {
        Cursor cur = null;
        try {
            if (findTable(db, tbl)) {
                cur = db.rawQuery("select count(*) from " + tbl, null);
                if (cur != null && cur.moveToFirst()) {
                    long count = cur.getLong(0);
                    return count;
                }
            }
            return 0;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean findTable(SQLiteDatabase db, String tbl) {

        StringBuilder where = new StringBuilder();
        where.append("type='table' and name='");
        where.append(tbl);
        where.append("'");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("sqlite_master"); // テーブル名
        Cursor cur = null;
        try {
            cur = qb.query(db, null, where.toString(), null, null, null, null,
                    null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    return true;
                }
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
}
