package jp.co.kayo.android.localplayer.ds.dropbox;

import com.dropbox.client2.DropboxAPI.Entry;

public class MyEntry {
    boolean linkParent = true;
    Entry entry;

    public MyEntry(Entry entry){
        linkParent = false;
        this.entry = entry;
    }
    
    public MyEntry(){
        linkParent = true;
    }

    public String fileName() {
        if(entry!=null){
            return entry.fileName();
        }
        else{
            return null;
        }
    }
    
    public boolean isParent(){
        return linkParent;
    }

    public boolean isDir() {
        if(entry!=null){
            return entry.isDir;
        }
        else{
            return false;
        }
    }

    public String getPath() {
        if(entry!=null){
            return entry.path;
        }
        else{
            return null;
        }
    }
}
