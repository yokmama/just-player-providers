//  Copyright 2011 Box.net.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

package jp.co.kayo.android.localplayer.ds.dropbox;

import java.io.IOException;

import jp.co.kayo.android.localplayer.consts.MediaConsts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Dashboard extends Activity {

    private String authToken;
    private SharedPreferences pref;
    private DropboxHelper dropboxHelper;
    private boolean mLoggedIn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        dropboxHelper = new DropboxHelper(this);
        mLoggedIn = dropboxHelper.isLoggdIn();
        if (!mLoggedIn) {
            Toast.makeText(getApplicationContext(), "You are not logged in.", Toast.LENGTH_SHORT)
                    .show();
            finish();
            return;
        }

        // Dropboxをデフォルトにする
        try {
            ContentValues values = new ContentValues();
            values.put("key_current_uri", MediaConsts.DROPBOX_AUTHORITY);
            getContentResolver().update(Uri.parse("content://jp.co.kayo.android.localplayer/pref"),
                    values, null, null);
        } catch (Exception e) {
            // JUST PLAYERのインストール画面を表示しますか？
            showInstallDialog();
        }

        final Button logoutButton = (Button) findViewById(R.id.logoutButton);
        final Button fullsyncButton = (Button) findViewById(R.id.fullsyncButton);
        final Button syncButton = (Button) findViewById(R.id.syncButton);
        final Button remoteButton = (Button) findViewById(R.id.remoteButton);
        final Button localButton = (Button) findViewById(R.id.localButton);
        final Button confButton = (Button) findViewById(R.id.confButton);

        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, MediaScanner.class);
                i.setAction("sync");
                startService(i);
            }
        });
        
        fullsyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContentResolver().query(MediaConsts.RESET_CONTENT_URI, null, null, null, null);
                Editor editor = pref.edit();
                editor.putInt("mediaSum", -1);
                editor.commit();
            }
        });

        remoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, RemoteFileBrowser.class);
                startActivity(i);
            }
        });

        localButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, LocalFileBrowser.class);
                startActivity(i);
            }
        });
        
        confButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Dashboard.this, PrefActivity.class);
                startActivity(i);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropboxHelper.getSession().unlink();

                mLoggedIn = false;
                dropboxHelper.clearKeys();

                Toast
                        .makeText(getApplicationContext(), "Logged out",
                                Toast.LENGTH_LONG)
                        .show();
                Intent i = new Intent(Dashboard.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showInstallDialog() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.txt_install_jp))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.txt_yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Uri uri = Uri
                                        .parse("market://search?q=jp.co.kayo.android.localplayer");
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton(getString(R.string.txt_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .show();

    }
}
