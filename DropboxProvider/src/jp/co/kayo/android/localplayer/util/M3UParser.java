package jp.co.kayo.android.localplayer.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.dropbox.DropboxHelper;
import jp.co.kayo.android.localplayer.ds.dropbox.MediaFile;
import jp.co.kayo.android.localplayer.ds.dropbox.PlaylistMetaInfo;
import android.os.Build;
import android.os.RemoteException;
import android.util.Log;

import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;

public class M3UParser {
    private static final String TAG = "JUSTPLAYER.PLAYLIST";
    private DropboxHelper mDropboxHelper;

    public static class PlaylistEntry {
        public String filepath;
        public String fname;
        public long bestmatchid;
        public int bestmatchlevel;
    }
    
    public M3UParser(DropboxHelper helper){
        mDropboxHelper = helper;
    }

    private ArrayList<PlaylistEntry> mPlaylistEntries = new ArrayList<PlaylistEntry>();
    
    public ArrayList<PlaylistEntry> getPlaylistEntries(){
        return mPlaylistEntries;
    }

    private void cachePlaylistEntry(String line, String playListDirectory) {
        PlaylistEntry entry = new PlaylistEntry();
        entry.fname = new File(line).getName();
        entry.filepath = playListDirectory+entry.fname;
        mPlaylistEntries.add(entry);
    }

    private void processM3uPlayList(String path, String playListDirectory) {
        BufferedReader reader = null;
        try {
            Entry entry = mDropboxHelper.metadata(path, 0, null, true, null);
            if (entry != null && !entry.isDir) {
                URL url = new URL(mDropboxHelper.getAuthrizedURL(entry.path));
                reader = new BufferedReader(
                        new InputStreamReader(url.openStream()), 8192);
                String line = reader.readLine();
                mPlaylistEntries.clear();
                while (line != null) {
                    // ignore comment lines, which begin with '#'
                    if (line.length() > 0 && line.charAt(0) != '#') {
                        cachePlaylistEntry(line, playListDirectory);
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processM3uPlayList()", e);
        } catch (DropboxException e) {
            Log.e(TAG, "DropboxException in MediaScanner.processM3uPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException in MediaScanner.processM3uPlayList()", e);
            }
        }
    }

    private void processPlsPlayList(String path, String playListDirectory) {
        BufferedReader reader = null;
        try {
            Entry entry = mDropboxHelper.metadata(path, 0, null, true, null);
            if (entry != null && !entry.isDir) {
                URL url = new URL(mDropboxHelper.getAuthrizedURL(entry.path));
                
                reader = new BufferedReader(
                        new InputStreamReader(url.openStream()), 8192);
                String line = reader.readLine();
                mPlaylistEntries.clear();
                while (line != null) {
                    // ignore comment lines, which begin with '#'
                    if (line.startsWith("File")) {
                        int equals = line.indexOf('=');
                        if (equals > 0) {
                            cachePlaylistEntry(line.substring(equals + 1), playListDirectory);
                        }
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processPlsPlayList()", e);
        } catch (DropboxException e) {
            Log.e(TAG, "DropboxException in MediaScanner.processPlsPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException in MediaScanner.processPlsPlayList()", e);
            }
        }
    }
    
    private void processTxtPlayList(String path, String playListDirectory) {
        BufferedReader reader = null;
        try {
            Entry entry = mDropboxHelper.metadata(path, 0, null, true, null);
            if (entry != null && !entry.isDir) {
                URL url = new URL(mDropboxHelper.getAuthrizedURL(entry.path));
                
                reader = new BufferedReader(
                        new InputStreamReader(url.openStream()), 8192);
                String line = reader.readLine();
                mPlaylistEntries.clear();
                while (line != null) {
                    // ignore comment lines, which begin with '#'
                    if (line.length() > 0 && line.charAt(0) != '#' && line.indexOf(".") > 0) {
                        cachePlaylistEntry(line, playListDirectory);
                    }
                    line = reader.readLine();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException in MediaScanner.processTxtPlayList()", e);
        } catch (DropboxException e) {
            Log.e(TAG, "DropboxException in MediaScanner.processTxtPlayList()", e);
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException in MediaScanner.processTxtPlayList()", e);
            }
        }
    }
    
    public String getPlaylistName(String path){
        int lastSlash = path.lastIndexOf('/');
        int lastDot = path.lastIndexOf('.');
        if (lastSlash < 0) {
            String name = (lastDot < 0 ? path : path.substring(0, lastDot));
            return name;
        }
        else {
            String name = (lastDot < 0 ? path.substring(lastSlash + 1)
                    : path.substring(lastSlash + 1, lastDot));
            return name;
        }
    }
    
    public void processPlayList(PlaylistMetaInfo entry) throws RemoteException {
        int lastSlash = entry.filepath.lastIndexOf('/');
        if (lastSlash < 0)
            throw new IllegalArgumentException("bad path " + entry.filepath);
        // make sure we have a name
        int lastDot = entry.filepath.lastIndexOf('.');
        entry.name = (lastDot < 0 ? entry.filepath.substring(lastSlash + 1)
                : entry.filepath.substring(lastSlash + 1, lastDot));

        String playListDirectory = entry.path;
        MediaFile.MediaFileType mediaFileType = MediaFile.getFileType(entry.filepath);
        int fileType = (mediaFileType == null ? 0 : mediaFileType.fileType);

        if (fileType == MediaFile.FILE_TYPE_M3U) {
            processM3uPlayList(entry.filepath, playListDirectory);
        } else if (fileType == MediaFile.FILE_TYPE_PLS) {
            processPlsPlayList(entry.filepath, playListDirectory);
        } else if (fileType == MediaFile.FILE_TYPE_TEXT) {
            processTxtPlayList(entry.filepath, playListDirectory);
        }
    }
}
