package jp.co.kayo.android.localplayer.ds.podcast;

import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PodcastAdapter extends CursorAdapter {
    private LayoutInflater mInflator;
    private ViewCache viewCache;
    private Handler handler = new Handler();

    public PodcastAdapter(Context context, Cursor c, boolean autoRequery, ViewCache cache) {
        super(context, c, autoRequery);
        viewCache = cache;
    }

    public LayoutInflater getInflator(Context context) {
        if (mInflator == null) {
            mInflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return mInflator;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
        String album = cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM));
        String artist = cursor.getString(cursor.getColumnIndex(AudioAlbum.ARTIST));
        String album_art = cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM_ART));
        String subtitle = cursor.getString(cursor.getColumnIndex(TableConsts.PODCAST_SUBTITLE));
        int position = cursor.getPosition();

        holder.textTitle.setText(album);
        holder.textSubTitle.setText(subtitle);
        holder.setPotision(position);
        
        Integer key = ViewCache.getAlbumKey(album, artist);
        Bitmap bmp = viewCache.getImage(
                album,
                artist,
                album_art,
                new ImageObserverImpl(handler, viewCache, holder,
                        key, position));
        if (bmp != null) {
            holder.imagePodcast.setImageBitmap(bmp);
        } else {
            holder.imagePodcast.setImageBitmap(null);
        }
         
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.row, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.imagePodcast = (ImageView) v.findViewById(R.id.imagePodcast);
        holder.textTitle = (TextView) v.findViewById(R.id.textTitle);
        holder.textSubTitle = (TextView) v.findViewById(R.id.textSubTitle);
        v.setTag(holder);
        return v;
    }
}
