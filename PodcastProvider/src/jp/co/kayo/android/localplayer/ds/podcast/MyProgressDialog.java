package jp.co.kayo.android.localplayer.ds.podcast;

import android.app.ProgressDialog;
import android.widget.TextView;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class MyProgressDialog extends ProgressDialog {
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String text = msg.getData().get("text").toString();
            ((TextView)findViewById(R.id.textProgressDialog)).setText(text);
        }
    };   
    
    private Runnable runnable = new Runnable(){
        @Override
        public void run() {
            try{
                callback.onExecute(handler);
            }
            finally{
                //dismissDialog(ID_PROGRESS_DIALOG);
                dismiss();
            }
        }
    };
    
    private DialogCallback callback;
    
    
    public MyProgressDialog(Context context, DialogCallback callback) {
        super(context);
        this.callback = callback;
        setProgressStyle(ProgressDialog.STYLE_SPINNER);
        setMessage(context.getString(R.string.msg_loading));
        setCancelable(true);
    }

    public void start(){
        (new Thread(runnable)).start();
    }
}
