package jp.co.kayo.android.localplayer.ds.podcast;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder {
    private int mPotision;
    public ImageView imagePodcast;
    public TextView textTitle;
    public TextView textSubTitle;

    public ViewHolder() {
    }
    
    public synchronized void setPotision(int potision) {
        mPotision = potision;
    }
    
    public synchronized int getPotision() {
        return mPotision;
    }
}