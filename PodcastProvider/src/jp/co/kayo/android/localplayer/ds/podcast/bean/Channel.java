package jp.co.kayo.android.localplayer.ds.podcast.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import jp.co.kayo.android.localplayer.ds.podcast.Logger;

public class Channel {
    public String site;
    public String title;
    public String link;
    public String description;
    public String language;
    public String copyright;
    public Image image;
    public String pubDate;
    public String itunes_subtitle;
    public String itunes_author;
    public String itunes_summary;
    public Owner itunes_owner;
    public String itunes_image;
    public long point;
    public ArrayList<String> itunes_category = new ArrayList<String>();
    public ArrayList<Item> items = new ArrayList<Item>();
    

    public void verbose() {
        Logger.d("Channel.site="+site);
        Logger.d("Channel.title="+title);
        Logger.d("Channel.link="+link);
        Logger.d("Channel.description="+description);
        Logger.d("Channel.language="+language);
        Logger.d("Channel.copyright="+copyright);
        if(image!=null){
            image.vervose();
        }
        Logger.d("Channel.pubDate="+pubDate);
        Logger.d("Channel.itunes_subtitle"+itunes_subtitle);
        Logger.d("Channel.itunes_author="+itunes_author);
        Logger.d("Channel.itunes_summary="+itunes_summary);
        Logger.d("Channel.itunes_owner="+itunes_owner);
        Logger.d("Channel.itunes_image="+itunes_image);
        Logger.d("Channel.point="+point);
        
    }
    
    public static String getKey(String site){
        if(site!=null){
            return Integer.toString(site.hashCode());
        }
        return "0";
    }
    
    public String getAlbumKey(){
        return getKey(site);
    }
    
    
    public String getArtistKey(){
        return getKey(itunes_author);
    }
    
    private Date tempPubDate;
    public Date getPubDate(){
        if(tempPubDate == null){
            SimpleDateFormat fmt = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            try {
                tempPubDate = fmt.parse("Sat, 26 May 2012 17:55:00 +0900");
            } catch (ParseException e) {
                tempPubDate = new Date();
            }
        }
        return tempPubDate;
    }

    public String getImage() {
        if(itunes_image!=null){
            return itunes_image;
        }else if(image!=null){
            return image.url;
        }
        return null;
    }

    public String getCategory() {
        StringBuilder buf = new StringBuilder();
        for(String s : itunes_category){
            if(buf.length()>0){
                buf.append(",");
            }
            buf.append("'");
            buf.append(s);
            buf.append("'");
        }
        return buf.toString();
    }
    
    public void setCategory(String c){
        String[] categorys = c.split("','");
        for(String s : categorys){
            if(s.startsWith("'")){
                s = s.substring(1);
            }
            if(s.endsWith("'")){
                s = s.substring(0, s.length()-1);
            }
            if(s!=null && s.length()>0){
                itunes_category.add(s);
            }
        }
    }

    public String getTagKeys() {
        StringBuilder buf = new StringBuilder();
        for(String s : itunes_category){
            if(s!=null && s.length()>0){
                buf.append("/");
                buf.append(getKey(s));
            }
        }
        if(buf.length()>0){
            buf.append("/");
        }
        return buf.toString();
    }
}
