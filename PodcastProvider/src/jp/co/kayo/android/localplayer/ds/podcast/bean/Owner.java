package jp.co.kayo.android.localplayer.ds.podcast.bean;

public class Owner {
    public String itunes_name;
    public String itunes_email;
    
    public Owner(){}
    public Owner(String name){
        itunes_name = name;
    }
}
