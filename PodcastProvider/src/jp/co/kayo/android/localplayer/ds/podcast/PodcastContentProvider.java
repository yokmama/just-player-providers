package jp.co.kayo.android.localplayer.ds.podcast;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Channel;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;

public class PodcastContentProvider extends ContentProvider implements MediaConsts{
    public static File cachedMusicDir = new File(
            Environment.getExternalStorageDirectory(),
            "data/jp.co.kayo.android.localplayer/cache/.mp3/");
    
    private UriMatcher uriMatcher = null;
    private SharedPreferences mPreference;
    private PodcastDatabaseHelper dbhelper;
    private PodcastHelper podcastHelper;
    private ConnectivityManager mConnectionMgr;
    
    public static final String PODCAST_AUTHORITY = MediaConsts.AUTHORITY+".podcast";
    public static final String PODCAST_CONTENT_AUTHORITY_SLASH = "content://" + PODCAST_AUTHORITY + "/";
    
    public static final Uri ALBUM_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/albums");
    public static final Uri MEDIA_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/media");
    public static final Uri ARTIST_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/artist");
    public static final Uri PLAYLIST_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/playlist");
    public static final Uri PLAYLIST_MEMBER_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/playlistmember");
    public static final Uri GENRES_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/genres");
    public static final Uri GENRES_MEMBER_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/genresmember");
    public static final Uri PLAYORDER_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "order/audio");
    public static final Uri FOLDER_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/file");
    public static final Uri VIDEO_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "video/media");
    public static final Uri FAVORITE_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "audio/favorite");
    public static final Uri CLEAR_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "config/clear");
    public static final Uri RESET_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "config/reset");
    public static final Uri AUTH_CONTENT_URI = Uri.parse(PODCAST_CONTENT_AUTHORITY_SLASH + "config/auth");
    
    String[] QUERY_AUDIO_COLS = new String[] { BaseColumns._ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK };
    
    String[] QUERY_PLAYLIST_MEMBER_COLS = new String[] { BaseColumns._ID,
            AudioPlaylistMember.AUDIO_ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK };
    
    public static final String CNF_WIFI_ONLY = "cnf_wifi_only";

    private boolean isWifiConnected() {
        if (mConnectionMgr == null) {
            mConnectionMgr = (ConnectivityManager) getContext()
                    .getApplicationContext().getSystemService(
                            Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo info = mConnectionMgr.getActiveNetworkInfo();
        if (info != null) {
            return ((info.getType() == ConnectivityManager.TYPE_WIFI) && (info
                    .isConnected()));
        }

        return false;
    }

    private boolean canUseNetwork() {
        if (mPreference.getBoolean(CNF_WIFI_ONLY, true)) {
            return isWifiConnected();
        } else {
            return true;
        }
    }
    
    private static String[] fastArrayCopy(String[] selectionArgs, int length){
        return Arrays.copyOf(selectionArgs, length);
    }
    
    private static Long parseLong(String s) {
        try {
            if (s != null) {
                return Long.parseLong(s);
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    private Hashtable<String, String> getMedia(Context contxt,
            String[] projection, long mediaId) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            String[] prj = projection;
            if (prj == null) {
                prj = new String[] { AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
                        AudioMedia.ARTIST, AudioMedia.TITLE,
                        AudioMedia.DURATION };
            }
            cur = contxt.getContentResolver().query(
                    ContentUris.withAppendedId(MEDIA_CONTENT_URI, mediaId),
                    prj, MediaConsts.AudioMedia._ID + " = ?", null, null);
            if (cur != null && cur.moveToFirst()) {
                for (int i = 0; i < prj.length; i++) {
                    String value = cur.getString(cur.getColumnIndex(prj[i]));
                    if (value != null) {
                        tbl.put(prj[i], value);
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
    
    public Map<String, Object> getID3TagInfo(File fname) {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            Map<String, Object> ret = new HashMap<String, Object>();
            mmr.setDataSource(fname.getPath());
            ret.put("album", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
            ret.put("title", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
            ret.put("artist",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
            ret.put("year", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR));
            ret.put("track",
                    mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER));
            ret.put("genre", mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
            ret.put("duration",
                    parseLong(mmr
                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));

            return ret;
        } catch (IllegalArgumentException e) {
            Logger.e("不正な引数", e);
        }
        return null;
    }
    
    private byte[] getBytes(String s) {
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            if (c >= 0x100) {
                // Byte列になっていない
                return null;
            }
            bytes[i] = (byte) c;
        }
        return bytes;
    }
    
    private int setId3Tag(Cursor cur, String enc) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        boolean hasTransaction = false;
        int count = 0;
        try {
            do {
                ContentValues updateValues = new ContentValues();
                byte[] bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.TITLE, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ARTIST, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ALBUM, new String(bufs, enc));
                }
                if (updateValues.size() > 0) {
                    if (hasTransaction != true) {
                        hasTransaction = true;
                        db.beginTransaction();
                    }
                    long id = cur.getLong(cur.getColumnIndex(AudioMedia._ID));
                    db.update(TableConsts.TBNAME_AUDIO, updateValues,
                            AudioMedia._ID + " = ?",
                            new String[] { Long.toString(id) });
                    count++;
                }
            } while (cur.moveToNext());
            return count;
        } catch (UnsupportedEncodingException e) {
        } finally {
            if (hasTransaction) {
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }
        return count;
    }

    private synchronized PodcastDatabaseHelper getDBHelper() {
        if (dbhelper == null) {
            if (dbhelper != null) {
                dbhelper.close();
                dbhelper = null;
            }
            dbhelper = new PodcastDatabaseHelper(getContext(), "podcast.db");
            // アップグレードのため一旦書込でひらいておく
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return dbhelper;
    }

    private void reset() {
        // TODO Auto-generated method stub
        
    }

    private PodcastHelper getPodcastHelper() {
        if(podcastHelper == null){
            podcastHelper = new PodcastHelper();
        }
        return podcastHelper;
    }
    
    @Override
    public boolean onCreate() {
        mPreference = PreferenceManager
                .getDefaultSharedPreferences(getContext()
                        .getApplicationContext());

        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/media", CODE_MEDIA);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/media/#", CODE_MEDIA_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/albums", CODE_ALBUMS);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/albums/#", CODE_ALBUMS_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/artist", CODE_ARTIST);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/artist/#", CODE_ARTIST_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/albumart", AUDIO_ALBUMART);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/albumart/#",
                AUDIO_ALBUMART_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/media/#/albumart",
                AUDIO_ALBUMART_FILE_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/playlist", CODE_PLAYLIST);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/playlist/#",
                CODE_PLAYLIST_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/playlistmember",
                CODE_PLAYLISTMEMBER);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/playlistmember/#",
                CODE_PLAYLISTMEMBER_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/favorite", CODE_FAVORITE);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/favorite/#",
                CODE_FAVORITE_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/file", CODE_FILE);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/file/#", CODE_FILE_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "video/media", CODE_VIDEO);
        uriMatcher.addURI(PODCAST_AUTHORITY, "video/media/#", CODE_VIDEO_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "order/audio", CODE_ORDER_AUDIO);
        uriMatcher.addURI(PODCAST_AUTHORITY, "order/audio/#",
                CODE_ORDER_AUDIO_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "download/media", CODE_DOWNLOAD);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/genres", CODE_GENRES);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/genres/#", CODE_GENRES_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/genresmember",
                CODE_GENRESMEMBER);
        uriMatcher.addURI(PODCAST_AUTHORITY, "audio/genresmember/#",
                CODE_GENRESMEMBER_ID);
        uriMatcher.addURI(PODCAST_AUTHORITY, "config/auth", CODE_AUTH);
        uriMatcher.addURI(PODCAST_AUTHORITY, "config/clear", CODE_CLEAR);
        uriMatcher.addURI(PODCAST_AUTHORITY, "config/reset", CODE_RESET);
        uriMatcher.addURI(PODCAST_AUTHORITY, "config/ping", CODE_PING);
        uriMatcher.addURI(PODCAST_AUTHORITY, "config/url", CODE_URL);

        return true;
    }
    
    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_ORDER_AUDIO: {
            return deleteOrderAudio(uri, selection, selectionArgs);
        }
        case CODE_PLAYLIST: {
            return deletePlaylist(uri, selection, selectionArgs);
        }
        case CODE_PLAYLISTMEMBER: {
            return deletePlaylistmember(uri, selection, selectionArgs);
        }
        case CODE_ALBUMS: {
            return deleteAlbums(uri, selection, selectionArgs);
        }
        case CODE_ALBUMS_ID: {
            return deleteAlbumsId(uri, selection, selectionArgs);
        }
        case CODE_ARTIST: {
            return deleteArtist(uri, selection, selectionArgs);
        }
        case CODE_MEDIA: {
            return deleteMedia(uri, selection, selectionArgs);
        }
        case CODE_VIDEO: {
            return deleteVideo(uri, selection, selectionArgs);
        }

        }
        return 0;
    }

    private int deleteVideo(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db.delete(TableConsts.TBNAME_VIDEO, selection, selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deleteMedia(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db.delete(TableConsts.TBNAME_AUDIO, selection, selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deleteArtist(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db
                .delete(TableConsts.TBNAME_ARTIST, selection, selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deleteAlbums(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db.delete(TableConsts.TBNAME_ALBUM, selection, selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deleteAlbumsId(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        long id = ContentUris.parseId(uri);
        int ret = db.delete(TableConsts.TBNAME_ALBUM, AudioAlbum._ID + " = ?",
                new String[]{Long.toString(id)});

        return ret;
    }
    
    private int deletePlaylistmember(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                MediaConsts.AudioPlaylistMember._ID + " = ?",
                new String[] { selectionArgs[0] });
        Logger.d("ret=" + ret);
        return ret;
    }

    private int deletePlaylist(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        int ret = db.delete(TableConsts.TBNAME_PLAYLIST, selection,
                selectionArgs);
        // リストのメンバーも削除する
        db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                MediaConsts.AudioPlaylistMember.PLAYLIST_ID + " = ?",
                selectionArgs);

        return ret;
    }

    private int deleteOrderAudio(Uri uri, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db
                .delete(TableConsts.TBNAME_ORDERLIST, selection, selectionArgs);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_MEDIA: {
            return insertMedia(uri, values);
        }
        case CODE_ALBUMS: {
            return insertAlbum(uri, values);
        }
        case CODE_ARTIST: {
            return insertArtist(uri, values);
        }
        case CODE_PLAYLIST: {
            return insertPlaylist(uri, values);
        }
        case CODE_PLAYLIST_ID: {
            return insertPlaylistmember(uri, values);
        }
        case CODE_FAVORITE: {
            return insertFavorite(uri, values);
        }
        case CODE_VIDEO: {
            return insertVideo(uri, values);
        }
        case CODE_ORDER_AUDIO: {
            return insertOrderAudio(uri, values);
        }
        case CODE_DOWNLOAD: {
            return insertDownload(uri, values);
        }
        case CODE_GENRES: {
            return insertGenres(uri, values);
        }
        case CODE_GENRES_ID: {
            return insertGenresmember(uri, values);
        }
        }
        return null;
    }

    private Uri insertGenresmember(Uri uri, ContentValues values) {
        // TODO Auto-generated method stub
        return null;
    }

    private Uri insertGenres(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = db.insert(TableConsts.TBNAME_GENRES, null, values);
        return ContentUris.withAppendedId(GENRES_CONTENT_URI, id);
    }

    private Uri insertDownload(Uri uri, ContentValues values) {
        // TODO Auto-generated method stub
        return null;
    }

    private Uri insertOrderAudio(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        long id = db.insert(TableConsts.TBNAME_ORDERLIST, null, values);
        return ContentUris.withAppendedId(PLAYORDER_CONTENT_URI, id);
    }

    private Uri insertVideo(Uri uri, ContentValues values) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();

            long id = db.insert(TableConsts.TBNAME_VIDEO, null, values);
            return ContentUris.withAppendedId(VIDEO_CONTENT_URI, id);
        } finally {
        }
    }

    private Uri insertFavorite(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = values.getAsLong(FAVORITE_ID);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = values.getAsString(FAVORITE_TYPE);
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] { Long.toString(id) });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] { Long.toString(id) });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] { Long.toString(id) });
            }
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(FAVORITE_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    private Uri insertPlaylistmember(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = ContentUris.parseId(uri);
        long media_id = values.getAsLong(AudioPlaylistMember.AUDIO_ID);
        int order = values.getAsInteger(AudioPlaylistMember.PLAY_ORDER);

        Cursor cur = null;
        boolean hasTransaction = false;
        try {
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_AUDIO);
            cur = qb.query(db, PodcastHelper.MEDIA_FIELDS, AudioMedia._ID + " = ?",
                    new String[] { Long.toString(media_id) }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                String media_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.MEDIA_KEY));
                String title = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE));
                String title_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE_KEY));
                long duration = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DURATION));
                String artist = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST));
                String artist_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST_KEY));
                String album = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM));
                String album_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM_KEY));
                String data = cur
                        .getString(cur.getColumnIndex(AudioMedia.DATA));
                int tarck = cur.getInt(cur.getColumnIndex(AudioMedia.TRACK));
                long date_added = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_ADDED));
                long date_modified = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_MODIFIED));
                String tags = cur.getString(cur
                        .getColumnIndex(TableConsts.GENRES_TAGS));
                String year = cur.getString(cur
                        .getColumnIndex(AudioPlaylistMember.YEAR));
                int point = cur.getInt(cur
                        .getColumnIndex(TableConsts.FAVORITE_POINT));

                db.beginTransaction();
                hasTransaction = true;
                ContentValues val = new ContentValues();
                val.put(TableConsts.PLAYLIST_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_PLAYLIST, val, AudioPlaylist._ID
                        + "=?", new String[] { Long.toString(id) });

                ContentValues dvalues_pl = new ContentValues();
                dvalues_pl.put(AudioPlaylistMember._ID, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAY_ORDER, order);
                dvalues_pl.put(AudioPlaylistMember.AUDIO_ID, media_id);
                dvalues_pl.put(AudioPlaylistMember.MEDIA_KEY, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAYLIST_ID, id);
                dvalues_pl.put(AudioPlaylistMember.TITLE, title);
                dvalues_pl.put(AudioPlaylistMember.TITLE_KEY, title_key);
                dvalues_pl.put(AudioPlaylistMember.DURATION, duration);
                dvalues_pl.put(AudioPlaylistMember.ARTIST, artist);
                dvalues_pl.put(AudioPlaylistMember.ARTIST_KEY, artist_key);
                dvalues_pl.put(AudioPlaylistMember.ALBUM, album);
                dvalues_pl.put(AudioPlaylistMember.ALBUM_KEY, album_key);
                dvalues_pl.put(AudioPlaylistMember.DATA, data);
                dvalues_pl.put(AudioPlaylistMember.TRACK, tarck);
                dvalues_pl.put(AudioPlaylistMember.DATE_ADDED, date_added);
                dvalues_pl
                        .put(AudioPlaylistMember.DATE_MODIFIED, date_modified);
                dvalues_pl.put(TableConsts.GENRES_TAGS, tags);
                dvalues_pl.put(AudioPlaylistMember.YEAR, year);
                dvalues_pl.put(TableConsts.FAVORITE_POINT, point);
                long ret_id = db.replace(TableConsts.TBNAME_PLAYLIST_AUDIO,
                        null, dvalues_pl);

                db.setTransactionSuccessful();
                return ContentUris.withAppendedId(
                        PLAYLIST_MEMBER_CONTENT_URI, ret_id);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (hasTransaction) {
                db.endTransaction();
            }
        }
        return null;
    }

    private Uri insertPlaylist(Uri uri, ContentValues values) {
        values.remove(AudioPlaylist.PLAYLIST_KEY);
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = db.insert(TableConsts.TBNAME_PLAYLIST, null, values);
        return ContentUris.withAppendedId(PLAYLIST_CONTENT_URI, id);
    }

    private Uri insertArtist(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        long id = db.insert(TableConsts.TBNAME_ARTIST, null, values);
        return ContentUris.withAppendedId(ARTIST_CONTENT_URI, id);
    }

    private Uri insertAlbum(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        long id = db.insert(TableConsts.TBNAME_ALBUM, null, values);
        return ContentUris.withAppendedId(ALBUM_CONTENT_URI, id);
    }

    private Uri insertMedia(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        long id = db.insert(TableConsts.TBNAME_AUDIO, null, values);
        return ContentUris.withAppendedId(MEDIA_CONTENT_URI, id);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_MEDIA: {
            return queryMedia(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_MEDIA_ID: {
            return queryMediaId(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ARTIST: {
            // 移植済
            return queryArtist(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_PLAYLIST: {
            // 移植済
            return queryPlaylist(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_PLAYLIST_ID: {
            return queryPlaylistmember(uri, projection, selection,
                    selectionArgs, sortOrder);
        }
        case CODE_FAVORITE: {
            // 移植済
            return queryFavorite(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_VIDEO: {
            // 移植済
            return queryVideo(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_VIDEO_ID: {
            return queryVideoId(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ALBUMS: {
            // 移植済
            return queryAlbum(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_GENRES: {
            // 移植済
            return queryGenres(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_GENRESMEMBER: {
            return queryGenresmember(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ORDER_AUDIO: {
            return queryOrderAudio(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_ORDER_AUDIO_ID: {
            return queryOrderAudioId(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_DOWNLOAD: {
            return queryDownload(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_PING: {
            boolean b = getPodcastHelper().isLoggdIn();
            MatrixCursor cursor = null;
            if (b == true) {
                cursor = new MatrixCursor(new String[] { Auth._ID,
                        Auth.AUTH_KEY });
                cursor.addRow(new Object[] { 0, "hassession" });
            }
            return cursor;
        }
        case CODE_AUTH:
        case CODE_URL: {
            MatrixCursor cursor = new MatrixCursor(new String[] { Auth._ID,
                    Auth.AUTH_URL });
            String url = getPodcastHelper().getAuthrizedURL(selectionArgs[0]);
            cursor.addRow(new Object[] { 0, url });
            return cursor;
        }
        case CODE_RESET: {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            getDBHelper().reset(db);
            //reset();
        }
            break;
        case CODE_FILE: {
            return queryFile(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        case CODE_FILE_ID: {
            return queryFileId(uri, projection, selection, selectionArgs,
                    sortOrder);
        }
        }
        return null;
    }

    private Cursor queryFileId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        // TODO Auto-generated method stub
        return null;
    }

    private Cursor queryFile(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        // TODO Auto-generated method stub
        return null;
    }


    private Cursor queryDownload(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        // TODO Auto-generated method stub
        return null;
    }

    private Cursor queryOrderAudioId(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        long id = ContentUris.parseId(uri);
        Cursor cur = db.query(TableConsts.TBNAME_ORDERLIST, projection,
                BaseColumns._ID + " = ?", new String[] { Long.toString(id) },
                null, null, sortOrder);
        return cur;
    }

    private Cursor queryOrderAudio(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        return db.query(TableConsts.TBNAME_ORDERLIST, projection, selection,
                selectionArgs, null, null, sortOrder);
    }

    private Cursor queryGenresmember(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        String[] sel = selection.split("=");
        String genres_key = null;
        for (int i = 0; i < sel.length; i++) {
            if (sel[i].indexOf(AudioGenresMember.GENRE_ID) != -1) {
                genres_key = selectionArgs[i];
                break;
            }
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_AUDIO);
        String limit = null;

        StringBuilder where = new StringBuilder();
        where.append(TableConsts.GENRES_TAGS).append(
                " like '%/" + genres_key + "/%'");

        int pos = selection != null ? selection.indexOf("AND LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, PodcastHelper.MEDIA_FIELDS, where.toString(), null, null,
                null, sortOrder, null);
        return cur;
    }

    private Cursor queryGenres(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_GENRES);
        try {
            db = getDBHelper().getReadableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    private Cursor queryAlbum(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_ALBUM);
        try {
            db = getDBHelper().getReadableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    private Cursor queryVideoId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            long id = ContentUris.parseId(uri);
            db = getDBHelper().getReadableDatabase();
            Cursor cur = db.query(TableConsts.TBNAME_VIDEO, projection,
                    BaseColumns._ID + " = ?",
                    new String[] { Long.toString(id) }, null, null, sortOrder);
            return cur;
        } finally {

        }
    }

    private Cursor queryVideo(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getReadableDatabase();
            Cursor cur = db.query(TableConsts.TBNAME_VIDEO, projection,
                    selection, selectionArgs, null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    private Cursor queryFavorite(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        MatrixCursor ret = new MatrixCursor(new String[] { BaseColumns._ID,
                TableConsts.FAVORITE_POINT });
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        Cursor cursor = null;
        try {
            long id = Long.parseLong(selectionArgs[0]);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                cursor = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT },
                        AudioMedia._ID + " = ?",
                        new String[] { Long.toString(id) }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                cursor = db.query(TableConsts.TBNAME_ALBUM, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT },
                        AudioAlbum._ID + " = ?",
                        new String[] { Long.toString(id) }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                cursor = db.query(TableConsts.TBNAME_ARTIST, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT },
                        AudioArtist._ID + " = ?",
                        new String[] { Long.toString(id) }, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                ret.addRow(new Object[] {
                        id,
                        cursor.getInt(cursor
                                .getColumnIndex(TableConsts.FAVORITE_POINT)) });
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    private Cursor queryPlaylistmember(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        long playlist_id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST_AUDIO);
        qb.appendWhere(AudioPlaylistMember.PLAYLIST_ID + " = " + playlist_id);
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();

            // PlaylistMemberからデータを取得し、合致するMedia情報をもとに、返事をかえす
            MatrixCursor ret_cursor = new MatrixCursor(QUERY_PLAYLIST_MEMBER_COLS);
            cur = qb.query(db, new String[] { BaseColumns._ID,
                    AudioPlaylistMember.AUDIO_ID,
                    AudioPlaylistMember.PLAY_ORDER }, selection, selectionArgs,
                    null, null, sortOrder);
            if (cur != null && cur.moveToFirst()) {
                do {
                    long mediaId = cur.getLong(cur
                            .getColumnIndex(AudioPlaylistMember.AUDIO_ID));
                    Hashtable<String, String> tbl = getMedia(getContext(),
                            QUERY_AUDIO_COLS, mediaId);
                    if (!tbl.isEmpty()) {
                        ret_cursor
                                .addRow(new Object[] {
                                        // Long.parseLong(tbl.get(BaseColumns._ID)),
                                        cur.getLong(cur
                                                .getColumnIndex(BaseColumns._ID)),
                                        tbl.get(AudioMedia._ID),
                                        tbl.get(AudioMedia.TITLE),
                                        tbl.get(AudioMedia.TITLE_KEY),
                                        tbl.get(AudioMedia.DATA),
                                        parseLong(tbl.get(AudioMedia.DURATION)),
                                        tbl.get(AudioMedia.ARTIST),
                                        tbl.get(AudioMedia.ARTIST_KEY),
                                        tbl.get(AudioMedia.ALBUM),
                                        tbl.get(AudioMedia.ALBUM_KEY),
                                        // tbl.get(AudioMedia.ALBUM_ART),
                                        cur.getInt(cur
                                                .getColumnIndex(AudioPlaylistMember.PLAY_ORDER)), });
                    }
                } while (cur.moveToNext());
            }

            return ret_cursor;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private Cursor queryPlaylist(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST);
        try {
            db = getDBHelper().getReadableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    private Cursor queryArtist(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = null;

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_ARTIST);
        try {
            db = getDBHelper().getReadableDatabase();

            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    private Cursor queryMediaId(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        long id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_AUDIO);
        return qb.query(db, PodcastHelper.MEDIA_FIELDS, AudioMedia._ID + " = ?",
                new String[] { Long.toString(id) }, null, null, sortOrder);
    }

    private Cursor queryMedia(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        
        boolean search_albumsongs = selection != null ? selection
                .indexOf(AudioAlbum.ALBUM_KEY) != -1 : false;
        
        if (search_albumsongs) {
            String[] sel = selection.split("=");
            String album_key = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioMedia.ALBUM_KEY) != -1) {
                    album_key = selectionArgs[i];
                    break;
                }
            }
            if (album_key != null) {
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                Cursor cur=null;
                String site_link = null;
                boolean isSuccess = false;
                try {
                    cur = getDBHelper().findAlbum(db, album_key);
                    if (cur != null && cur.moveToFirst()) {
                        site_link = cur.getString(cur.getColumnIndex(TableConsts.SITE_LINK));
                        long updDate = cur.getLong(cur.getColumnIndex(AudioAlbum.DATE_MODIFIED));
                        long initFlg = cur.getLong(cur.getColumnIndex(TableConsts.ALBUM_INIT_FLG));
                        //最終更新日時より新しい更新日付の場合は全取得を行う。
                        //そうでない場合は通常検索を行う
                        try {
                            URL url = new URL(site_link);
                            
                            ArrayList<Channel> channels = getPodcastHelper().getPodcastFeed(url, true);
                            if(channels!=null){
                                for(Channel c : channels){
                                    if(c.getPubDate().getTime()>updDate || initFlg == 0){
                                        return getPodcastHelper().getFeed(db, url, album_key);
                                    }
                                }
                                isSuccess = true;
                            }
                        } catch (MalformedURLException e) {
                        }
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    if(isSuccess){
                        if(site_link!=null){
                            //Clickする度にポイントアップする
                            podcastHelper.raiseSite(site_link);
                        }
                    }
                }
            }
        }
        
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_AUDIO);
        try {
            db = getDBHelper().getReadableDatabase();
            Cursor cur = qb.query(db, projection, selection, selectionArgs,
                    null, null, sortOrder);
            return cur;
        } finally {
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
        case CODE_MEDIA: {
            return updateMedia(uri, values, selection, selectionArgs);
        }
        case CODE_MEDIA_ID: {
            return updateMediaId(uri, values, selection, selectionArgs);
        }
        case CODE_ALBUMS: {
            return updateAlbum(uri, values, selection, selectionArgs);
        }
        case CODE_ALBUMS_ID: {
            return updateAlbumId(uri, values, selection, selectionArgs);
        }
        case CODE_ARTIST: {
            return updateArtist(uri, values, selection, selectionArgs);
        }
        case CODE_ARTIST_ID: {
            return updateArtistId(uri, values, selection, selectionArgs);
        }
        case CODE_PLAYLIST: {
            return updatePlaylist(uri, values, selection, selectionArgs);
        }
        case CODE_PLAYLISTMEMBER: {
            return updatePlaylistmember(uri, values, selection, selectionArgs);
        }
        case CODE_FAVORITE: {
            return updateFavorite(uri, values, selection, selectionArgs);
        }
        case CODE_VIDEO: {
            return updateVideo(uri, values, selection, selectionArgs);
        }
        case CODE_VIDEO_ID: {
            return updateVideoId(uri, values, selection, selectionArgs);
        }
        case CODE_DOWNLOAD: {
            return updateDownload(uri, values, selection, selectionArgs);
        }
        case CODE_GENRES: {
            return updateGenres(uri, values, selection, selectionArgs);
        }
        }
        return 0;
    }

    private int updateGenres(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db.update(TableConsts.TBNAME_GENRES, values, selection,
                selectionArgs);
    }

    private int updateDownload(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private int updateVideoId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        // TODO Auto-generated method stub
        return 0;
    }

    private int updateVideo(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = null;
        try {
            db = getDBHelper().getWritableDatabase();

            return db.update(TableConsts.TBNAME_VIDEO, values, selection,
                    selectionArgs);
        } finally {
        }
    }

    private int updateFavorite(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = Long.parseLong(selectionArgs[0]);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] { Long.toString(id) });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] { Long.toString(id) });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] { Long.toString(id) });
            }
            db.setTransactionSuccessful();
            return 1;
        } finally {
            db.endTransaction();
        }
    }

    private int updatePlaylistmember(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();
            cur = db.query(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] { MediaConsts.AudioPlaylistMember.AUDIO_ID },
                    selection, selectionArgs, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return db.update(TableConsts.TBNAME_PLAYLIST_AUDIO, values,
                        selection, selectionArgs);
            } else {
                long id = db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null,
                        values);
                return 1;
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private int updatePlaylist(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db.update(TableConsts.TBNAME_PLAYLIST, values, selection,
                selectionArgs);
    }

    private int updateArtistId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ARTIST, values, AudioArtist._ID
                        + " = ?", new String[] { Long.toString(id) });

                cur = db.query(TableConsts.TBNAME_ARTIST,
                        new String[] { AudioArtist.ARTIST_KEY },
                        AudioArtist._ID + " = ?",
                        new String[] { Long.toString(id) }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String artistKey = cur.getString(cur
                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ARTIST_INIT_FLG)
                            && values.getAsInteger(TableConsts.ARTIST_INIT_FLG) == 0) {
                        // Mediaのキャッシュファイルを削除
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioArtist.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ARTIST_KEY + " = ?",
                                new String[] { artistKey });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
                getContext().getContentResolver().notifyChange(
                        MEDIA_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ARTIST_CONTENT_URI, null);
            }
        }
    }

    private int updateArtist(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db.update(TableConsts.TBNAME_ARTIST, values, selection,
                selectionArgs);
    }

    private int updateAlbumId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ARTIST, values, AudioArtist._ID
                        + " = ?", new String[] { Long.toString(id) });

                cur = db.query(TableConsts.TBNAME_ARTIST,
                        new String[] { AudioArtist.ARTIST_KEY },
                        AudioArtist._ID + " = ?",
                        new String[] { Long.toString(id) }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String artistKey = cur.getString(cur
                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ARTIST_INIT_FLG)
                            && values.getAsInteger(TableConsts.ARTIST_INIT_FLG) == 0) {
                        // Mediaのキャッシュファイルを削除
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioArtist.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ARTIST_KEY + " = ?",
                                new String[] { artistKey });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
                getContext().getContentResolver().notifyChange(
                        MEDIA_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ARTIST_CONTENT_URI, null);
            }
        }
    }

    private int updateAlbum(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db.update(TableConsts.TBNAME_ALBUM, values, selection,
                selectionArgs);
    }

    private int updateMediaId(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getReadableDatabase();
                Cursor cur = null;
                try {
                    cur = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                            AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ALBUM,
                            AudioMedia.ARTIST }, "_ID = ?",
                            new String[] { Long.toString(id) }, null, null,
                            null);
                    if (cur != null && cur.moveToFirst()) {
                        return setId3Tag(cur, enc);
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    getContext().getContentResolver().notifyChange(
                            MEDIA_CONTENT_URI, null);
                }
            }
            return 0;
        } else if (values != null
                && values.get(TableConsts.AUDIO_CACHE_FILE) != null) {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            boolean hasTransaction = false;
            boolean needNotify = false;
            try {
                long id = ContentUris.parseId(uri);
                String fname = values.getAsString(TableConsts.AUDIO_CACHE_FILE);
                cur = db.query(TableConsts.TBNAME_AUDIO,
                        new String[] { AudioMedia.DURATION }, "_ID = ?",
                        new String[] { Long.toString(id) }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    long duration = cur.getLong(cur
                            .getColumnIndex(AudioMedia.DURATION));
                    if (duration <= 0) {
                        // データを取得しなおして設定する
                        File cacheFile = new File(cachedMusicDir, fname);
                        Map<String, Object> ret = getID3TagInfo(cacheFile);

                        String album = (String) ret.get("album");
                        String artist = (String) ret.get("artist");
                        String title = (String) ret.get("title");
                        String track = (String) ret.get("track");
                        if (album != null && album.length() > 0) {
                            values.put(AudioMedia.ALBUM, album);
                        }
                        if (artist != null && artist.length() > 0) {
                            values.put(AudioMedia.ARTIST, artist);
                        }
                        if (title != null && title.length() > 0) {
                            values.put(AudioMedia.TITLE, title);
                        }
                        if (track != null && track.length() > 0) {
                            values.put(AudioMedia.TRACK, track);
                        }
                        values.put(AudioMedia.DURATION,
                                (Long) ret.get("duration"));

                        // アルバムも更新しておく?うーんやめとく
                        needNotify = true;
                    }
                }

                db.beginTransaction();
                hasTransaction = true;
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] { Long.toString(id) });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (hasTransaction) {
                    if (needNotify) {
                        getContext().getContentResolver().notifyChange(
                                MEDIA_CONTENT_URI, null);
                    }
                    db.endTransaction();
                }
            }
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] { Long.toString(id) });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                db.endTransaction();
            }
        }
    }

    private int updateMedia(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        return db.update(TableConsts.TBNAME_AUDIO, values, selection,
                selectionArgs);
    }

}
