package jp.co.kayo.android.localplayer.ds.podcast;

import android.os.Handler;

public interface DialogCallback {
    public void onExecute(Handler handler);
}
