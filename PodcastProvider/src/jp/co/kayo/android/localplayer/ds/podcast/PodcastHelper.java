package jp.co.kayo.android.localplayer.ds.podcast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;

import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Channel;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Image;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Item;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Owner;

import org.apache.commons.httpclient.contrib.ssl.EasySSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Xml;

public class PodcastHelper {
    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int SOCKET_TIMEOUT = 20000;
    private static final int MCC_TIMEOUT = 5000;
    private final int SOCK_BUFSIZE = 4096;

    private final int CHANNEL_TAG       = 0;
    private final int IMAGE_TAG         = 1;
    private final int OWNER_TAG         = 2;
    private final int ITME_TAG          = 3;
    
    private volatile SchemeRegistry mSchemeRegistry;
    private volatile DocumentBuilder mDocBuilder = null;
    private BasicHttpParams mParams = null;
    private BasicHttpContext mHttpContext;

    private boolean mForceClose = false;

    private String mErrorText;
    private String mErrorCode;
    
    public static final String[] MEDIA_FIELDS = new String[] { AudioMedia._ID,
        AudioMedia.TITLE, AudioMedia.MEDIA_KEY, AudioMedia.TITLE_KEY,
        AudioMedia.DURATION, AudioMedia.DATA, AudioMedia.ARTIST,
        AudioMedia.ARTIST_KEY, AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
        AudioMedia.TRACK, AudioMedia.YEAR,
        AudioMedia.DATE_ADDED, AudioMedia.DATE_MODIFIED,
        TableConsts.AUDIO_CACHE_FILE, TableConsts.GENRES_TAGS,
        TableConsts.FAVORITE_POINT };
    
    private static String getFilename(String uri) {
        if (uri != null) {
            String ret = uri.replaceAll("auth=[^&]+", "");
            if (ret.equals(uri)) {
                ret = uri.replaceAll("ssid=[^&]+", "");
            }

            String fname = "media" + Integer.toString(ret.hashCode()) + ".dat";
            return fname;
        } else {
            return null;
        }
    }
    
    public static File getCacheFile(String uri) {
        Logger.d("getCacheFile uri=" + uri);
        String s = getFilename(uri);
        if (s != null) {
            File cacheFile = new File(PodcastContentProvider.cachedMusicDir, s);
            Logger.d("getCacheFile trn=" + cacheFile.getPath());
            return cacheFile;
        }
        return null;
    }

    public void forceClose() {
        mForceClose = true;
    }

    public String getErrorText() {
        return mErrorText;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    private void errorReset() {
        mErrorText = null;
        mErrorCode = null;
    }

    public boolean isLoggdIn() {
        // TODO Auto-generated method stub
        return false;
    }

    public String getAuthrizedURL(String string) {
        return string;
    }

    synchronized SchemeRegistry getRegistry() {
        if (mSchemeRegistry == null) {
            mSchemeRegistry = new SchemeRegistry();
            // mSchemeRegistry.register(new Scheme("http",
            // PlainSocketFactory.getSocketFactory(), 80));
            // mSchemeRegistry.register(new Scheme("https",
            // SSLSocketFactory.getSocketFactory(), 443));
            // http scheme
            mSchemeRegistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            // https scheme
            mSchemeRegistry.register(new Scheme("https",
                    new EasySSLSocketFactory(), 443));
        }
        return mSchemeRegistry;
    }

    synchronized BasicHttpParams getHttpParams() {
        if (mParams == null) {
            mParams = new BasicHttpParams();
            mParams.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            // mParams.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
            // new ConnPerRouteBean(30));
            mParams.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setSocketBufferSize(mParams, SOCK_BUFSIZE); // ソケットバッファサイズ
                                                                             // 4KB
            HttpConnectionParams.setSoTimeout(mParams, SOCKET_TIMEOUT); // ソケット通信タイムアウト20秒
            HttpConnectionParams.setConnectionTimeout(mParams, SOCKET_TIMEOUT); // HTTP通信タイムアウト20秒
            HttpProtocolParams.setContentCharset(mParams, "UTF-8"); // 文字コードをUTF-8と明示
            // HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_0);

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            BasicScheme basicAuth = new BasicScheme();
            mHttpContext = new BasicHttpContext();
            mHttpContext.setAttribute("http.auth.credentials-provider",
                    credentialsProvider);
            mHttpContext.setAttribute("preemptive-auth", basicAuth);
        }
        return mParams;
    }

    public String postData(String url, List<NameValuePair> nameValuePairs) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);
        String json=null;
        try {
            if(nameValuePairs!=null){
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            }

            HttpResponse response = httpclient.execute(httppost);
            if (response.getStatusLine().getStatusCode() < 400){  
                InputStream objStream = response.getEntity().getContent();  
                InputStreamReader objReader = new InputStreamReader(objStream);  
                BufferedReader objBuf = new BufferedReader(objReader);  
                StringBuilder objJson = new StringBuilder();  
                String sLine;  
                while((sLine = objBuf.readLine()) != null){  
                    objJson.append(sLine);  
                }  
                json = objJson.toString();  
                objStream.close();  
            } 
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
            Logger.e("initialize error", e);
        }
        finally{
            httpclient.getConnectionManager().shutdown();
        }
        return json;
    } 
    
    public XMLHandler parseXml(URL url, XMLHandler handler) {
        errorReset();
        InputStream in = null;
        HttpURLConnection connection = null;
        ThreadSafeClientConnManager cm = null;
        try {
            XmlPullParser parser = Xml.newPullParser();
            if (Build.VERSION.SDK_INT <= 8) {
                cm = new ThreadSafeClientConnManager(getHttpParams(),
                        getRegistry());
                DefaultHttpClient client = new DefaultHttpClient(cm,
                        getHttpParams());

                HttpGet get = new HttpGet(url.toString());
                get.setHeader("Host", url.getHost());
                get.setHeader("User-Agent", "Android-JustPlayer");
                HttpResponse httpResponse = client.execute(get, mHttpContext);
                if (httpResponse != null
                        && httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    in = httpEntity.getContent();
                }
            } else {
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(CONNECTION_TIMEOUT);
                int responseCode = connection.getResponseCode();

                // クライアントにHTTPレスポンスを書きこむ
                if (responseCode == 200 || responseCode == 206
                        || responseCode == 203) {
                    in = connection.getInputStream();
                }
            }

            if (in != null) {
                parser.setInput(in, "UTF-8");
                int eventType = parser.getEventType();
                mForceClose = false;
                boolean isXMLContents = false;
                while (eventType != XmlPullParser.END_DOCUMENT && !mForceClose) {
                    switch (eventType) {
                    case XmlPullParser.END_TAG: {
                        if (handler.endTag(parser) != true) {
                            return handler;
                        }
                    }
                        break;
                    case XmlPullParser.START_TAG: {
                        isXMLContents = true;
                        if (handler.startTag(parser) != true) {
                            return handler;
                        }
                    }
                        break;
                    }
                    eventType = parser.next();
                }
                if (isXMLContents != true) {
                    mErrorText = "XML内にタグがひとつもありません。";
                    mErrorCode = "404";
                }
            } else {
                mErrorText = "レスポンスが取得できません。";
                mErrorCode = "404";
            }
        } catch (java.net.SocketTimeoutException e) {
            Logger.d("Socket Timeout");
            mErrorText = e.getMessage();
            mErrorCode = "451";
            handler = null;
        } catch (Exception e) {
            mErrorText = e.getMessage();
            Logger.d("error url =" + url.toString());
            Logger.e("(Exception) parse xml:", e);
            handler = null;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    Logger.e("connection.disconnect", e);
                }
                connection = null;
            }
            if (cm != null) {
                cm.shutdown();
            }
        }
        return handler;
    }
    
    public ArrayList<Channel> getPodcastFeed(final URL url, final boolean headerOnly){
        final ArrayList<Channel> channels = new ArrayList<Channel>();
        
        XMLHandler ret = parseXml(url, new XMLHandler() {
            Stack<Integer> tagStack = new Stack<Integer>();
            
            private Channel getCurrent(){
                if(channels.size()>0){
                    return channels.get(channels.size()-1);
                }
                return null;
            }
            
            @Override
            public boolean startTag(XmlPullParser parser) {
                String name = parser.getName();
                Logger.d("starttag:"+name);
                if("channel".endsWith(name)){
                    Channel c = new Channel();
                    c.site = url.toString();
                    channels.add(c);
                    tagStack.push(CHANNEL_TAG);
                }
                if("image".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String href = XMLUtils.getAttributeValue(parser, "href");
                        if(href!=null && href.length()>0){
                            c.itunes_image = href;
                        }
                        else{
                            Image i = new Image();
                            c.image = i;
                            tagStack.push(IMAGE_TAG);
                        }
                    }
                    else{
                        return false;
                    }
                }
                if("owner".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        Owner o = new Owner();
                        c.itunes_owner = o;
                        tagStack.push(OWNER_TAG);
                    }
                    else{
                        return false;
                    }
                }
                else if("title".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(value.length()>0){
                            if(tagStack.lastElement() == CHANNEL_TAG){
                                c.title = value;
                            }
                            else if(tagStack.lastElement() == IMAGE_TAG){
                                c.image.title = value;
                            }
                            else if(tagStack.lastElement() == ITME_TAG){
                                c.items.get(c.items.size()-1).title =value;
                            }
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("link".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.link = value;
                        }
                        else if(tagStack.lastElement() == IMAGE_TAG){
                            c.image.link = value;
                        }
                        else if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).link = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("description".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.description = value;
                        }
                        else if(tagStack.lastElement()== ITME_TAG){
                            c.items.get(c.items.size()-1).description = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("language".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        c.language = value;
                    }
                    else{
                        return false;
                    }
                }
                else if("copyright".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        c.copyright = value;
                    }
                    else{
                        return false;
                    }
                }
                else if("url".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == IMAGE_TAG){
                            c.image.url = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("pubDate".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.pubDate = value;
                        }
                        else if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).pubDate = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("subtitle".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.itunes_subtitle = value;
                        }
                        else if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).itunes_subtitle = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("author".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(value.length()>0){
                            if(tagStack.lastElement() == CHANNEL_TAG){
                                c.itunes_author = value;
                            }
                            else if(tagStack.lastElement() == ITME_TAG){
                                c.items.get(c.items.size()-1).itunes_author = value;
                            }
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("summary".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.itunes_summary = value;
                        }
                        else if(tagStack.lastElement()== ITME_TAG){
                            c.items.get(c.items.size()-1).itunes_summary = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                /*else if("image".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == CHANNEL_TAG){
                            c.itunes_image = value;
                        }
                    }
                    else{
                        return false;
                    }
                }*/
                else if("owner".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        c.itunes_owner = new Owner();
                        tagStack.push(OWNER_TAG);
                    }
                    else{
                        return false;
                    }
                }
                else if("name".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == OWNER_TAG){
                            c.itunes_owner.itunes_name = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("email".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == OWNER_TAG){
                            c.itunes_owner.itunes_email = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("category".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String category = XMLUtils.getAttributeValue(parser, "text");
                        if(category!=null && category.length()>0){
                            c.itunes_category.add(category);
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("enclosure".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).enclosure_url = XMLUtils
                                    .getAttributeValue(parser, "url");
                            c.items.get(c.items.size()-1).enclosure_length = XMLUtils
                                    .getAttributeValue(parser, "length");
                            c.items.get(c.items.size()-1).enclosure_type = XMLUtils
                                    .getAttributeValue(parser, "type");
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("guid".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).guid = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("duration".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).itunes_duration = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("keywords".endsWith(name)){
                    Channel c = getCurrent();
                    if(c!=null){
                        String value = XMLUtils.getTextValue(parser);
                        if(tagStack.lastElement() == ITME_TAG){
                            c.items.get(c.items.size()-1).itunes_keywords = value;
                        }
                    }
                    else{
                        return false;
                    }
                }
                else if("item".endsWith(name)){
                    if(headerOnly){
                        Logger.d("This is Header only");
                        return false;
                    }
                    else{
                        Channel c = getCurrent();
                        if(c!=null){
                            Item o = new Item();
                            c.items.add(o);
                            tagStack.push(ITME_TAG);
                        }
                        else{
                            return false;
                        }       
                    }
                }
                
                return true;
            }
            
            @Override
            public boolean endTag(XmlPullParser parser) {
                String name = parser.getName();
                Logger.d("endtag:"+name);
                if("channel".equals(name)){
                    if(tagStack.lastElement() == CHANNEL_TAG){
                        tagStack.pop();
                    }
                }
                else if("image".equals(name)){
                    if(tagStack.lastElement() == IMAGE_TAG){
                        tagStack.pop();
                    }
                }
                else if("owner".equals(name)){
                    if(tagStack.lastElement() == OWNER_TAG){
                        tagStack.pop();
                    }
                }
                else if("item".equals(name)){
                    if(tagStack.lastElement() == IMAGE_TAG){
                        tagStack.pop();
                    }
                }
                return true;
            }
        });
        if(ret != null){
            return channels;
        }
        else{
            return null;
        }
    }
    
    public Cursor getFeed(SQLiteDatabase db, URL url, String albumKey){
        MatrixCursor cursor = new MatrixCursor(MEDIA_FIELDS);
        
        ArrayList<Channel> channels = getPodcastFeed(url, false);
        if(channels!=null){
            boolean hasTransaction = false;
            try{
                for(Channel c : channels){
                    c.verbose();
                    if(albumKey.equals(c.getAlbumKey())){
                        int track = c.items.size();
                        for(Item i : c.items){
                            i.verbose();
                            if(i.enclosure_url!=null && i.enclosure_url.length()>0){
                                cursor.addRow(new Object[]{
                                        (long)(i.enclosure_url.hashCode()&0xffffffffL), //AudioMedia._ID
                                        i.title,//AudioMedia.TITLE
                                        Integer.toString(i.enclosure_url.hashCode()),//AudioMedia.MEDIA_KEY
                                        Integer.toString(i.title.hashCode()),//AudioMedia.TITLE_KEY
                                        Long.parseLong(i.enclosure_length),//AudioMedia.DURATION
                                        i.enclosure_url,//DATA
                                        i.itunes_author!=null?i.itunes_author:c.itunes_author,//ARTIST
                                        Channel.getKey(c.itunes_author),//ARTIST_KEY
                                        c.title,//ALBUM
                                        Integer.toString(c.title.hashCode()),//ALBUM_KEY
                                        track,//TRACK
                                        Integer.toString(i.getPubDate().getYear()),//YEAR
                                        i.getPubDate().getTime(),//DATE_ADDED
                                        i.getPubDate().getTime(),//DATE_MODIFIED
                                        null,//AUDIO_CACHE_FILE
                                        null,//GENRES_TAGS
                                        0
                                });
                                
                                if(!hasTransaction){
                                    hasTransaction = true;
                                    db.beginTransaction();
                                    ContentValues values = new ContentValues();
                                    values.put(TableConsts.ARTIST_INIT_FLG, 1);
                                    db.update(TableConsts.TBNAME_ALBUM, values, AudioAlbum.ALBUM_KEY + " = ?", new String[]{albumKey});
                                }
                                ContentValues initialValues = new ContentValues();
                                initialValues.put(AudioMedia._ID, (long)(i.enclosure_url.hashCode()&0xffffffffL));
                                initialValues.put(AudioMedia.TITLE, i.title);
                                initialValues.put(AudioMedia.MEDIA_KEY, Integer.toString(i.enclosure_url.hashCode()));
                                initialValues.put(AudioMedia.TITLE_KEY, Integer.toString(i.title.hashCode()));
                                initialValues.put(AudioMedia.DURATION, i.enclosure_length);
                                initialValues.put(AudioMedia.DATA, i.enclosure_url);
                                initialValues.put(AudioMedia.ARTIST, i.itunes_author!=null?i.itunes_author:c.itunes_author);
                                initialValues.put(AudioMedia.ARTIST_KEY, c.getArtistKey());
                                initialValues.put(AudioMedia.ALBUM, c.title);
                                initialValues.put(AudioMedia.ALBUM_KEY, c.getAlbumKey());
                                initialValues.put(AudioMedia.TRACK, track);
                                String tagkeys = c.getTagKeys();
                                if(tagkeys!=null && tagkeys.length()>0){
                                    initialValues.put(TableConsts.GENRES_TAGS, tagkeys);
                                }
                                initialValues.put(AudioMedia.YEAR, Integer.toString(i.getPubDate().getYear()));
                                initialValues.put(AudioMedia.DATE_ADDED, i.getPubDate().getTime());
                                initialValues.put(AudioMedia.DATE_MODIFIED, i.getPubDate().getTime());
                                //キャッシュの有無
                                File cacheFile = getCacheFile(i.enclosure_url);
                                if(cacheFile!=null && cacheFile.exists()){
                                    initialValues.put(TableConsts.AUDIO_CACHE_FILE, cacheFile.getName());
                                }
                                else{
                                    initialValues.put(TableConsts.AUDIO_CACHE_FILE, (String)null);
                                }
                                
                                db.replace(TableConsts.TBNAME_AUDIO, null, initialValues);
                                track--;
                            }
                        }
                    }
                }
            }
            finally{
                if(hasTransaction){
                    db.setTransactionSuccessful();
                    db.endTransaction();
                }
            }
        }
        
        return cursor;
    }
    
    public void raiseSite(final String site){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("site", site));
                
                String json = new PodcastHelper().postData(SystemConsts.SITE+"/raise", nameValuePairs);
            }
        });
        t.start();
    }
}
