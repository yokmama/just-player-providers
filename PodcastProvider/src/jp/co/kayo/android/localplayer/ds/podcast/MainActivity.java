package jp.co.kayo.android.localplayer.ds.podcast;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.ds.podcast.bean.Channel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity implements OnClickListener, LoaderCallbacks<Cursor>, OnItemLongClickListener, OnItemClickListener {
    
    private Button btnAdd;
    private PodcastAdapter adapter;
    private ListView listView;
    private EditText editPodcastFeed;
    private ViewCache viewCache;
    
    private final int EVT_RELOAD = 0;
    private final int EVT_LIST = 1;
    private final int EVT_TOAST = 2;
    
    Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
            case EVT_RELOAD:{getSupportLoaderManager().restartLoader(R.layout.main, null, MainActivity.this);}break;
            case EVT_LIST:{showList((String)msg.obj);}break;
            case EVT_TOAST:{Toast.makeText(MainActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();}break;
            }
        }
    };
    
    private boolean findAlbum(String site){
        Cursor cursor = null;
        try{
            cursor = getContentResolver().query(PodcastContentProvider.ALBUM_CONTENT_URI, null, TableConsts.SITE_LINK + " = ?", new String[]{site}, null);
            boolean nothing = false;
            if(cursor !=null && cursor.moveToFirst()){
                return true;
            }
            return false;
        }
        finally{
            if(cursor!=null){
                cursor.close();
            }
        }
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ソフトウエアキーボードを解除
        this.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setContentView(R.layout.main);
        listView = (ListView)findViewById(android.R.id.list);
        editPodcastFeed = (EditText) findViewById(R.id.editPodcastFeed);
        viewCache = (ViewCache)getSupportFragmentManager().findFragmentByTag("tag_cache");
        findViewById(R.id.btnAdd).setOnClickListener(this);
        findViewById(R.id.btnSelect).setOnClickListener(this);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        
    }

    
    
    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        super.onActivityResult(arg0, arg1, arg2);
        if(arg2 != null){
            String site = arg2.getStringExtra("site");
            //editPodcastFeed.setText(site);
            Bundle bundle = new Bundle();
            bundle.putString("url", site);
            bundle.putBoolean("regist", false);
            showDialog(100, bundle);
            //onCreateDialogOwn(102, bundle);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        
        getSupportLoaderManager().initLoader(R.layout.main, null, this);
    }

    
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAdd){
            final String url = editPodcastFeed.getText().toString();
            if(url.length()>0){
                try{
                    //check
                    if(!findAlbum(url)){
                        //post
                        Bundle bundle = new Bundle();
                        bundle.putString("url", url);
                        bundle.putBoolean("regist", true);
                        showDialog(100, bundle);
                        //onCreateDialogOwn(100, bundle);
                    }
                    else{
                        Toast.makeText(this, getString(R.string.err_registeredurl), Toast.LENGTH_LONG).show();
                    }
                }
                finally{
                    editPodcastFeed.setText("");
                }
            }
        }
        else if(v.getId() == R.id.btnSelect){
            //list
            //post
            showDialog(101);
            //onCreateDialogOwn(101, null);
        }
    }
    
    protected Dialog onCreateDialog(final int id, final Bundle args) {
        if(id == 100){
            MyProgressDialog dialog = new MyProgressDialog(MainActivity.this, new DialogCallback() {
                @Override
                public void onExecute(Handler handler) {
                    try{
                        registPodcast(args.getString("url"), args.getBoolean("regist"));
                    }
                    finally{
                        removeDialog(id);
                    }
                }
            });
            dialog.start();
            //dialog.show();
            return dialog;
        }
        else if(id == 101){
            MyProgressDialog dialog = new MyProgressDialog(MainActivity.this, new DialogCallback() {
                @Override
                public void onExecute(Handler handler) {
                    try{
                        listPodcast();
                    }
                    finally{
                        removeDialog(id);
                    }
                }
            });
            dialog.start();
            //dialog.show();
            return dialog;
        }
        return null;
    }
    
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(adapter == null){
            adapter = new PodcastAdapter(this, null, true, viewCache);
            listView.setAdapter(adapter);
        }
        
        return new CursorLoader(this, PodcastContentProvider.ALBUM_CONTENT_URI,
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor newCursor) {
        Cursor cur =  null;
        if(Build.VERSION.SDK_INT<11){
            adapter.changeCursor(newCursor);
            cur =  adapter.getCursor();
        }
        else{
            cur = adapter.swapCursor(newCursor);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        // TODO Auto-generated method stub
        
    }
    
    private void registPodcast(String url, boolean regist){
        if(url.length()>0){
            Cursor cur = null;
            PodcastHelper helper = new PodcastHelper();
            try {
                List<Channel> channels = helper.getPodcastFeed(new URL(url), true);
                if(channels!=null){
                    for(Channel c : channels){
                        c.verbose();
                        //Serverへ登録
                        if(regist){
                            boolean ret = rgistServer(url, c);
                        }
                        if(!findAlbum(url)){
                            //Localへアルバム登録
                            ContentValues values = new ContentValues();
                            values.put(AudioAlbum.ALBUM, c.title);
                            values.put(AudioAlbum.ALBUM_KEY, c.getAlbumKey());
                            values.put(AudioAlbum.ARTIST, c.itunes_author);
                            values.put(TableConsts.SITE_LINK, url);
                            values.put(TableConsts.PODCAST_LINK, c.link);
                            values.put(TableConsts.PODCAST_SUBTITLE, c.itunes_subtitle);
                            values.put(TableConsts.PODCAST_SUMMARY, c.itunes_summary);
                            values.put(AudioAlbum.DATE_MODIFIED, c.getPubDate().getTime());
                            values.put(AudioAlbum.ALBUM_ART, c.getImage());
                            values.put(TableConsts.ALBUM_INIT_FLG, 0);
                            getContentResolver().insert(PodcastContentProvider.ALBUM_CONTENT_URI, values);
                            //Localへアーティスト登録
                            String artist = c.itunes_author;
                            if(artist!=null && artist.length()>0){
                                cur = getContentResolver().query(PodcastContentProvider.ARTIST_CONTENT_URI, new String[]{AudioArtist._ID, AudioArtist.NUMBER_OF_ALBUMS}, AudioArtist.ARTIST + " = ?", new String[]{artist}, null);
                                if(cur!=null){
                                    if(cur.moveToFirst() && cur.getCount()>0){
                                        //登録不要
                                        long id = cur.getLong(cur.getColumnIndex(AudioArtist._ID));
                                        int num = cur.getInt(cur.getColumnIndex(AudioArtist.NUMBER_OF_ALBUMS)); 
                                        ContentValues artist_values = new ContentValues();
                                        artist_values.put(AudioArtist.NUMBER_OF_ALBUMS, num++);
                                        getContentResolver().update(ContentUris.withAppendedId(PodcastContentProvider.ARTIST_CONTENT_URI, id), artist_values, null, null);
                                    }
                                    else{
                                        Calendar cal = Calendar.getInstance();
                                        ContentValues artist_values = new ContentValues();
                                        artist_values.put(AudioArtist.ARTIST, artist);
                                        artist_values.put(AudioArtist.ARTIST_KEY, c.getArtistKey());
                                        artist_values.put(AudioArtist.DATE_ADDED, cal.getTimeInMillis());
                                        artist_values.put(AudioArtist.DATE_MODIFIED, cal.getTimeInMillis());
                                        artist_values.put(AudioArtist.NUMBER_OF_ALBUMS, 1);
                                        artist_values.put(AudioArtist.NUMBER_OF_TRACKS, 0);
                                        getContentResolver().insert(PodcastContentProvider.ARTIST_CONTENT_URI, artist_values);
                                    }
                                    cur.close();
                                    cur = null;
                                }
                            }
                            
                            //Localへジャンル登録
                            for(String category : c.itunes_category){
                                if(category!=null && category.length()>0){
                                    cur = getContentResolver().query(PodcastContentProvider.GENRES_CONTENT_URI, new String[]{AudioGenres._ID}, AudioGenres.NAME + " = ?", new String[]{category}, null);
                                    if(cur!=null){
                                        if(cur.moveToFirst() && cur.getCount()>0){
                                            //登録不要
                                        }
                                        else{
                                            Calendar cal = Calendar.getInstance();
                                            ContentValues genres_values = new ContentValues();
                                            genres_values.put(AudioGenres.NAME, category);
                                            genres_values.put(AudioGenres.GENRES_KEY, Integer.toString(category.hashCode()));
                                            genres_values.put(AudioGenres.DATE_ADDED, cal.getTimeInMillis());
                                            genres_values.put(AudioGenres.DATE_MODIFIED, cal.getTimeInMillis());
                                            getContentResolver().insert(PodcastContentProvider.GENRES_CONTENT_URI, genres_values);
                                        }
                                        cur.close();
                                        cur = null;
                                    }
                                }
                            }
                            
                            mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, getString(R.string.msg_registered)));
                            mHandler.sendEmptyMessage(EVT_RELOAD);
                        }
                        else{
                            mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, getString(R.string.err_registeredurl)));
                        }
                    }
                }
                else{
                    if(helper.getErrorText()!=null && helper.getErrorText().length()>0){
                        mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, helper.getErrorText()));
                    }
                    else{
                        mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, getString(R.string.err_wrongurl)));
                    }
                }
            } catch (MalformedURLException e) {
                mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, getString(R.string.err_malformedurl)));
            }
            finally{
                if(cur!=null){
                    cur.close();
                }
            }
        }
    }
    
    private void listPodcast(){
        String json = new PodcastHelper().postData(SystemConsts.SITE+"/listPodcast", null);
        if(json!=null && json.length()>0){
            Logger.d("json="+json);
            Message msg = mHandler.obtainMessage(EVT_LIST);
            msg.obj = json;
            mHandler.sendMessage(msg);
        }
        else{
            mHandler.sendMessage(mHandler.obtainMessage(EVT_TOAST, getString(R.string.err_serverconnection)));
        }
    }
    
    private void showList(String json){
        Intent i = new Intent(this, SelectActivity.class);
        i.putExtra("json", json);
        startActivityForResult(i, 0);
    }
    
    private boolean rgistServer(String site, Channel c) {
        
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("title", c.title));
        nameValuePairs.add(new BasicNameValuePair("description", c.description));
        nameValuePairs.add(new BasicNameValuePair("language",c.language));
        nameValuePairs.add(new BasicNameValuePair("owner", c.itunes_author));
        nameValuePairs.add(new BasicNameValuePair("site", site));
        nameValuePairs.add(new BasicNameValuePair("image", c.getImage()));
        nameValuePairs.add(new BasicNameValuePair("category", c.getCategory()));
        
        String json = new PodcastHelper().postData(SystemConsts.SITE+"/regist", nameValuePairs);
        if(json!=null){
            try {
                JSONObject jsonobj = new JSONObject(json.toString());
                if(jsonobj!=null){
                    String result = (String)jsonobj.get("result");
                    if(result!=null&&result.equals("true")){
                        return true;
                    }
                }
            } catch (JSONException e) {
            }
        }
        
        return false;
    }



    @Override
    public boolean onItemLongClick(AdapterView<?> l, View arg1, int position,
            long id) {
        Cursor cursor = (Cursor) adapter.getItem(position);
        final long albumId = cursor.getLong(cursor.getColumnIndex(AudioAlbum._ID));
        String title = cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM));
        
        showConfiumDialog(title, getString(R.string.msg_confium_delete), new DialogCallback() {
            @Override
            public void onExecute(Handler handler) {
                getContentResolver().delete(
                        ContentUris.withAppendedId(PodcastContentProvider.ALBUM_CONTENT_URI, albumId), null, null);
                getSupportLoaderManager().restartLoader(R.layout.main, null, MainActivity.this);
            }
        });
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Cursor cursor = (Cursor) adapter.getItem(position);
        String title = cursor.getString(cursor.getColumnIndex(AudioAlbum.ALBUM));
        String summary = cursor.getString(cursor.getColumnIndex(TableConsts.PODCAST_SUMMARY));
        showMessageDialog(title, summary);
    }

    private void showConfiumDialog(String title, String text, final DialogCallback callback){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // タイトルを設定
        alertDialogBuilder.setTitle(title);
        // メッセージを設定
        alertDialogBuilder.setMessage(text);
        // アイコンを設定
        alertDialogBuilder.setIcon( R.drawable.ic_launcher);
        // Positiveボタンとリスナを設定
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.onExecute(null);
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // ダイアログを表示
        alertDialogBuilder.create().show();
    }
    
    private void showMessageDialog(String title, String text){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // タイトルを設定
        alertDialogBuilder.setTitle(title);
        // メッセージを設定
        alertDialogBuilder.setMessage(text);
        // アイコンを設定
        alertDialogBuilder.setIcon( R.drawable.ic_launcher);
        // Positiveボタンとリスナを設定
        alertDialogBuilder.setPositiveButton(getString(R.string.btn_close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // ダイアログを表示
        alertDialogBuilder.create().show();
    }
}