package jp.co.kayo.android.localplayer.ds.ampache.util;

import javax.net.ssl.HttpsURLConnection;

import android.net.SSLCertificateSocketFactory;

public class SSLHelper {
    public void disableSSLVerifier() {
        HttpsURLConnection
                .setDefaultSSLSocketFactory(SSLCertificateSocketFactory
                        .getInsecure(-1, null));
        HttpsURLConnection
                .setDefaultHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    }
}
