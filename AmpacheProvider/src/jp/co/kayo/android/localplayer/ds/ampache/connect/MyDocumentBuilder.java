package jp.co.kayo.android.localplayer.ds.ampache.connect;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MyDocumentBuilder {
    private static final String TAG = "MyDocumentBuilder";

    private DocumentBuilderFactory factory = DocumentBuilderFactory
            .newInstance();
    private DocumentBuilder builder = null;

    private String errorCode = null;
    private String errorText = null;

    public MyDocumentBuilder() {
    }

    public Document getDocument(InputStream in) throws SAXException,
            IOException, ParserConfigurationException {
        DocumentBuilder docBuilder = getBuilder();
        Document doc = docBuilder.parse(in);
        // Logger.d(XMLUtils.toString(doc));
        NodeList list = doc.getElementsByTagName("error");
        if (list == null || list.getLength() == 0) {
            return doc;
        } else if (list.getLength() > 0) {
            Element element = (Element) list.item(0);
            errorCode = element.getAttribute("code");
            errorText = XMLUtils.getTextValue(element);
        }

        return null;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorText() {
        return errorText;
    }

    private synchronized DocumentBuilder getBuilder()
            throws ParserConfigurationException {
        if (builder == null) {
            builder = factory.newDocumentBuilder();
        }
        return builder;
    }
}
