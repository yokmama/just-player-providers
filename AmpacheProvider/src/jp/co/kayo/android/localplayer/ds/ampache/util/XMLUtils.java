package jp.co.kayo.android.localplayer.ds.ampache.util;

import java.io.IOException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.text.Html;
import android.text.TextUtils;

public class XMLUtils {
    public static String getAttributeValue(XmlPullParser parser, String name) {
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            if (parser.getAttributeName(i).equals(name)) {
                return parser.getAttributeValue(i);
            }
        }
        return null;
    }

    public static String getTextValue(XmlPullParser parser) {

        try {
            return parser.nextText();
        } catch (XmlPullParserException e) {
            Logger.e("XMLHandler.XmlPullParserException", e);
        } catch (IOException e) {
            Logger.e("XMLHandler.IOException", e);
        }
        return "";
        /*
         * String ret = ""; try { StringBuilder buf = new StringBuilder(); do{
         * int type = parser.nextToken();
         * 
         * if(type == XmlPullParser.TEXT || type == XmlPullParser.CDSECT){
         * buf.append(parser.getText()); } else if(type ==
         * XmlPullParser.ENTITY_REF){ String s = "&"+parser.getName()+";";
         * String a = Html.fromHtml(s).toString(); buf.append(a); } else{ break;
         * } }while(true); return buf.toString(); } catch
         * (XmlPullParserException e) {
         * Logger.e("XMLHandler.XmlPullParserException", e); } catch
         * (IOException e) { Logger.e("XMLHandler.IOException", e); } finally{
         * try { parser.next(); } catch (XmlPullParserException e) {
         * Logger.e("XMLHandler.XmlPullParserException", e); } catch
         * (IOException e) { Logger.e("XMLHandler.IOException", e); } } return
         * ret;
         */
    }

    public static String getTextValue(Node node) {
        NodeList nodes = node.getChildNodes();
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            if (n.getNodeType() == Node.TEXT_NODE) {
                buf.append(n.getNodeValue());
                // return n.getNodeValue();
            } else if (n.getNodeType() == Node.CDATA_SECTION_NODE) {
                buf.append(n.getNodeValue());
                // return n.getNodeValue();
            }
        }
        return buf.toString();
    }

    /***
     * XSLTのライブラリがないので落ちます Xlanか何かをライブラリに追加する必要あり
     * 
     * @param doc
     * @return
     */
    /*
     * public static String toString(Document doc) { if (doc != null) { try {
     * StringWriter sw = new StringWriter(); TransformerFactory tfactory =
     * TransformerFactory.newInstance(); Transformer transformer; transformer =
     * tfactory.newTransformer(); transformer.transform(new DOMSource(doc), new
     * StreamResult(sw)); String xml = sw.toString(); return xml; } catch
     * (Exception e) { Logger.e("toString", e); } } return null; }
     */

    public static String getStringFromNode(Node root) throws IOException {
        StringBuilder result = new StringBuilder();
        if (root.getNodeType() == 3)
            result.append(root.getNodeValue());
        else {
            if (root.getNodeType() != 9) {
                StringBuffer attrs = new StringBuffer();
                if (root.getAttributes() != null) {
                    for (int k = 0; k < root.getAttributes().getLength(); ++k) {
                        attrs.append(" ")
                                .append(root.getAttributes().item(k)
                                        .getNodeName())
                                .append("=\"")
                                .append(root.getAttributes().item(k)
                                        .getNodeValue()).append("\" ");
                    }
                }
                result.append("<").append(root.getNodeName()).append(" ")
                        .append(attrs).append(">");
            } else {
                result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            }

            NodeList nodes = root.getChildNodes();
            for (int i = 0, j = nodes.getLength(); i < j; i++) {
                Node node = nodes.item(i);
                result.append(getStringFromNode(node));
            }

            if (root.getNodeType() != 9) {
                result.append("</").append(root.getNodeName()).append(">");
            }
        }
        return result.toString();
    }
}
