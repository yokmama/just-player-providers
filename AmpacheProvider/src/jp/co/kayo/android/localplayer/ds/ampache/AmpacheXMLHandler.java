package jp.co.kayo.android.localplayer.ds.ampache;

import jp.co.kayo.android.localplayer.ds.ampache.bean.Album;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Artist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Playlist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Song;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Tag;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Video;
import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.xmlpull.v1.XmlPullParser;

public class AmpacheXMLHandler extends XMLHandler {
    boolean mContinue = false;
    Album mAlbum = null;
    Artist mArtist = null;
    Tag mTag = null;
    Playlist mPlaylist = null;
    Video mVideo = null;
    Song mSong;
    Creator creator = null;
    String host;

    AmpacheXMLHandler(String host, Creator creator) {
        this.host = host;
        this.creator = creator;
    }

    @Override
    public boolean startTag(XmlPullParser parser) {
        String tag = parser.getName();
        Logger.d("start tag :" + tag);
        if ("album".equals(tag)) {
            if (mSong != null) {
                mSong.setValue(host, tag, parser);
            } else {
                mContinue = true;
                mAlbum = new Album();
                String id = XMLUtils.getAttributeValue(parser, "id");
                mAlbum.setId(id);
            }
        } else if ("artist".equals(tag)) {
            if (mAlbum != null) {
                mAlbum.setValue(host, tag, parser);
            } else if (mSong != null) {
                mSong.setValue(host, tag, parser);
            } else {
                mContinue = true;
                mArtist = new Artist();
                String id = XMLUtils.getAttributeValue(parser, "id");
                mArtist.setId(id);
            }
        } else if ("tag".equals(tag)) {
            if (mAlbum != null) {
                mAlbum.setValue(host, tag, parser);
            } else if (mArtist != null) {
                mArtist.setValue(host, tag, parser);
            } else if (mPlaylist != null) {
                mPlaylist.setValue(host, tag, parser);
            } else if (mVideo != null) {
                mVideo.setValue(host, tag, parser);
            } else if (mSong != null) {
                mSong.setValue(host, tag, parser);
            } else {
                mContinue = true;
                mTag = new Tag();
                String id = XMLUtils.getAttributeValue(parser, "id");
                mTag.setId(id);
            }
        } else if ("playlist".equals(tag)) {
            mContinue = true;
            mPlaylist = new Playlist();
            String id = XMLUtils.getAttributeValue(parser, "id");
            mPlaylist.setId(id);
        } else if ("video".equals(tag)) {
            mContinue = true;
            mVideo = new Video();
            String id = XMLUtils.getAttributeValue(parser, "id");
            mVideo.setId(id);
        } else if ("song".equals(tag)) {
            mContinue = true;
            mSong = new Song();
            String id = XMLUtils.getAttributeValue(parser, "id");
            mSong.setId(id);
        } else if (mAlbum != null) {
            mAlbum.setValue(host, tag, parser);
        } else if (mArtist != null) {
            mArtist.setValue(host, tag, parser);
        } else if (mTag != null) {
            mTag.setValue(host, tag, parser);
        } else if (mPlaylist != null) {
            mPlaylist.setValue(host, tag, parser);
        } else if (mVideo != null) {
            mVideo.setValue(host, tag, parser);
        } else if (mSong != null) {
            mSong.setValue(host, tag, parser);
        }
        return true;
    }

    @Override
    public boolean endTag(XmlPullParser parser) {
        String tag = parser.getName();
        Logger.d("end tag :" + tag);
        if ("album".equals(tag)) {
            if (mAlbum != null && mAlbum.getName() != null) {
                // DbInsert
                creator.createAlbum(mAlbum);
            }
            mAlbum = null;
        } else if ("artist".equals(tag)) {
            if (mArtist != null && mArtist.getName() != null) {
                // DbInsert
                creator.createArtist(mArtist);
            }
            mArtist = null;
        } else if ("tag".equals(tag)) {
            if (mTag != null && mTag.getName() != null) {
                // DbInsert
                creator.createTag(mTag);
            }
            mTag = null;
        } else if (tag.equals("playlist")) {
            if (mPlaylist != null && mPlaylist.getName() != null) {
                // DbInsert
                creator.createPlaylist(mPlaylist);
            }
            mPlaylist = null;
        } else if (tag.equals("video")) {
            if (mVideo != null && mVideo.getTitle() != null) {
                // DbInsert
                creator.createVideo(mVideo);
            }
            mVideo = null;
        } else if (tag.equals("song")) {
            if (mSong != null && mSong.getTitle() != null) {
                // DbInsert
                creator.createSong(mSong);
            }
        } else if (tag.equals("root")) {
            return false;
        }
        return true;
    }

}
