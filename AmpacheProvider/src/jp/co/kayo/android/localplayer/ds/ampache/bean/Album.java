package jp.co.kayo.android.localplayer.ds.ampache.bean;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

/**
 * @author yokmama
 * 
 */
public class Album {
    private String mId;
    private String mName;
    private Artist mArtist;
    private String mYear;
    private int mNumTrack;
    private String mDisk;
    private ArrayList<Tag> mTags = new ArrayList<Tag>();
    private String mArt;
    private String mPreciserating;
    private int mRating;

    public Album() {
    }

    public void setValue(String server, String tag, XmlPullParser parser) {
        if (tag.equals("name")) {
            setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("artist")) {
            // ex <artist id="129348">AC/DC</artist>
            setArtist(new Artist());
            String id = XMLUtils.getAttributeValue(parser, "id");
            getArtist().setId(id);
            getArtist().setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("year")) {
            setYear(XMLUtils.getTextValue(parser));
        } else if (tag.equals("tracks")) {
            setNumTrack(getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("disk")) {
            setDisk(XMLUtils.getTextValue(parser));
        } else if (tag.equals("tag")) {
            Tag tagvalue = new Tag();
            String id = XMLUtils.getAttributeValue(parser, "id");
            String count = XMLUtils.getAttributeValue(parser, "count");
            tagvalue.setId(id);
            tagvalue.setName(XMLUtils.getTextValue(parser));
            tagvalue.setSortOrder(ValueRetriever.getInt(count));
            getTags().add(tagvalue);
        } else if (tag.equals("art")) {
            String u = XMLUtils.getTextValue(parser);

            String s1 = server.replaceFirst("[^/]*//[^/]+", "");
            String s2 = u.replaceFirst("[^/]*//[^/]+", "");
            setArt(s1.equals("/") ? s2 : s2.replace(s1, ""));
            /*
             * String https = null; if (server != null &&
             * server.indexOf("https:") != -1) { https =
             * server.replace("https:", "http:"); } if (server == null) {
             * setArt(u); } else if (https != null) { if (u.indexOf("http:") !=
             * -1) { setArt(u.replaceFirst(https, "")); } else {
             * setArt(u.replaceFirst(server, "")); } } else {
             * setArt(u.replaceFirst(server, "")); }
             */
        } else if (tag.equals("preciserating")) {
            setPreciserating(XMLUtils.getTextValue(parser));
        } else if (tag.equals("rating")) {
            setRating(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        }
    }

    public void parse(String server, Node node) {
        setId(((Element) node).getAttribute("id"));

        /*
         * String https = null; if (server != null && server.indexOf("https:")
         * != -1) { https = server.replace("https:", "http:"); }
         */
        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("name")) {
                setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("artist")) {
                // ex <artist id="129348">AC/DC</artist>
                setArtist(new Artist());
                getArtist().setId(((Element) child).getAttribute("id"));
                getArtist().setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("year")) {
                setYear(XMLUtils.getTextValue(child));
            } else if (tag.equals("tracks")) {
                setNumTrack(getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("disk")) {
                setDisk(XMLUtils.getTextValue(child));
            } else if (tag.equals("tag")) {
                Tag tagvalue = new Tag();
                tagvalue.setId(((Element) child).getAttribute("id"));
                tagvalue.setName(XMLUtils.getTextValue(child));
                tagvalue.setSortOrder(ValueRetriever.getInt(((Element) child)
                        .getAttribute("count")));
                getTags().add(tagvalue);
            } else if (tag.equals("art")) {
                String u = XMLUtils.getTextValue(child);

                String s1 = server.replaceFirst("[^/]*//[^/]+", "");
                String s2 = u.replaceFirst("[^/]*//[^/]+", "");
                setArt(s1.equals("/") ? s2 : s2.replace(s1, ""));
                /*
                 * if (server == null) { setArt(u); } else if (https != null) {
                 * if (u.indexOf("http:") != -1) { setArt(u.replaceFirst(https,
                 * "")); } else { setArt(u.replaceFirst(server, "")); } } else {
                 * setArt(u.replaceFirst(server, "")); }
                 */

            } else if (tag.equals("preciserating")) {
                setPreciserating(XMLUtils.getTextValue(child));
            } else if (tag.equals("rating")) {
                setRating(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            }
        }
    }

    int getInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            // nothing
        }
        return 0;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setArtist(Artist artist) {
        this.mArtist = artist;
    }

    public Artist getArtist() {
        return mArtist;
    }

    public void setYear(String year) {
        this.mYear = year;
    }

    public String getYear() {
        return mYear;
    }

    public void setNumTrack(int tracks) {
        this.mNumTrack = tracks;
    }

    public int getNumTrack() {
        return mNumTrack;
    }

    public void setDisk(String disk) {
        this.mDisk = disk;
    }

    public String getDisk() {
        return mDisk;
    }

    public void setArt(String art) {
        this.mArt = art;
    }

    public String getArt() {
        return mArt;
    }

    public void setPreciserating(String preciserating) {
        this.mPreciserating = preciserating;
    }

    public String getPreciserating() {
        return mPreciserating;
    }

    public void setRating(int rating) {
        this.mRating = rating;
    }

    public int getRating() {
        return mRating;
    }

    public ArrayList<Tag> getTags() {
        return mTags;
    }

    public void setTags(ArrayList<Tag> tags) {
        mTags = tags;
    }

}
