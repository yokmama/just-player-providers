package jp.co.kayo.android.localplayer.ds.ampache.connect;

import jp.co.kayo.android.localplayer.ds.ampache.AmpacheHelper;
import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ServerConfig {
    public String host;
    public String user;
    public String pass;
    public int dbIndex;

    // public boolean useSSL;

    public void save(SharedPreferences pref) {
        Editor editor = pref.edit();

        editor.putString(AmpacheHelper.AMPACHE_HOSTNAME, host);
        editor.putString(AmpacheHelper.AMPACHE_USERNAME, user);
        editor.putString(AmpacheHelper.AMPACHE_PASSWD, pass);
        editor.putInt(AmpacheHelper.AMPACHE_INDEX, dbIndex);
        // editor.putBoolean(AmpacheHelper.AMPACHE_USESSL, useSSL);

        editor.commit();
    }

    public void load(SharedPreferences pref) {
        host = pref.getString(AmpacheHelper.AMPACHE_HOSTNAME,
                SystemConsts.DEFAULT_HOST);
        user = pref.getString(AmpacheHelper.AMPACHE_USERNAME,
                SystemConsts.DEFAULT_USID);
        pass = pref.getString(AmpacheHelper.AMPACHE_PASSWD,
                SystemConsts.DEFAULT_PSWD);
        dbIndex = pref.getInt(AmpacheHelper.AMPACHE_INDEX,
                SystemConsts.DEFAULT_INDEX);
        // useSSL = pref.getBoolean(AmpacheHelper.AMPACHE_USESSL, false);
    }

}
