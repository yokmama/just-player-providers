package jp.co.kayo.android.localplayer.ds.ampache;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jp.co.kayo.android.localplayer.ds.ampache.bean.Album;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Artist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Playlist;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Song;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Tag;
import jp.co.kayo.android.localplayer.ds.ampache.bean.Video;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioGenresMember;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.VideoMedia;
import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.SSLHelper;
import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.apache.commons.httpclient.contrib.ssl.EasySSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.util.Xml;
import android.widget.Toast;

public class AmpacheHelper {
    public static final String AMPACHE_HOSTNAME = "KEY.AMPACHE_HOSTNAME";
    public static final String AMPACHE_USERNAME = "KEY.AMPACHE_USERNAME";
    public static final String AMPACHE_PASSWD = "KEY.AMPACHE_PASSWD";
    public static final String AMPACHE_INDEX = "KEY.AMPACHE_INDEX";
    public static final String AMPACHE_USESSL = "KEY.AMPACHE_SSL";

    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int SOCKET_TIMEOUT = 20000;
    private static final int MCC_TIMEOUT = 5000;
    private final int SOCK_BUFSIZE = 4096;

    private SharedPreferences mPreferences;
    private DocumentBuilderFactory mDocFactory = DocumentBuilderFactory
            .newInstance();
    private Context mContext;
    private volatile SchemeRegistry mSchemeRegistry;
    // private volatile ThreadSafeClientConnManager mClientConnectionManager =
    // null;
    private volatile DocumentBuilder mDocBuilder = null;
    private BasicHttpParams mParams = null;
    private BasicHttpContext mHttpContext;

    private volatile Object mUrllock = new Object();
    private volatile Object mAuthlock = new Object();

    private String mErrorText;
    private String mErrorCode;
    private boolean mForceEnd = false;
    private int retryCount = 0;
    private long lastConnected;

    public void forceEnd() {
        mForceEnd = true;
    }

    public String getErrorText() {
        return mErrorText;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    private void errorReset() {
        mErrorText = null;
        mErrorCode = null;
    }

    public static void appendSubPath(StringBuilder buf, String host, String path) {
        boolean b1 = host.charAt(host.length() - 1) == '/';
        boolean b2 = path.charAt(0) == '/';
        buf.append(host);
        if (!b1 && !b2) {
            buf.append("/");
        }
        buf.append(path);
    }

    public static String getDbName(int n) {
        return String.format("ampache_%02d.db", n);
    }

    public AmpacheHelper(Context context) {
        this.mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context
                .getApplicationContext());

        if (Build.VERSION.SDK_INT > 7) {
            new SSLHelper().disableSSLVerifier();
        }
        disableConnectionReuseIfNecessary();
        enableHttpResponseCache(context);
    }

    private void enableHttpResponseCache(Context context) {
        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            File httpCacheDir = new File(context.getCacheDir(), "http");
            Class.forName("android.net.http.HttpResponseCache")
                    .getMethod("install", File.class, long.class)
                    .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
        }
    }

    private void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT <= 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    private String getURL() {
        synchronized (mUrllock) {
            if (ServerState.getInstance().getServerurl() == null) {
                ServerState.getInstance().setServerurl(
                        mPreferences.getString(AMPACHE_HOSTNAME,
                                SystemConsts.DEFAULT_HOST));
                // ServerState.getInstance().setUseSSL(mPreferences.getBoolean(AMPACHE_USESSL,
                // false));
            }
            return ServerState.getInstance().getServerurl();
        }
    }

    private String getMediaURL() {
        String url = getURL();
        /*
         * if (!ServerState.getInstance().isUseSSL() && url.indexOf("https:") !=
         * -1) { return
         * ServerState.getInstance().getServerurl().replace("https:", "http:");
         * } else { return url; }
         */
        return url;
    }

    public String getAuth(boolean b) {
        String url = getURL();
        synchronized (mAuthlock) {
            if (b || ServerState.getInstance().getAuthkey() == null
                    || ServerState.getInstance().getAuthkey().length() == 0) {
                String old = ServerState.getInstance().getAuthkey();
                ServerState.getInstance().setAuthkey(
                        mPreferences.getString("key.auth", ""));
                if (b) {
                    ServerState.getInstance().setAuthkey("");
                }
                if (ServerState.getInstance().getAuthkey().length() == 0) {
                    String usid = mPreferences.getString(AMPACHE_USERNAME,
                            SystemConsts.DEFAULT_USID);
                    String pswd = mPreferences.getString(AMPACHE_PASSWD,
                            SystemConsts.DEFAULT_PSWD);
                    String key = auth(mContext, url, usid, pswd);
                    if (key != null) {
                        ServerState.getInstance().setAuthkey(key);
                        Editor editor = mPreferences.edit();
                        editor.putString("key.auth", ServerState.getInstance()
                                .getAuthkey());
                        editor.commit();
                        Logger.d("getAuth old=" + old + " new="
                                + ServerState.getInstance().getAuthkey());
                    }
                    Logger.d("got authkey = "
                            + ServerState.getInstance().getAuthkey());
                }
            }
            return ServerState.getInstance().getAuthkey();
        }
    }

    public void clearAuth() {
        ServerState.getInstance().setSuccess(true);
        synchronized (mUrllock) {
            ServerState.getInstance().setServerurl(null);
        }
        synchronized (mAuthlock) {
            ServerState.getInstance().setAuthkey(null);
            Editor editor = mPreferences.edit();
            editor.putString("key.auth", "");
            editor.commit();
        }
    }

    public String getAuthrizedURL(String url) {
        if (url != null) {
            String auth = getAuth(false);
            String ret = url.replaceAll("auth=[^&]+", "auth=" + auth);
            if (ret.equals(url)) {
                ret = url.replaceAll("ssid=[^&]+", "ssid=" + auth);
            }
            if (ret.indexOf("http") == -1) {
                ret = getURL() + ret;
            }

            return ret;
        }
        return null;
    }

    public String getMediaAuthrizedURL(String url) {
        if (url != null) {
            String auth = getAuth(false);
            String ret = url.replaceAll("auth=[^&]+", "auth=" + auth);
            if (ret.equals(url)) {
                ret = url.replaceAll("ssid=[^&]+", "ssid=" + auth);
            }
            if (ret.indexOf("http") == -1) {
                ret = getMediaURL() + ret;
            }

            return ret;
        }
        return null;
    }

    public boolean hasSession() {
        return ServerState.getInstance().isSuccess();
    }

    synchronized SchemeRegistry getRegistry() {
        if (mSchemeRegistry == null) {
            mSchemeRegistry = new SchemeRegistry();
            // mSchemeRegistry.register(new Scheme("http",
            // PlainSocketFactory.getSocketFactory(), 80));
            // mSchemeRegistry.register(new Scheme("https",
            // SSLSocketFactory.getSocketFactory(), 443));
            // http scheme
            mSchemeRegistry.register(new Scheme("http", PlainSocketFactory
                    .getSocketFactory(), 80));
            // https scheme
            mSchemeRegistry.register(new Scheme("https",
                    new EasySSLSocketFactory(), 443));
        }
        return mSchemeRegistry;
    }

    synchronized BasicHttpParams getHttpParams() {
        if (mParams == null) {
            mParams = new BasicHttpParams();
            mParams.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
            // mParams.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE,
            // new ConnPerRouteBean(30));
            mParams.setParameter(HttpProtocolParams.USE_EXPECT_CONTINUE, false);
            HttpConnectionParams.setSocketBufferSize(mParams, SOCK_BUFSIZE); // ソケットバッファサイズ
                                                                             // 4KB
            HttpConnectionParams.setSoTimeout(mParams, SOCKET_TIMEOUT); // ソケット通信タイムアウト20秒
            HttpConnectionParams.setConnectionTimeout(mParams, SOCKET_TIMEOUT); // HTTP通信タイムアウト20秒
            HttpProtocolParams.setContentCharset(mParams, "UTF-8"); // 文字コードをUTF-8と明示
            // HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setVersion(mParams, HttpVersion.HTTP_1_0);

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            BasicScheme basicAuth = new BasicScheme();
            mHttpContext = new BasicHttpContext();
            mHttpContext.setAttribute("http.auth.credentials-provider",
                    credentialsProvider);
            mHttpContext.setAttribute("preemptive-auth", basicAuth);
        }
        return mParams;
    }

    synchronized DocumentBuilder getBuilder()
            throws ParserConfigurationException {
        if (mDocBuilder == null) {
            mDocBuilder = mDocFactory.newDocumentBuilder();
        }
        return mDocBuilder;
    }

    public static byte[] getByteArrayFromStream(InputStream in) {
        byte[] line = new byte[512];
        byte[] result = null;
        ByteArrayOutputStream out = null;
        int size = 0;
        try {
            out = new ByteArrayOutputStream();
            BufferedInputStream bis = new BufferedInputStream(in);
            while (true) {
                size = bis.read(line);
                if (size < 0) {
                    break;
                }
                out.write(line, 0, size);
            }
            result = out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
            }
        }
        return result;
    }

    public byte[] getBytes(URL url) {
        errorReset();
        InputStream in = null;
        HttpURLConnection connection = null;
        ThreadSafeClientConnManager cm = null;
        try {
            if (Build.VERSION.SDK_INT <= 8) {// Build.VERSION.SDK_INT==8 ||
                                             // Build.VERSION.SDK_INT==7){
                cm = new ThreadSafeClientConnManager(getHttpParams(),
                        getRegistry());
                DefaultHttpClient client = new DefaultHttpClient(cm,
                        getHttpParams());

                HttpGet get = new HttpGet(url.toString());
                get.setHeader("Host", url.getHost());
                get.setHeader("User-Agent", "Android-JustPlayer");
                HttpResponse httpResponse = client.execute(get, mHttpContext);
                if (httpResponse != null
                        && httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    in = httpEntity.getContent();
                }
            } else {
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(CONNECTION_TIMEOUT);
                int responseCode = connection.getResponseCode();

                // クライアントにHTTPレスポンスを書きこむ
                if (responseCode == 200 || responseCode == 206
                        || responseCode == 203) {
                    in = connection.getInputStream();
                }
            }

            if (in != null) {
                byte[] bytes = getByteArrayFromStream(in);
                return bytes;
            }
        } catch (java.net.SocketTimeoutException e) {
            Logger.d("Socket Timeout");
            mErrorText = e.getMessage();
            mErrorCode = "451";
        } catch (IOException e) {
            mErrorText = e.getMessage();
            Logger.d("error url =" + url.toString());
            Logger.e("(IO Exception) parse xml:", e);
        } finally {
            lastConnected = System.currentTimeMillis();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    Logger.e("connection.disconnect", e);
                }
                connection = null;
            }
            if (cm != null) {
                cm.shutdown();
            }
        }
        return null;
    }

    public XMLHandler parseXml(byte[] bytes, XMLHandler handler) {
        errorReset();
        try {
            XmlPullParser parser = Xml.newPullParser();
            String data = new String(bytes);
            Logger.d("xml=" + data + " size=" + bytes.length);
            parser.setInput(new StringReader(data));
            int eventType = parser.getEventType();
            mForceEnd = false;
            while (eventType != XmlPullParser.END_DOCUMENT && !mForceEnd) {
                Logger.d("eventType=" + eventType + " mForceEnd=" + mForceEnd);
                switch (eventType) {
                case XmlPullParser.END_TAG: {
                    String tag = parser.getName();
                    Logger.d("end tag " + tag);
                    if (handler.endTag(parser) != true) {
                        Logger.d("root end");
                        return handler;
                    }
                }
                    break;
                case XmlPullParser.START_TAG: {
                    String tag = parser.getName();
                    Logger.d("start tag " + tag);
                    if ("error".equals(tag)) {
                        mErrorText = XMLUtils.getTextValue(parser);
                        mErrorCode = XMLUtils.getAttributeValue(parser, "code");
                        if(mErrorCode!=null && "401".endsWith(mErrorCode)){
                            clearAuth();
                        }else if(mErrorText!=null && mErrorText.contains("Session Expired")){
                            clearAuth();
                        }
                        return handler;
                    } else if (handler.startTag(parser) != true) {
                        return handler;
                    }
                }
                    break;
                }
                Logger.d("next1");
                eventType = parser.next();
                Logger.d("next2");
            }
            Logger.d("break here ");
        } catch (XmlPullParserException e) {
            mErrorText = e.getMessage();
            Logger.e("(XmlPullParserException) parse xml:", e);
        } catch (IOException e) {
            mErrorText = e.getMessage();
            Logger.e("(IOException) parse xml:", e);
        } finally {
        }
        return handler;
    }

    public XMLHandler parseXml(URL url, XMLHandler handler) {
        errorReset();
        InputStream in = null;
        HttpURLConnection connection = null;
        ThreadSafeClientConnManager cm = null;
        try {
            XmlPullParser parser = Xml.newPullParser();
            if (Build.VERSION.SDK_INT <= 8) {// Build.VERSION.SDK_INT==8 ||
                                             // Build.VERSION.SDK_INT==7){
                cm = new ThreadSafeClientConnManager(getHttpParams(),
                        getRegistry());
                DefaultHttpClient client = new DefaultHttpClient(cm,
                        getHttpParams());

                HttpGet get = new HttpGet(url.toString());
                get.setHeader("Host", url.getHost());
                get.setHeader("User-Agent", "Android-JustPlayer");
                HttpResponse httpResponse = client.execute(get, mHttpContext);
                if (httpResponse != null
                        && httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    in = httpEntity.getContent();
                }
            } else {
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setReadTimeout(CONNECTION_TIMEOUT);
                int responseCode = connection.getResponseCode();

                // クライアントにHTTPレスポンスを書きこむ
                if (responseCode == 200 || responseCode == 206
                        || responseCode == 203) {
                    in = connection.getInputStream();
                }
            }

            if (in != null) {
                parser.setInput(in, "UTF-8");
                int eventType = parser.getEventType();
                mForceEnd = false;
                boolean isXMLContents = false;
                while (eventType != XmlPullParser.END_DOCUMENT && !mForceEnd) {
                    switch (eventType) {
                    case XmlPullParser.END_TAG: {
                        if (handler.endTag(parser) != true) {
                            return handler;
                        }
                    }
                        break;
                    case XmlPullParser.START_TAG: {
                        isXMLContents = true;
                        String tag = parser.getName();
                        if ("error".equals(tag)) {
                            mErrorText = XMLUtils.getTextValue(parser);
                            mErrorCode = XMLUtils.getAttributeValue(parser,
                                    "code");
                            if(mErrorCode!=null && "401".endsWith(mErrorCode)){
                                clearAuth();
                            }else if(mErrorText!=null && mErrorText.contains("Session Expired")){
                                clearAuth();
                            }
                            return handler;
                        } else if (handler.startTag(parser) != true) {
                            return handler;
                        }
                    }
                        break;
                    }
                    eventType = parser.next();
                }
                if (isXMLContents != true) {
                    mErrorText = "UserID/Password might be wrong.";
                    mErrorCode = "404";
                }
            } else {
                mErrorText = "UserID/Password might be wrong.";
                mErrorCode = "404";
            }
        } catch (java.net.SocketTimeoutException e) {
            Logger.d("Socket Timeout");
            mErrorText = e.getMessage();
            mErrorCode = "451";
        } catch (java.net.SocketException e) {
            Logger.d("No Route Host");
            mErrorText = e.getMessage();
            mErrorCode = "451";
        } catch (Exception e) {
            mErrorText = e.getMessage();
            Logger.d("error url =" + url.toString());
            Logger.e("(Exception) parse xml:", e);
        } finally {
            lastConnected = System.currentTimeMillis();
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    Logger.e("connection.disconnect", e);
                }
                connection = null;
            }
            if (cm != null) {
                cm.shutdown();
            }
        }
        return handler;
    }

    public AmpacheServer getServerInfo(Context context, String server,
            String user, String pswd) {
        try {
            long time = Calendar.getInstance().getTimeInMillis() / 1000;
            byte[] passphrase = getSHAPass(pswd, time);

            StringBuilder buf = new StringBuilder();
            AmpacheHelper.appendSubPath(buf, server,
                    "server/xml.server.php?action=handshake&auth=");
            buf.append(toHexString(passphrase));
            buf.append("&timestamp=").append(time);
            buf.append("&version=350001&user=").append(user);

            final AmpacheServer inf = new AmpacheServer();
            inf.setUrl(server);
            inf.setUsid(user);
            inf.setPswd(pswd);

            this.parseXml(new URL(buf.toString()), new XMLHandler() {

                @Override
                public boolean startTag(XmlPullParser parser) {
                    String tag = parser.getName();
                    if ("update".equals(tag)) {
                        inf.setDateUpdate(ValueRetriever.parseDate(XMLUtils
                                .getTextValue(parser)));
                    } else if ("update".equals(tag)) {
                        inf.setDateUpdate(ValueRetriever.parseDate(XMLUtils
                                .getTextValue(parser)));
                    } else if ("add".equals(tag)) {
                        inf.setDateAdd(ValueRetriever.parseDate(XMLUtils
                                .getTextValue(parser)));
                    } else if ("clean".equals(tag)) {
                        inf.setDateClean(ValueRetriever.parseDate(XMLUtils
                                .getTextValue(parser)));
                    } else if ("songs".equals(tag)) {
                        inf.setNumSong(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    } else if ("albums".equals(tag)) {
                        inf.setNumAlbum(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    } else if ("artists".equals(tag)) {
                        inf.setNumArtist(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    } else if ("playlists".equals(tag)) {
                        inf.setNumPlaylist(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    } else if ("videos".equals(tag)) {
                        inf.setNumVideo(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    } else if ("tags".equals(tag)) {
                        inf.setNumTag(ValueRetriever.getInt(XMLUtils
                                .getTextValue(parser)));
                    }

                    return true;
                }

                @Override
                public boolean endTag(XmlPullParser parser) {
                    return true;
                }
            });
            inf.setErrorText(mErrorText);
            inf.setErrorCode(mErrorCode);

            return inf;
        } catch (MalformedURLException e) {
            Logger.e("new URL", e);
        } catch (Exception e) {
            Logger.e("getDocument", e);
        } finally {
        }
        return null;
    }

    public long getLastUpdate(String serverinfUrl, boolean useIndexCacheFlg) {
        if (serverinfUrl != null) {
            String key = useIndexCacheFlg ? "KEY_UPDT"
                    + serverinfUrl.hashCode() + "_FULL" : "KEY_UPDT"
                    + serverinfUrl.hashCode();
            long lastUpdate = mPreferences.getLong(key, -1);

            return lastUpdate;
        }
        return -1;
    }

    public void setLastUpdate(String serverinfUrl, boolean useIndexCacheFlg,
            long lastUpdate) {
        if (serverinfUrl != null) {
            String key = useIndexCacheFlg ? "KEY_UPDT"
                    + serverinfUrl.hashCode() + "_FULL" : "KEY_UPDT"
                    + serverinfUrl.hashCode();
            Editor editor = mPreferences.edit();
            editor.putLong(key, lastUpdate);
            editor.commit();
        }
    }

    public long insertAlbum(SQLiteDatabase db, Album album) {
        Calendar cal = Calendar.getInstance();
        try {
            ContentValues values = new ContentValues();
            values.put(AudioAlbum._ID, album.getId());
            values.put(AudioAlbum.ALBUM_KEY, album.getId());
            values.put(AudioAlbum.ALBUM, album.getName());
            values.put(AudioAlbum.ALBUM_ART, album.getArt());
            if (album.getArtist() != null) {
                values.put(AudioAlbum.ARTIST, album.getArtist().getName());
            }
            values.put(AudioAlbum.FIRST_YEAR, album.getYear());
            values.put(AudioAlbum.LAST_YEAR, album.getYear());
            values.put(AudioAlbum.NUMBER_OF_SONGS, album.getNumTrack());
            values.put(AudioAlbum.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioAlbum.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.FAVORITE_POINT, album.getRating());
            values.put(TableConsts.ALBUM_DEL_FLG, 0);

            long id = db.replace(TableConsts.TBNAME_ALBUM, null, values);
            return id;
        } finally {
        }
    }

    public long insertPlaylist(SQLiteDatabase db, Playlist playlist) {
        Calendar cal = Calendar.getInstance();
        try {
            ContentValues values = new ContentValues();
            values.put(AudioPlaylist._ID, playlist.getId());
            values.put(AudioPlaylist.NAME, playlist.getName());
            values.put(AudioPlaylist.PLAYLIST_KEY, playlist.getId());
            values.put(AudioPlaylist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioPlaylist.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.PLAYLIST_DEL_FLG, 0);

            long id = db.replace(TableConsts.TBNAME_PLAYLIST, null, values);
            return id;
        } finally {
        }
    }

    public long insertGenres(SQLiteDatabase db, Tag tag) {
        Calendar cal = Calendar.getInstance();
        try {
            ContentValues values = new ContentValues();
            values.put(AudioGenres._ID, tag.getId());
            values.put(AudioGenres.NAME, tag.getName());
            values.put(AudioGenres.GENRES_KEY, tag.getId());
            values.put(AudioGenres.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioGenres.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(AudioGenres.NUMBER_OF_ALBUMS, tag.getNumAlbum());
            values.put(AudioGenres.NUMBER_OF_ARTISTS, tag.getNumArtist());
            values.put(AudioGenres.NUMBER_OF_TRACKS, tag.getNumSong());
            values.put(AudioGenres.NUMBER_OF_PLAYLISTS, tag.getNumPlaylist());
            values.put(AudioGenres.NUMBER_OF_VIDEOS, tag.getNumVideo());
            values.put(TableConsts.GENRES_DEL_FLG, 0);

            long id = db.replace(TableConsts.TBNAME_GENRES, null, values);
            return id;
        } finally {
        }
    }

    public Uri insertGenresMember(Tag tag, Song song) {
        Calendar cal = Calendar.getInstance();
        ContentValues genres_member_values = new ContentValues();
        genres_member_values.put(AudioGenresMember.GENRE_ID, tag.getId());
        genres_member_values.put(AudioGenresMember.AUDIO_ID, song.getId());
        genres_member_values.put(AudioGenresMember.TITLE, song.getTitle());
        genres_member_values.put(AudioGenresMember.ALBUM, song.getAlbum()
                .getName());
        genres_member_values.put(AudioGenresMember.ARTIST, song.getArtist()
                .getName());
        // genres_member_values.put(AudioGenresMember.ALBUM_ART, song.art);
        genres_member_values.put(AudioGenresMember.DEFAULT_SORT_ORDER,
                tag.getSortOrder());
        genres_member_values.put(AudioGenresMember.DATE_ADDED,
                cal.getTimeInMillis());
        genres_member_values.put(AudioGenresMember.DATE_MODIFIED,
                cal.getTimeInMillis());

        Uri uri = mContext.getContentResolver().insert(
                ContentUris.withAppendedId(MediaConsts.GENRES_CONTENT_URI, 0),
                genres_member_values);
        return uri;
    }

    public void insertVideo(SQLiteDatabase db, Video video) {
        Calendar cal = Calendar.getInstance();
        try {
            ContentValues values = new ContentValues();
            values.put(VideoMedia._ID, video.getId());
            values.put(VideoMedia.MEDIA_KEY, video.getId());
            values.put(VideoMedia.TITLE, video.getTitle());
            values.put(VideoMedia.MIME_TYPE, video.getMime());
            values.put(VideoMedia.RESOLUTION, video.getResolution());
            values.put(VideoMedia.SIZE, video.getSize());
            values.put(VideoMedia.DATA, video.getUrl());
            values.put(VideoMedia.DATE_ADDED, cal.getTimeInMillis());
            values.put(VideoMedia.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.VIDEO_DEL_FLG, 0);

            db.replace(TableConsts.TBNAME_VIDEO, null, values);
        } finally {
        }
    }

    public Uri insertPlaylistMember(Song song, String playlistkey, int order) {
        Calendar cal = Calendar.getInstance();
        Cursor cur = null;
        try {
            Logger.d("registPlaylistMember: " + song.getTitle() + ","
                    + song.getArtist().getName());

            ContentValues values = new ContentValues();
            values.put(AudioPlaylistMember.PLAYLIST_ID, playlistkey);
            values.put(AudioPlaylistMember.AUDIO_ID, song.getId());
            values.put(AudioPlaylistMember.PLAY_ORDER, order);
            values.put(AudioPlaylistMember.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioPlaylistMember.DATE_MODIFIED, cal.getTimeInMillis());

            return mContext.getContentResolver().insert(
                    MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, values);
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public long insertArtist(SQLiteDatabase db, Artist artist) {
        Calendar cal = Calendar.getInstance();
        try {
            ContentValues values = new ContentValues();
            values.put(AudioArtist._ID, artist.getId());
            values.put(AudioArtist.ARTIST_KEY, artist.getId());
            values.put(AudioArtist.ARTIST, artist.getName());
            values.put(AudioArtist.NUMBER_OF_ALBUMS, artist.getNumAlbums());
            values.put(AudioArtist.NUMBER_OF_TRACKS, artist.getNumSong());
            values.put(AudioArtist.DATE_ADDED, cal.getTimeInMillis());
            values.put(AudioArtist.DATE_MODIFIED, cal.getTimeInMillis());
            values.put(TableConsts.FAVORITE_POINT, artist.getRating());
            values.put(TableConsts.ARTIST_DEL_FLG, 0);

            long id = db.replace(TableConsts.TBNAME_ARTIST, null, values);
            return id;
        } finally {
        }
    }

    public boolean isRegisterdGenres(SQLiteDatabase db, String key) {

        Cursor cur = null;
        try {
            cur = db.query(TableConsts.TBNAME_GENRES, new String[] {
                    BaseColumns._ID, AudioGenres.NAME }, AudioGenres.GENRES_KEY
                    + " = ?", new String[] { key }, null, null, null);
            if (cur != null && cur.getCount() > 0) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean isRegisterdArtist(SQLiteDatabase db, String key) {

        Cursor cur = null;
        try {
            cur = db.query(TableConsts.TBNAME_ARTIST, new String[] {
                    BaseColumns._ID, AudioArtist.ARTIST },
                    AudioArtist.ARTIST_KEY + " = ?", new String[] { key },
                    null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public boolean isRegisterdAlbum(SQLiteDatabase db, String key) {

        Cursor cur = null;
        try {
            cur = db.query(TableConsts.TBNAME_ALBUM, new String[] {
                    BaseColumns._ID, AudioAlbum.ALBUM }, AudioAlbum.ALBUM_KEY
                    + " = ?", new String[] { key }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public String toHexString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public byte[] getSHAPass(String pw, long time)
            throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] key = md.digest(pw.getBytes());

        String s = Long.toString(time) + toHexString(key);

        byte[] passphrase = md.digest(s.getBytes());

        return passphrase;
    }

    public String auth(Context context, String host, String user, String pass) {
        try {
            long time = Calendar.getInstance().getTimeInMillis() / 1000;
            byte[] passphrase = getSHAPass(pass, time);

            StringBuilder buf = new StringBuilder();
            AmpacheHelper.appendSubPath(buf, host,
                    "server/xml.server.php?action=handshake&auth=");
            buf.append(toHexString(passphrase));
            buf.append("&timestamp=").append(time);
            buf.append("&version=350001&user=").append(user);
            AuthHandler handler = new AuthHandler();
            parseXml(new URL(buf.toString()), handler);
            return handler.getAuthToken();
        } catch (NoSuchAlgorithmException e) {
            Logger.e("auth", e);
        } catch (MalformedURLException e) {
            Logger.e("auth", e);
        } catch (Exception e) {
            Handler handler = new Handler(context.getMainLooper());
            final Context con = context;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(con,
                            con.getString(R.string.txt_could_not_open_url),
                            Toast.LENGTH_SHORT).show();
                }
            });
            Logger.e("auth", e);
        } finally {

        }
        ServerState.getInstance().setSuccess(false);

        return null;
    }

    // SimpleDateFormat format = new
    // SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
    public String ping(boolean force) {
        Logger.d("ping " + getURL() + " at " + force);
        try {
            long dt = System.currentTimeMillis() - lastConnected;
            if (force) {
                return getAuth(true);
            } else {
                StringBuilder buf = new StringBuilder();
                AmpacheHelper.appendSubPath(buf, getURL(),
                        "server/xml.server.php?action=ping&auth=");
                buf.append(getAuth(false));
                parseXml(new URL(buf.toString()), new XMLHandler() {
                    @Override
                    public boolean startTag(XmlPullParser parser) {
                        return false;
                    }

                    @Override
                    public boolean endTag(XmlPullParser parser) {
                        return false;
                    }
                });
            }
        } catch (MalformedURLException e) {
            Logger.e("auth", e);
        } catch (Exception e) {
            Logger.e("auth", e);
        }
        return null;
    }

    public boolean writeAlbumDelFlg(int flg, String id) {
        return writeDelFlg(flg, MediaConsts.ALBUM_CONTENT_URI,
                TableConsts.ALBUM_DEL_FLG, MediaConsts.AudioAlbum.ALBUM_KEY
                        + " = ?", new String[] { id });
    }

    public boolean writeArtistDelFlg(int flg, String id) {
        return writeDelFlg(flg, MediaConsts.ARTIST_CONTENT_URI,
                TableConsts.ARTIST_DEL_FLG, MediaConsts.AudioArtist.ARTIST_KEY
                        + " = ?", new String[] { id });
    }

    public boolean writeGenreDelFlg(int flg, String id) {
        return writeDelFlg(flg, MediaConsts.GENRES_CONTENT_URI,
                TableConsts.GENRES_DEL_FLG, MediaConsts.AudioGenres.GENRES_KEY
                        + " = ?", new String[] { id });
    }

    public boolean writePlaylistDelFlg(int flg, String id) {
        return writeDelFlg(flg, MediaConsts.PLAYLIST_CONTENT_URI,
                TableConsts.PLAYLIST_DEL_FLG,
                MediaConsts.AudioPlaylist.PLAYLIST_KEY + " = ?",
                new String[] { id });
    }

    public boolean writeVideoDelFlg(int flg, String id) {
        return writeDelFlg(flg, MediaConsts.VIDEO_CONTENT_URI,
                TableConsts.VIDEO_DEL_FLG, MediaConsts.VideoMedia.MEDIA_KEY
                        + " = ?", new String[] { id });
    }

    private boolean writeDelFlg(int flg, Uri contentUri, String column,
            String where, String[] whereArgs) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(column, flg);
            mContext.getContentResolver().update(contentUri, cv, where,
                    whereArgs);
        } catch (SQLiteException e) {
            return false;
        }
        return true;
    }

    public ArrayList<String> getAllAlbumKey() {
        ArrayList<String> keyList = baseGetAllKey(
                MediaConsts.ALBUM_CONTENT_URI, AudioAlbum.ALBUM_KEY);
        return keyList;
    }

    public ArrayList<String> getAllTagKey() {
        ArrayList<String> keyList = baseGetAllKey(
                MediaConsts.GENRES_CONTENT_URI, AudioGenres.GENRES_KEY);
        return keyList;
    }

    public ArrayList<String> getAllArtistKey() {
        ArrayList<String> keyList = baseGetAllKey(
                MediaConsts.ARTIST_CONTENT_URI, AudioArtist.ARTIST_KEY);
        return keyList;
    }

    public ArrayList<String> getAllPlaylistKey() {
        ArrayList<String> keyList = baseGetAllKey(
                MediaConsts.PLAYLIST_CONTENT_URI, AudioPlaylist.PLAYLIST_KEY);
        return keyList;
    }

    public ArrayList<String> getAllVideoKey() {
        ArrayList<String> keyList = baseGetAllKey(
                MediaConsts.VIDEO_CONTENT_URI, VideoMedia.MEDIA_KEY);
        return keyList;
    }

    private ArrayList<String> baseGetAllKey(Uri uri, String column) {
        ArrayList<String> keyList = new ArrayList<String>();
        Cursor cur = null;
        try {
            cur = mContext.getContentResolver().query(uri,
                    new String[] { column }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                do {
                    String key = cur.getString(cur.getColumnIndex(column));
                    keyList.add(key);
                } while (cur.moveToNext());
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return keyList;
    }
}
