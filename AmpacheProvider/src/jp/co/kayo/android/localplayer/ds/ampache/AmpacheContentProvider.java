package jp.co.kayo.android.localplayer.ds.ampache;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.TableConsts;
import jp.co.kayo.android.localplayer.ds.ampache.util.ArrayUtils;
import jp.co.kayo.android.localplayer.ds.ampache.util.Logger;
import jp.co.kayo.android.localplayer.ds.ampache.util.StrictHelper;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.MediaStore;

/**
 * @author yokmama
 */
public class AmpacheContentProvider extends ContentProvider implements
        MediaConsts {
    private UriMatcher mUriMatcher;
    private SharedPreferences mPreference;
    private RequestCache mCache;
    private AmpacheDatabaseHelper mDatabaseHelper;
    private String mDatabasename;
    private int mDatabaseIndex = -1;
    private AmpacheHelper mAmpacheHelper = null;
    private ConnectivityManager mConnectionMgr;

    public final String FAVORITE_ID = "media_id";
    public final String FAVORITE_TYPE = "type";
    public final String FAVORITE_POINT = "point";
    public final String FAVORITE_TYPE_SONG = "song";
    public final String FAVORITE_TYPE_ALBUM = "album";
    public final String FAVORITE_TYPE_ARTIST = "artist";

    private boolean isWifiConnected() {
        if (mConnectionMgr == null) {
            mConnectionMgr = (ConnectivityManager) getContext()
                    .getApplicationContext().getSystemService(
                            Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo info = mConnectionMgr.getActiveNetworkInfo();
        if (info != null) {
            return ((info.getType() == ConnectivityManager.TYPE_WIFI) && (info
                    .isConnected()));
        }

        return false;
    }

    private boolean canUseNetwork() {
        if (mPreference.getBoolean(MainActivity.CNF_WIFI_ONLY, true)) {
            return isWifiConnected();
        } else {
            return true;
        }
    }

    private synchronized AmpacheHelper getAmpacheHelper() {
        if (mAmpacheHelper == null) {
            mAmpacheHelper = new AmpacheHelper(getContext());
        }

        return mAmpacheHelper;
    }

    private String getDBN() {
        if (mDatabaseIndex == -1) {
            mDatabaseIndex = mPreference.getInt(AmpacheHelper.AMPACHE_INDEX,
                    SystemConsts.DEFAULT_INDEX);
            if (mDatabaseIndex == -1) {
                mDatabaseIndex = 0;
            }
        }
        return AmpacheHelper.getDbName(mDatabaseIndex);
    }

    private synchronized AmpacheDatabaseHelper getDBHelper() {
        String s = getDBN();
        if (mDatabaseHelper == null || !mDatabasename.equals(s)) {
            if (mDatabaseHelper != null) {
                mDatabaseHelper.close();
                mDatabaseHelper = null;
            }
            mDatabasename = getDBN();
            mDatabaseHelper = new AmpacheDatabaseHelper(getContext(),
                    mDatabasename);
            // Once the write has opened for the upgrade
            SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
            if (db != null) {
                db.close();
                db = null;
            }
        }
        return mDatabaseHelper;
    }

    @Override
    public boolean onCreate() {
        StrictHelper.registStrictMode();
        mPreference = PreferenceManager
                .getDefaultSharedPreferences(getContext()
                        .getApplicationContext());
        mCache = new RequestCache(getContext(), mPreference, getAmpacheHelper());
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/media", CODE_MEDIA);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/media/#", CODE_MEDIA_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/albums", CODE_ALBUMS);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/albums/#", CODE_ALBUMS_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/artist", CODE_ARTIST);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/artist/#", CODE_ARTIST_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/albumart", AUDIO_ALBUMART);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/albumart/#",
                AUDIO_ALBUMART_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/media/#/albumart",
                AUDIO_ALBUMART_FILE_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/playlist", CODE_PLAYLIST);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/playlist/#",
                CODE_PLAYLIST_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/playlistmember",
                CODE_PLAYLISTMEMBER);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/playlistmember/#",
                CODE_PLAYLISTMEMBER_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/favorite", CODE_FAVORITE);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/favorite/#",
                CODE_FAVORITE_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "video/media", CODE_VIDEO);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "video/media/#", CODE_VIDEO_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "order/audio", CODE_ORDER_AUDIO);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "order/audio/#",
                CODE_ORDER_AUDIO_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "download/media", CODE_DOWNLOAD);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/genres", CODE_GENRES);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/genres/#", CODE_GENRES_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/genresmember",
                CODE_GENRESMEMBER);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "audio/genresmember/#",
                CODE_GENRESMEMBER_ID);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/auth", CODE_AUTH);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/clear", CODE_CLEAR);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/reset", CODE_RESET);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/ping", CODE_PING);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/url", CODE_URL);
        mUriMatcher.addURI(AMPACHE_AUTHORITY, "config/close", CODE_CLOSE);

        return true;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_PLAYLIST: {
                return playlistDelete(uri, selection, selectionArgs);
            }
            case CODE_PLAYLIST_ID: {
                return playlistmemberDelete(uri, selection, selectionArgs);
            }
            default: {
                return 0;
            }
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return mediaInsert(uri, values);
            }
            case CODE_ALBUMS: {
                return albumInsert(uri, values);
            }
            case CODE_ARTIST: {
                return artistInsert(uri, values);
            }
            case CODE_PLAYLIST: {
                return playlistInsert(uri, values);
            }
            case CODE_PLAYLIST_ID: {
                return playlistmemberInsert(uri, values);
            }
            case CODE_FAVORITE: {
                return favoriteInsert(uri, values);
            }
            case CODE_VIDEO: {
                return videoInsert(uri, values);
            }
            case CODE_ORDER_AUDIO: {
                return orderAudioInsert(uri, values);
            }
            case CODE_GENRES: {
                return genresInsert(uri, values);
            }
            case CODE_GENRES_ID: {
                return genresmemberInsert(uri, values);
            }
            default: {
                return null;
            }
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_MEDIA: {
                return mediaQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_MEDIA_ID: {
                return mediaIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ARTIST: {
                return artistQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST: {
                return playlistQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PLAYLIST_ID: {
                return playlistmemberQuery(uri, projection, selection,
                        selectionArgs, sortOrder);
            }
            case CODE_FAVORITE: {
                return favoriteQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO: {
                return videoQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_VIDEO_ID: {
                return videoIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ALBUMS: {
                return albumQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRES: {
                return genresQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_GENRESMEMBER: {
                return genresmemberQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO: {
                return orderAudioQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_ORDER_AUDIO_ID: {
                return orderAudioIdQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_DOWNLOAD: {
                return downloadQuery(uri, projection, selection, selectionArgs,
                        sortOrder);
            }
            case CODE_PING: {
                boolean force = false;
                if (selection != null && selection.indexOf("force") != -1) {
                    force = true;
                }
                String key = getAmpacheHelper().ping(force);
                MatrixCursor cursor = null;
                if (key != null && key.length() > 0) {
                    cursor = new MatrixCursor(new String[] {
                            Auth._ID,
                            Auth.AUTH_KEY
                    });
                    cursor.addRow(new Object[] {
                            0, key
                    });
                }
                return cursor;
            }
            case CODE_CLOSE: {
                mDatabaseIndex = -1;
                getAmpacheHelper().clearAuth();
            }
                break;
            case CODE_CLEAR: {
                getAmpacheHelper().clearAuth();
            }
                break;
            case CODE_AUTH: {
                MatrixCursor cursor = new MatrixCursor(new String[] {
                        Auth._ID,
                        Auth.AUTH_URL
                });
                String url = getAmpacheHelper().getAuthrizedURL(selectionArgs[0]);
                cursor.addRow(new Object[] {
                        0, url
                });
                return cursor;
            }
            case CODE_URL: {
                MatrixCursor cursor = new MatrixCursor(new String[] {
                        Auth._ID,
                        Auth.AUTH_URL
                });
                String url = getAmpacheHelper().getMediaAuthrizedURL(
                        selectionArgs[0]);
                cursor.addRow(new Object[] {
                        0, url
                });
                return cursor;
            }
            case CODE_RESET: {
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                try {
                    db.beginTransaction();
                    if (projection == null) {
                        getDBHelper().rebuild(db);
                    } else {
                        getDBHelper().deleteTable(db, projection);
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
            }
        }
        return null;
    }

    private Cursor mediaQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        boolean search_albumsongs = selection != null ? selection
                .indexOf(AudioAlbum.ALBUM_KEY) != -1 : false;
        boolean canUseNetwork = canUseNetwork();
        if (search_albumsongs) {
            String[] sel = selection.split("=");
            String album_key = null;
            for (int i = 0; i < sel.length; i++) {
                if (sel[i].indexOf(AudioMedia.ALBUM_KEY) != -1) {
                    album_key = selectionArgs[i];
                    break;
                }
            }
            if (album_key != null) {
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
                qb.setTables(TableConsts.TBNAME_ALBUM);
                Cursor cur = null;
                try {
                    cur = qb.query(db,
                            new String[] {
                                TableConsts.ALBUM_INIT_FLG
                            },
                            AudioAlbum.ALBUM_KEY + " = ?",
                            new String[] {
                                album_key
                            }, null, null, null);
                    if (cur != null && cur.moveToFirst()) {
                        int n = cur.getColumnIndex(TableConsts.ALBUM_INIT_FLG);
                        if (!cur.isNull(n)) {
                            int flg = cur.getInt(n);
                            if (flg == 1) {
                                SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
                                qb2.setTables(TableConsts.TBNAME_AUDIO);
                                if (!canUseNetwork) {
                                    qb2.appendWhere(TableConsts.AUDIO_CACHE_FILE
                                            + " is not null");
                                }
                                return qb2.query(db, RequestCache.MEDIA_FIELDS,
                                        AudioMedia.ALBUM_KEY + " = ?",
                                        new String[] {
                                            album_key
                                        }, null, null,
                                        sortOrder);
                            }
                        }
                        if (canUseNetwork) {
                            return mCache.mediaQueryAlbum(db, album_key, uri,
                                    null, selection, selectionArgs, sortOrder);
                        }
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                }
            }
            return new MatrixCursor(projection != null ? projection
                    : RequestCache.MEDIA_FIELDS);
        }
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_AUDIO);
        if (!canUseNetwork) {
            qb2.appendWhere(TableConsts.AUDIO_CACHE_FILE + " is not null");
        }
        String limit = null;
        int pos = selection != null ? selection.indexOf("AND LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork) {
                    return mCache.mediaQuery(db, null, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    return new MatrixCursor(RequestCache.MEDIA_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, RequestCache.MEDIA_FIELDS, selection,
                selectionArgs, null, null, sortOrder, limit);
        if (cur != null && cur.getCount() > 0) {
            return cur;
        } else {
            // リミットは最低かけておく
            limit = "0," + mPreference.getString(SystemConsts.PREF_LIMIT, "50");// SystemConsts.DEFAULT_LIST_LIMIT))
            if (canUseNetwork) {
                return mCache.mediaQuery(db, null, selection, selectionArgs,
                        sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.MEDIA_FIELDS);
    }

    private Cursor albumQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        boolean search_id = selection != null ? selection
                .indexOf(AudioAlbum._ID) != -1 : false;
        boolean search_albumkey = selection != null ? selection
                .indexOf(AudioAlbum.ALBUM_KEY) != -1 : false;
        boolean search_artist = selection != null ? selection
                .indexOf(AudioAlbum.ARTIST) != -1 : false;
        boolean canUseNetwork = canUseNetwork();
        if (search_id || search_albumkey || search_artist) {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_ALBUM);
            if (!canUseNetwork) {
                qb.appendWhere(TableConsts.ALBUM_INIT_FLG + " = 1");
            }
            return qb.query(db, projection, selection, selectionArgs, null,
                    null, sortOrder);
        }
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_ALBUM);
        if (!canUseNetwork) {
            qb2.appendWhere(TableConsts.ALBUM_INIT_FLG + " = 1");
        }
        String limit = null;
        int pos = selection != null ? selection.indexOf("LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork) {
                    return mCache.albumQuery(db, projection, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    return new MatrixCursor(RequestCache.ALBUM_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, projection, selection, selectionArgs, null,
                null, sortOrder, limit);
        if (cur != null && cur.getCount() > 0) {
            return cur;
        } else {
            if (limit == null) {
                // リミットは最低かけておく
                limit = "0,20";
            }
            if (canUseNetwork) {
                return mCache.albumQuery(db, projection, selection,
                        selectionArgs, sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.ALBUM_FIELDS);
    }

    private Cursor mediaIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        long id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_AUDIO);
        return qb.query(db, RequestCache.MEDIA_FIELDS, AudioMedia._ID + " = ?",
                new String[] {
                    Long.toString(id)
                }, null, null, sortOrder);
    }

    private Cursor artistQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        boolean search_id = selection != null ? selection
                .indexOf(AudioArtist._ID) != -1 : false;

        boolean search_artistkey = selection != null ? selection
                .indexOf(AudioArtist.ARTIST_KEY) != -1 : false;
        if (search_id || search_artistkey) {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_ARTIST);
            return qb.query(db, projection, selection, selectionArgs, null,
                    null, sortOrder);
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_ARTIST);
        String limit = null;
        int pos = selection != null ? selection.indexOf("LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork()) {
                    return mCache.artistQuery(db, projection, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    return new MatrixCursor(RequestCache.ARTIST_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, projection, selection, selectionArgs, null,
                null, sortOrder, limit);
        if (cur != null && cur.getCount() > 0) {
            return cur;
        } else {
            if (limit == null) {
                // リミットは最低かけておく
                limit = "0,20";
            }
            if (canUseNetwork()) {
                return mCache.artistQuery(db, projection, selection,
                        selectionArgs, sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.ARTIST_FIELDS);
    }

    private Cursor playlistQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        boolean search_id = selection != null ? selection
                .indexOf(AudioPlaylist._ID) != -1 : false;
        boolean search_playlistkey = selection != null ? selection
                .indexOf(AudioPlaylist.PLAYLIST_KEY) != -1 : false;
        if (search_id || search_playlistkey) {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_PLAYLIST);
            return qb.query(db, projection, selection, selectionArgs, null,
                    null, sortOrder);
        }
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_PLAYLIST);
        String limit = null;
        int pos = selection != null ? selection.indexOf("LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork()) {
                    return mCache.playlistQuery(db, projection, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    return new MatrixCursor(RequestCache.PLAYLIST_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, projection, selection, selectionArgs, null,
                null, sortOrder, limit);
        if (cur != null && cur.getCount() > 0) {
            return cur;
        } else {
            if (limit == null) {
                // リミットは最低かけておく
                limit = "0,20";
            }
            if (canUseNetwork()) {
                return mCache.playlistQuery(db, projection, selection,
                        selectionArgs, sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.PLAYLIST_FIELDS);
    }

    String[] playlist_audio_cols = new String[] {
            BaseColumns._ID,
            AudioPlaylistMember.AUDIO_ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    String[] audio_cols = new String[] {
            BaseColumns._ID, AudioMedia.TITLE,
            AudioMedia.TITLE_KEY, AudioMedia.DATA, AudioMedia.DURATION,
            AudioMedia.ARTIST, AudioMedia.ARTIST_KEY, AudioMedia.ALBUM,
            AudioMedia.ALBUM_KEY,
            // AudioMedia.ALBUM_ART,
            AudioMedia.TRACK
    };

    private Cursor playlistmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = null;
        long playlist_id = ContentUris.parseId(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TableConsts.TBNAME_PLAYLIST_AUDIO);
        qb.appendWhere(AudioPlaylistMember.PLAYLIST_ID + " = " + playlist_id);
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();

            // PlaylistMemberからデータを取得し、合致するMedia情報をもとに、返事をかえす
            MatrixCursor ret_cursor = new MatrixCursor(playlist_audio_cols);
            cur = qb.query(db, new String[] {
                    BaseColumns._ID,
                    AudioPlaylistMember.AUDIO_ID,
                    AudioPlaylistMember.PLAY_ORDER
            }, selection, selectionArgs,
                    null, null, sortOrder);
            if (cur != null && cur.moveToFirst()) {
                do {
                    long mediaId = cur.getLong(cur
                            .getColumnIndex(AudioPlaylistMember.AUDIO_ID));
                    Hashtable<String, String> tbl = getMedia(getContext(),
                            audio_cols, mediaId);
                    if (!tbl.isEmpty()) {
                        ret_cursor
                                .addRow(new Object[] {
                                        // Long.parseLong(tbl.get(BaseColumns._ID)),
                                        cur.getLong(cur
                                                .getColumnIndex(BaseColumns._ID)),
                                        tbl.get(AudioMedia._ID),
                                        tbl.get(AudioMedia.TITLE),
                                        tbl.get(AudioMedia.TITLE_KEY),
                                        tbl.get(AudioMedia.DATA),
                                        parseLong(tbl.get(AudioMedia.DURATION)),
                                        tbl.get(AudioMedia.ARTIST),
                                        tbl.get(AudioMedia.ARTIST_KEY),
                                        tbl.get(AudioMedia.ALBUM),
                                        tbl.get(AudioMedia.ALBUM_KEY),
                                        // tbl.get(AudioMedia.ALBUM_ART),
                                        cur.getInt(cur
                                                .getColumnIndex(AudioPlaylistMember.PLAY_ORDER)),
                                });
                    }
                } while (cur.moveToNext());
            }
            else {
                // ないならネットからひろう
                String playlist_key = getPlaylistKey(playlist_id);
                if (playlist_key != null) {
                    SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
                    qb2.setTables(TableConsts.TBNAME_PLAYLIST);
                    Cursor cur2 = null;
                    try {
                        cur2 = qb2.query(db, new String[] {
                                TableConsts.PLAYLIST_INIT_FLG
                        },
                                AudioPlaylist.PLAYLIST_KEY + " = ?", new String[] {
                                    playlist_key
                                }, null, null, null);
                        if (cur2 != null && cur2.moveToFirst()) {
                            int n = cur2.getColumnIndex(TableConsts.PLAYLIST_INIT_FLG);
                            if (!cur2.isNull(n)) {
                                int flg = cur2.getInt(n);
                                if (flg == 1) {
                                    SQLiteQueryBuilder qb3 = new SQLiteQueryBuilder();
                                    qb3.setTables(TableConsts.TBNAME_PLAYLIST_AUDIO);
                                    return qb3.query(db, projection,
                                            AudioPlaylistMember.PLAYLIST_ID + " = ?", new String[] {
                                                playlist_key
                                            }, null, null, sortOrder);
                                }
                            }
                            if (canUseNetwork()) {
                                return mCache.playlistmemberQuery(db, playlist_key, playlist_id);
                            }
                        }
                    } finally {
                        if (cur != null) {
                            cur.close();
                        }
                    }
                }
            }

            return ret_cursor;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private Cursor favoriteQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        MatrixCursor ret = new MatrixCursor(new String[] {
                BaseColumns._ID,
                TableConsts.FAVORITE_POINT
        });
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        Cursor cursor = null;
        try {
            long id = Long.parseLong(selectionArgs[0]);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                cursor = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioMedia._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                cursor = db.query(TableConsts.TBNAME_ALBUM, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioAlbum._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                cursor = db.query(TableConsts.TBNAME_ARTIST, new String[] {
                        BaseColumns._ID, TableConsts.FAVORITE_POINT
                },
                        AudioArtist._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
            }
            if (cursor != null && cursor.moveToFirst()) {
                ret.addRow(new Object[] {
                        id,
                        cursor.getInt(cursor
                                .getColumnIndex(TableConsts.FAVORITE_POINT))
                });
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ret;
    }

    private Cursor videoQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = getDBHelper().getReadableDatabase();
        return db.query(TableConsts.TBNAME_VIDEO, projection, selection,
                selectionArgs, null, null, sortOrder);
    }

    private Cursor videoIdQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        Logger.e("videoIdQuery not supported yet.", new Exception());
        return null;
    }

    private Cursor genresQuery(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        boolean search_id = selection != null ? selection
                .indexOf(AudioGenres._ID) != -1 : false;
        boolean search_genreskey = selection != null ? selection
                .indexOf(AudioGenres.GENRES_KEY) != -1 : false;
        if (search_id || search_genreskey) {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_GENRES);
            return qb.query(db, projection, selection, selectionArgs, null,
                    null, sortOrder);
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_GENRES);
        String limit = null;
        int pos = selection != null ? selection.indexOf("LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork()) {
                    return mCache.genresQuery(db, projection, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    new MatrixCursor(RequestCache.GENRES_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, projection, selection, selectionArgs, null,
                null, sortOrder, limit);
        if (cur != null && cur.getCount() > 0) {
            return cur;
        } else {
            if (limit == null) {
                // リミットは最低かけておく
                limit = "0,20";
            }
            if (canUseNetwork()) {
                return mCache.genresQuery(db, projection, selection,
                        selectionArgs, sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.GENRES_FIELDS);
    }

    private Cursor genresmemberQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {

        String[] sel = selection.split("=");
        String genres_key = null;
        for (int i = 0; i < sel.length; i++) {
            if (sel[i].indexOf(AudioGenresMember.GENRE_ID) != -1) {
                genres_key = selectionArgs[i];
                break;
            }
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        SQLiteQueryBuilder qb2 = new SQLiteQueryBuilder();
        qb2.setTables(TableConsts.TBNAME_AUDIO);
        String limit = null;

        StringBuilder where = new StringBuilder();
        where.append(TableConsts.GENRES_TAGS).append(
                " like '%/" + genres_key + "/%'");

        int pos = selection != null ? selection.indexOf("AND LIMIT") : -1;
        if (pos != -1) {
            // 途中から取得
            String offset = selectionArgs[selectionArgs.length - 1];
            String size = selectionArgs[selectionArgs.length - 2];
            selection = selection.substring(0, pos);
            if (Build.VERSION.SDK_INT < 9) {
                String[] tmp = new String[selectionArgs.length - 2];
                for (int i = 0; i < tmp.length; i++) {
                    tmp[i] = selectionArgs[i];
                }
                selectionArgs = tmp;
            } else {
                selectionArgs = ArrayUtils.fastArrayCopy(selectionArgs,
                        selectionArgs.length - 2);
            }
            if (!offset.equals("0")) {
                limit = offset + "," + size;
                if (canUseNetwork()) {
                    return mCache.genresmemberQuery(db, null, selection,
                            selectionArgs, sortOrder, limit);
                } else {
                    new MatrixCursor(RequestCache.MEDIA_FIELDS);
                }
            }
        }
        // 最初から取得
        Cursor cur = qb2.query(db, RequestCache.MEDIA_FIELDS, where.toString(),
                null, null, null, sortOrder, null);
        if (cur.getCount() > 0) {
            return cur;
        } else {
            if (limit == null) {
                // リミットは最低かけておく
                limit = "0,20";
            }
            if (canUseNetwork()) {
                return mCache.genresmemberQuery(db, null, selection,
                        selectionArgs, sortOrder, limit);
            }
        }
        return new MatrixCursor(RequestCache.MEDIA_FIELDS);
    }

    private Cursor orderAudioQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        Logger.e("orderAudioQuery", new Exception());
        return null;
    }

    private Cursor orderAudioIdQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        Logger.e("orderAudioIdQuery", new Exception());
        return null;
    }

    private Cursor downloadQuery(Uri uri, String[] projection,
            String selection, String[] selectionArgs, String sortOrder) {
        Logger.e("downloadQuery", new Exception());
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        int match = mUriMatcher.match(uri);
        switch (match) {
            case CODE_PLAYLIST: {
                return playlistUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_PLAYLISTMEMBER: {
                return playlistmemberUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_FAVORITE: {
                return favoriteUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_MEDIA_ID: {
                return mediaIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ALBUMS_ID: {
                return albumIdUpdate(uri, values, selection, selectionArgs);
            }
            case CODE_ARTIST_ID: {
                return artistIdUpdate(uri, values, selection, selectionArgs);
            }
            default: {
                return 0;
            }
        }
    }

    private int artistIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            // Artist Update
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getWritableDatabase();
                Cursor cur = null;
                try {
                    db.beginTransaction();
                    cur = db.query(TableConsts.TBNAME_ARTIST,
                            new String[] {
                                AudioArtist.ARTIST
                            },
                            AudioArtist._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            }, null, null, null);
                    if (cur != null && cur.moveToFirst()) {
                        byte[] bufs = getBytes(cur.getString(cur
                                .getColumnIndex(AudioArtist.ARTIST)));
                        if (bufs != null && bufs.length > 0) {
                            ContentValues updateValues = new ContentValues();
                            updateValues.put(AudioMedia.ARTIST, new String(bufs, enc));

                            int ret = db.update(TableConsts.TBNAME_ARTIST, updateValues,
                                    AudioMedia._ID + " = ?",
                                    new String[] {
                                        Long.toString(id)
                                    });
                            db.setTransactionSuccessful();
                            return ret;
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    db.endTransaction();
                    getContext().getContentResolver().notifyChange(
                            ARTIST_CONTENT_URI, null);
                }
            }
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ARTIST, values, AudioArtist._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });

                cur = db.query(TableConsts.TBNAME_ARTIST,
                        new String[] {
                            AudioArtist.ARTIST_KEY
                        },
                        AudioArtist._ID + " = ?",
                        new String[] {
                            Long.toString(id)
                        }, null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String artistKey = cur.getString(cur
                            .getColumnIndex(AudioArtist.ARTIST_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ARTIST_INIT_FLG)
                            && values.getAsInteger(TableConsts.ARTIST_INIT_FLG) == 0) {
                        // Mediaのキャッシュファイルを削除
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioArtist.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ARTIST_KEY + " = ?",
                                new String[] {
                                    artistKey
                                });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
                getContext().getContentResolver().notifyChange(
                        MEDIA_CONTENT_URI, null);
                getContext().getContentResolver().notifyChange(
                        ARTIST_CONTENT_URI, null);
            }
        }
    }

    private int playlistmemberUpdate(Uri uri, ContentValues values,
            String selection, String[] selectionArgs) {
        SQLiteDatabase db = null;
        Cursor cur = null;
        try {
            db = getDBHelper().getWritableDatabase();
            cur = db.query(TableConsts.TBNAME_PLAYLIST_AUDIO,
                    new String[] {
                        MediaConsts.AudioPlaylistMember.AUDIO_ID
                    },
                    selection, selectionArgs, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                return db.update(TableConsts.TBNAME_PLAYLIST_AUDIO, values,
                        selection, selectionArgs);
            } else {
                long id = db.insert(TableConsts.TBNAME_PLAYLIST_AUDIO, null,
                        values);
                return 1;
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    private int playlistUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        return db.update(TableConsts.TBNAME_PLAYLIST, values, selection,
                selectionArgs);
    }

    private int albumIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String albumkey = getAlbumKey(id);
                if (albumkey != null) {
                    String enc = values.getAsString(AudioMedia.ENCODING);
                    Cursor cur = getContext().getContentResolver().query(
                            MediaConsts.MEDIA_CONTENT_URI,
                            new String[] {
                                    AudioMedia._ID, AudioMedia.TITLE,
                                    AudioMedia.ALBUM, AudioMedia.ARTIST
                            },
                            AudioMedia.ALBUM_KEY + " = ?",
                            new String[] {
                                albumkey
                            }, null);
                    if (cur != null && cur.moveToFirst()) {
                        return setId3Tag(cur, enc);
                    }
                }
            }
            return 0;
        } else {
            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            Cursor cur = null;
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_ALBUM, values, AudioAlbum._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });

                // 関連する音楽の更新
                cur = db.query(TableConsts.TBNAME_ALBUM,
                        new String[] {
                            AudioAlbum.ALBUM_KEY
                        }, AudioAlbum._ID
                                + " = ?", new String[] {
                            Long.toString(id)
                        },
                        null, null, null);
                if (cur != null && cur.moveToFirst()) {
                    String albumKey = cur.getString(cur
                            .getColumnIndex(AudioAlbum.ALBUM_KEY));
                    ContentValues audioValues = new ContentValues();
                    if (values.containsKey(TableConsts.ALBUM_INIT_FLG)
                            && values.getAsInteger(TableConsts.ALBUM_INIT_FLG) == 0) {
                        // Albumの初期化フラグをOff
                        audioValues.put(TableConsts.AUDIO_CACHE_FILE,
                                (String) null);
                    } else {
                        if (values.containsKey(AudioAlbum.ALBUM)) {
                            audioValues.put(AudioMedia.ALBUM,
                                    values.getAsString(AudioAlbum.ALBUM));
                        }
                        if (values.containsKey(AudioAlbum.ARTIST)) {
                            audioValues.put(AudioMedia.ARTIST,
                                    values.getAsString(AudioAlbum.ARTIST));
                        }
                    }
                    if (audioValues.size() > 0) {
                        db.update(TableConsts.TBNAME_AUDIO, audioValues,
                                AudioMedia.ALBUM_KEY + " = ?",
                                new String[] {
                                    albumKey
                                });
                    }
                }

                db.setTransactionSuccessful();
                return 1;
            } finally {
                if (cur != null) {
                    cur.close();
                }
                db.endTransaction();
            }
        }
    }

    private int mediaIdUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        if (values != null && values.containsKey(AudioMedia.ENCODING)) {
            long id = ContentUris.parseId(uri);
            if (id >= 0) {
                String enc = values.getAsString(AudioMedia.ENCODING);
                SQLiteDatabase db = getDBHelper().getReadableDatabase();
                Cursor cur = null;
                try {
                    cur = db.query(TableConsts.TBNAME_AUDIO, new String[] {
                            AudioMedia._ID, AudioMedia.TITLE, AudioMedia.ALBUM,
                            AudioMedia.ARTIST
                    }, "_ID = ?",
                            new String[] {
                                Long.toString(id)
                            }, null, null,
                            null);
                    if (cur != null && cur.moveToFirst()) {
                        return setId3Tag(cur, enc);
                    }
                } finally {
                    if (cur != null) {
                        cur.close();
                    }
                    getContext().getContentResolver().notifyChange(
                            MEDIA_CONTENT_URI, null);
                }
            }
            return 0;
        } else {

            SQLiteDatabase db = getDBHelper().getWritableDatabase();
            try {
                long id = ContentUris.parseId(uri);
                db.beginTransaction();
                db.update(TableConsts.TBNAME_AUDIO, values, AudioMedia._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
                db.setTransactionSuccessful();
                return 1;
            } finally {
                db.endTransaction();
            }
        }
    }

    public Uri favoriteInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = values.getAsLong(FAVORITE_ID);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = values.getAsString(FAVORITE_TYPE);
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            }
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MediaConsts.FAVORITE_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    public int favoriteUpdate(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = Long.parseLong(selectionArgs[0]);
            int point = values.getAsInteger(FAVORITE_POINT);
            String type = selectionArgs[1];
            if (type.equals(FAVORITE_TYPE_SONG)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_AUDIO, s_values, AudioMedia._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ALBUM)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ALBUM, s_values, AudioAlbum._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            } else if (type.equals(FAVORITE_TYPE_ARTIST)) {
                ContentValues s_values = new ContentValues();
                s_values.put(TableConsts.FAVORITE_POINT, point);
                db.update(TableConsts.TBNAME_ARTIST, s_values, AudioArtist._ID
                        + " = ?", new String[] {
                    Long.toString(id)
                });
            }
            db.setTransactionSuccessful();
            return 1;
        } finally {
            db.endTransaction();
        }
    }

    public Uri mediaInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_AUDIO, null, values);
            db.setTransactionSuccessful();
            return ContentUris
                    .withAppendedId(MediaConsts.MEDIA_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri albumInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ALBUM, null, values);
            db.setTransactionSuccessful();
            return ContentUris
                    .withAppendedId(MediaConsts.ALBUM_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri artistInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ARTIST, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MediaConsts.ARTIST_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri playlistInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = db.insert(TableConsts.TBNAME_PLAYLIST, null, values);
        return ContentUris.withAppendedId(PLAYLIST_CONTENT_URI, id);
    }

    public Uri playlistmemberInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        long id = ContentUris.parseId(uri);
        long media_id = values.getAsLong(AudioPlaylistMember.AUDIO_ID);
        int order = values.getAsInteger(AudioPlaylistMember.PLAY_ORDER);

        Cursor cur = null;
        boolean hasTransaction = false;
        try {
            SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
            qb.setTables(TableConsts.TBNAME_AUDIO);
            cur = qb.query(db, RequestCache.MEDIA_FIELDS, AudioMedia._ID + " = ?",
                    new String[] {
                        Long.toString(media_id)
                    }, null, null, null);
            if (cur != null && cur.moveToFirst()) {
                String media_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.MEDIA_KEY));
                String title = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE));
                String title_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE_KEY));
                long duration = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DURATION));
                String artist = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST));
                String artist_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST_KEY));
                String album = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM));
                String album_key = cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM_KEY));
                String data = cur
                        .getString(cur.getColumnIndex(AudioMedia.DATA));
                int tarck = cur.getInt(cur.getColumnIndex(AudioMedia.TRACK));
                long date_added = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_ADDED));
                long date_modified = cur.getLong(cur
                        .getColumnIndex(AudioMedia.DATE_MODIFIED));
                String tags = cur.getString(cur
                        .getColumnIndex(TableConsts.GENRES_TAGS));
                String year = cur.getString(cur
                        .getColumnIndex(AudioPlaylistMember.YEAR));
                int point = cur.getInt(cur
                        .getColumnIndex(TableConsts.FAVORITE_POINT));

                db.beginTransaction();
                hasTransaction = true;
                ContentValues val = new ContentValues();
                val.put(TableConsts.PLAYLIST_INIT_FLG, 1);
                db.update(TableConsts.TBNAME_PLAYLIST, val, AudioPlaylist._ID
                        + "=?", new String[] {
                    Long.toString(id)
                });

                ContentValues dvalues_pl = new ContentValues();
                dvalues_pl.put(AudioPlaylistMember._ID, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAY_ORDER, order);
                dvalues_pl.put(AudioPlaylistMember.AUDIO_ID, media_id);
                dvalues_pl.put(AudioPlaylistMember.MEDIA_KEY, media_key);
                dvalues_pl.put(AudioPlaylistMember.PLAYLIST_ID, id);
                dvalues_pl.put(AudioPlaylistMember.TITLE, title);
                dvalues_pl.put(AudioPlaylistMember.TITLE_KEY, title_key);
                dvalues_pl.put(AudioPlaylistMember.DURATION, duration);
                dvalues_pl.put(AudioPlaylistMember.ARTIST, artist);
                dvalues_pl.put(AudioPlaylistMember.ARTIST_KEY, artist_key);
                dvalues_pl.put(AudioPlaylistMember.ALBUM, album);
                dvalues_pl.put(AudioPlaylistMember.ALBUM_KEY, album_key);
                dvalues_pl.put(AudioPlaylistMember.DATA, data);
                dvalues_pl.put(AudioPlaylistMember.TRACK, tarck);
                dvalues_pl.put(AudioPlaylistMember.DATE_ADDED, date_added);
                dvalues_pl
                        .put(AudioPlaylistMember.DATE_MODIFIED, date_modified);
                dvalues_pl.put(TableConsts.GENRES_TAGS, tags);
                dvalues_pl.put(AudioPlaylistMember.YEAR, year);
                dvalues_pl.put(TableConsts.FAVORITE_POINT, point);
                long ret_id = db.replace(TableConsts.TBNAME_PLAYLIST_AUDIO,
                        null, dvalues_pl);

                db.setTransactionSuccessful();
                return ContentUris.withAppendedId(
                        MediaConsts.PLAYLIST_MEMBER_CONTENT_URI, ret_id);
            }
        } finally {
            if (cur != null) {
                cur.close();
            }
            if (hasTransaction) {
                db.endTransaction();
            }
        }
        return null;
    }

    public Uri genresInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_GENRES, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(MediaConsts.GENRES_CONTENT_URI,
                    id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri genresmemberInsert(Uri uri, ContentValues values) {
        // SQLiteDatabase db = getDBHelper().getWritableDatabase();

        // long id = db.insert(TableConsts.TBNAME_GENRES_AUDIO, null, values);
        // return
        // ContentUris.withAppendedId(MediaConsts.GENRES_MEMBER_CONTENT_URI,
        // id);
        return null;
    }

    public Uri orderAudioInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_ORDERLIST, null, values);
            db.setTransactionSuccessful();
            return ContentUris.withAppendedId(
                    MediaConsts.PLAYORDER_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    public Uri videoInsert(Uri uri, ContentValues values) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        try {
            db.beginTransaction();
            long id = db.insert(TableConsts.TBNAME_VIDEO, null, values);
            db.setTransactionSuccessful();
            return ContentUris
                    .withAppendedId(MediaConsts.VIDEO_CONTENT_URI, id);
        } finally {
            db.endTransaction();
        }
    }

    private int playlistDelete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        int ret = db.delete(TableConsts.TBNAME_PLAYLIST, selection,
                selectionArgs);
        // リストのメンバーも削除する
        db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                MediaConsts.AudioPlaylistMember.PLAYLIST_ID + " = ?",
                selectionArgs);

        return ret;
    }

    private int playlistmemberDelete(Uri uri, String selection,
            String[] selectionArgs) {
        long id = ContentUris.parseId(uri);

        StringBuilder where = new StringBuilder();
        where.append(MediaConsts.AudioPlaylistMember.PLAYLIST_ID).append(" = ")
                .append(Long.toString(id));
        if (selection != null && selection.length() > 0) {
            where.append(" AND ( ").append(selection).append(" )");
        }

        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        int ret = db.delete(TableConsts.TBNAME_PLAYLIST_AUDIO,
                where.toString(),
                selectionArgs);
        Logger.d("ret=" + ret);
        return ret;
    }

    public Hashtable<String, String> getMedia(Context contxt,
            String[] projection, long mediaId) {
        Hashtable<String, String> tbl = new Hashtable<String, String>();
        Cursor cur = null;
        try {
            String[] prj = projection;
            if (prj == null) {
                prj = new String[] {
                        AudioMedia.ALBUM, AudioMedia.ALBUM_KEY,
                        AudioMedia.ARTIST, AudioMedia.TITLE,
                        AudioMedia.DURATION
                };
            }
            cur = contxt.getContentResolver().query(
                    ContentUris.withAppendedId(MEDIA_CONTENT_URI, mediaId),
                    prj, MediaConsts.AudioMedia._ID + " = ?", null, null);
            if (cur != null && cur.moveToFirst()) {
                for (int i = 0; i < prj.length; i++) {
                    String value = cur.getString(cur.getColumnIndex(prj[i]));
                    if (value != null) {
                        tbl.put(prj[i], value);
                    }
                }
            }

            return tbl;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }

    public static long parseLong(String text) {
        if (text != null && text.length() > 0) {
            try {
                return Long.parseLong(text);
            } catch (Exception e) {

            }
        }
        return 0;
    }

    private byte[] getBytes(String s) {
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            if (c >= 0x100) {
                // Byte列になっていない
                return null;
            }
            bytes[i] = (byte) c;
        }
        return bytes;
    }

    private int setId3Tag(Cursor cur, String enc) {
        SQLiteDatabase db = getDBHelper().getWritableDatabase();
        boolean hasTransaction = false;
        int count = 0;
        try {
            do {
                ContentValues updateValues = new ContentValues();
                byte[] bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.TITLE)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.TITLE, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ARTIST)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ARTIST, new String(bufs, enc));
                }
                bufs = getBytes(cur.getString(cur
                        .getColumnIndex(AudioMedia.ALBUM)));
                if (bufs != null) {
                    updateValues.put(AudioMedia.ALBUM, new String(bufs, enc));
                }
                if (updateValues.size() > 0) {
                    if (hasTransaction != true) {
                        hasTransaction = true;
                        db.beginTransaction();
                    }
                    long id = cur.getLong(cur.getColumnIndex(AudioMedia._ID));
                    db.update(TableConsts.TBNAME_AUDIO, updateValues,
                            AudioMedia._ID + " = ?",
                            new String[] {
                                Long.toString(id)
                            });
                    count++;
                }
            } while (cur.moveToNext());
            return count;
        } catch (UnsupportedEncodingException e) {
        } finally {
            if (hasTransaction) {
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }
        return count;
    }

    private String getAlbumKey(long id) {
        Cursor cur = null;
        try {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            cur = db.query(TableConsts.TBNAME_ALBUM,
                    new String[] {
                        AudioAlbum.ALBUM_KEY
                    }, AudioAlbum._ID
                            + " = ?", new String[] {
                        Long.toString(id)
                    }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                String albumKey = cur.getString(cur
                        .getColumnIndex(AudioAlbum.ALBUM_KEY));
                return albumKey;
            }

        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return null;
    }

    private String getPlaylistKey(long id) {
        Cursor cur = null;
        try {
            SQLiteDatabase db = getDBHelper().getReadableDatabase();
            cur = db.query(TableConsts.TBNAME_PLAYLIST,
                    new String[] {
                        AudioPlaylist.PLAYLIST_KEY
                    }, AudioPlaylist._ID
                            + " = ?", new String[] {
                        Long.toString(id)
                    }, null,
                    null, null);
            if (cur != null && cur.moveToFirst()) {
                String playlistKey = cur.getString(cur
                        .getColumnIndex(AudioPlaylist.PLAYLIST_KEY));
                return playlistKey;
            }

        } finally {
            if (cur != null) {
                cur.close();
            }
        }
        return null;
    }
}
