package jp.co.kayo.android.localplayer.ds.ampache.util;

import java.util.Arrays;

public class ArrayUtils {
    public static String[] fastArrayCopy(String[] selectionArgs, int length) {
        return Arrays.copyOf(selectionArgs, length);
    }
}
