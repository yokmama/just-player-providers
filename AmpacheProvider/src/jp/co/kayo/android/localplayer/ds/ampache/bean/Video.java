package jp.co.kayo.android.localplayer.ds.ampache.bean;

import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public class Video {
    private String mId;
    private String mTitle;
    private String mMime;
    private String mResolution;
    private String mSize;
    private String mUrl;

    public void setValue(String server, String tag, XmlPullParser parser) {
        if (tag.equals("title")) {
            setTitle(XMLUtils.getTextValue(parser));
        } else if (tag.equals("mime")) {
            setMime(XMLUtils.getTextValue(parser));
        } else if (tag.equals("resolution")) {
            setResolution(XMLUtils.getTextValue(parser));
        } else if (tag.equals("size")) {
            setSize(XMLUtils.getTextValue(parser));
        } else if (tag.equals("url")) {
            String u = XMLUtils.getTextValue(parser);
            String s1 = server.replaceFirst("[^/]*//[^/]+", "");
            String s2 = u.replaceFirst("[^/]*//[^/]+", "");
            setUrl(s1.equals("/") ? s2 : s2.replace(s1, ""));
            /*
             * String https = null; if (server != null &&
             * server.indexOf("https:") != -1) { https =
             * server.replace("https:", "http:"); } if (server == null) {
             * setUrl(u); } else if (https != null) { if (u.indexOf("http:") !=
             * -1) { setUrl(u.replaceFirst(https, "")); } else {
             * setUrl(u.replaceFirst(server, "")); } } else {
             * setUrl(u.replaceFirst(server, "")); }
             */
        }
    }

    public void parse(String server, Node node) {
        setId(((Element) node).getAttribute("id"));

        /*
         * String https = null; if (server != null && server.indexOf("https:")
         * != -1) { https = server.replace("https:", "http:"); }
         */

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("title")) {
                setTitle(XMLUtils.getTextValue(child));
            } else if (tag.equals("mime")) {
                setMime(XMLUtils.getTextValue(child));
            } else if (tag.equals("resolution")) {
                setResolution(XMLUtils.getTextValue(child));
            } else if (tag.equals("size")) {
                setSize(XMLUtils.getTextValue(child));
            } else if (tag.equals("url")) {
                String u = XMLUtils.getTextValue(child);
                String s1 = server.replaceFirst("[^/]*//[^/]+", "");
                String s2 = u.replaceFirst("[^/]*//[^/]+", "");
                setUrl(s1.equals("/") ? s2 : s2.replace(s1, ""));
                /*
                 * if (server == null) { setUrl(u); } else if (https != null) {
                 * if (u.indexOf("http:") != -1) { setUrl(u.replaceFirst(https,
                 * "")); } else { setUrl(u.replaceFirst(server, "")); } } else {
                 * setUrl(u.replaceFirst(server, "")); }
                 */
            }
        }
    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setMime(String mime) {
        mMime = mime;
    }

    public String getMime() {
        return mMime;
    }

    public void setResolution(String resolution) {
        mResolution = resolution;
    }

    public String getResolution() {
        return mResolution;
    }

    public void setSize(String size) {
        mSize = size;
    }

    public String getSize() {
        return mSize;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }
}
