package jp.co.kayo.android.localplayer.ds.ampache.connect;

import jp.co.kayo.android.localplayer.ds.ampache.AmpacheHelper;
import jp.co.kayo.android.localplayer.ds.ampache.AmpacheServer;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class AmpacheConnector extends ProgressTask {
    public static final int EVT_AMPACHE_ABORT = 1002;
    public static final int EVT_SET_SERVER_INF = 1004;

    private Handler mHandler;
    private Context mContext;
    private ServerConfig mConfig;

    public AmpacheConnector(Context activityContext, Handler handler,
            ServerConfig config) {
        super(activityContext);
        mContext = activityContext;
        mHandler = handler;
        mConfig = config;
    }

    @Override
    public Object doInBackground(Object... params) {
        // TODO: connection
        AmpacheHelper helper = new AmpacheHelper(mContext);
        AmpacheServer srv = helper.getServerInfo(mContext, mConfig.host,
                mConfig.user, mConfig.pass);

        if (!this.isCancelled() && srv != null) {
            Message msg = Message.obtain();
            msg.what = EVT_SET_SERVER_INF;
            msg.obj = new Object[] { srv, mConfig };
            mHandler.sendMessage(msg);
        }

        return null;
    }

    @Override
    public void onPostExecute(Object result) {
        super.onPostExecute(result);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        mHandler.sendEmptyMessage(EVT_AMPACHE_ABORT);
    }
}