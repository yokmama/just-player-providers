package jp.co.kayo.android.localplayer.ds.ampache.bean;

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts;
import jp.co.kayo.android.localplayer.ds.ampache.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.ds.ampache.util.ValueRetriever;
import jp.co.kayo.android.localplayer.ds.ampache.util.XMLUtils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;

public class Artist {
    private String mId;
    private String mName;
    private int mNumAlbum;
    private int mNumSong;
    private ArrayList<Tag> mTags = new ArrayList<Tag>();
    private String mPreciserating;
    private int mRating;

    public Artist() {
    }

    public void setValue(String server, String tag, XmlPullParser parser) {
        if (tag.equals("name")) {
            setName(XMLUtils.getTextValue(parser));
        } else if (tag.equals("albums")) {
            setNumAlbums(getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("songs")) {
            setNumSong(getInt(XMLUtils.getTextValue(parser)));
        } else if (tag.equals("tag")) {
            Tag tagvalue = new Tag();
            String id = XMLUtils.getAttributeValue(parser, "id");
            String count = XMLUtils.getAttributeValue(parser, "count");
            tagvalue.setId(id);
            tagvalue.setName(XMLUtils.getTextValue(parser));
            tagvalue.setSortOrder(ValueRetriever.getInt(count));
            getTags().add(tagvalue);
        } else if (tag.equals("preciserating")) {
            setPreciserating(XMLUtils.getTextValue(parser));
        } else if (tag.equals("rating")) {
            setRating(ValueRetriever.getInt(XMLUtils.getTextValue(parser)));
        }
    }

    public void parse(Node node) {
        setId(((Element) node).getAttribute("id"));

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);
            String tag = child.getNodeName();
            if (tag.equals("name")) {
                setName(XMLUtils.getTextValue(child));
            } else if (tag.equals("albums")) {
                setNumAlbums(getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("songs")) {
                setNumSong(getInt(XMLUtils.getTextValue(child)));
            } else if (tag.equals("tag")) {
                Tag tagvalue = new Tag();
                tagvalue.setId(((Element) child).getAttribute("id"));
                tagvalue.setName(XMLUtils.getTextValue(child));
                tagvalue.setSortOrder(ValueRetriever.getInt(((Element) child)
                        .getAttribute("count")));
                getTags().add(tagvalue);
            } else if (tag.equals("preciserating")) {
                setPreciserating(XMLUtils.getTextValue(child));
            } else if (tag.equals("rating")) {
                setRating(ValueRetriever.getInt(XMLUtils.getTextValue(child)));
            }
        }
    }

    int getInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {

        }
        return 0;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setNumAlbums(int albums) {
        this.mNumAlbum = albums;
    }

    public int getNumAlbums() {
        return mNumAlbum;
    }

    public void setNumSong(int songs) {
        this.mNumSong = songs;
    }

    public int getNumSong() {
        return mNumSong;
    }

    public void setPreciserating(String preciserating) {
        mPreciserating = preciserating;
    }

    public void setTags(ArrayList<Tag> tags) {
        mTags = tags;
    }

    public ArrayList<Tag> getTags() {
        return mTags;
    }

    public String getPreciserating() {
        return mPreciserating;
    }

    public void setRating(int rating) {
        mRating = rating;
    }

    public int getRating() {
        return mRating;
    }
}
