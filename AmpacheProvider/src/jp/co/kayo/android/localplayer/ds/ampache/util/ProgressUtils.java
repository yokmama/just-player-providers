package jp.co.kayo.android.localplayer.ds.ampache.util;

import jp.co.kayo.android.localplayer.ds.ampache.consts.SystemConsts;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

public class ProgressUtils {
    private static Handler mHandler;

    public static void setHandler(Handler handler) {
        ProgressUtils.mHandler = handler;
    }

    public static void setVisible(boolean b) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_VISIBLE;
            msg.arg1 = b ? 1 : 0;
            mHandler.sendMessage(msg);
        }
    }

    public static void startProgress(int max) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_START;
            msg.arg1 = max;
            mHandler.sendMessage(msg);
        }
    }

    public static void stopProgress() {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_STOP;
            mHandler.sendMessage(msg);
        }
    }

    public static void moveProgress(int pos) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_MOVE;
            msg.arg1 = pos;
            mHandler.sendMessage(msg);
        }
    }

    public static void setProgress(int pos, int max) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_SET;
            msg.arg1 = pos;
            msg.arg2 = max;
            mHandler.sendMessage(msg);
        }
    }

    public static void toast(String s) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_TOAST;
            msg.obj = s;
            mHandler.sendMessage(msg);
        }
    }

    public static void title(String s) {
        if (mHandler != null) {
            Message msg = Message.obtain();
            msg.what = SystemConsts.EVT_PEOGRESS_TITLE;
            msg.obj = s;
            mHandler.sendMessage(msg);
        }
    }
}
