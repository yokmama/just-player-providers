package jp.co.kayo.android.localplayer.ds.ampache.consts;

public interface SystemConsts {
    public static final String DEFAULT_HOST = "";
    public static final String DEFAULT_USID = "";
    public static final String DEFAULT_PSWD = "";
    public static final int DEFAULT_INDEX = -1;
    public static final String DEFAULT_LIST_LIMIT = "50";

    public static final String CONTENTSKEY_ALBUM = "Album.";
    public static final String CONTENTSKEY_ARTIST = "Artist.";
    public static final String CONTENTSKEY_PLAYLIST = "Playlist.";
    public static final String CONTENTSKEY_MEDIA = "Media.";
    public static final String CONTENTSKEY_GENRES = "Genres.";
    public static final String CONTENTSKEY_PLAYALBUM = "PlayAlbum.";

    public static final String PREF_CONTENTURI = "key_current_uri";
    public static final String PREF_REPEATFLG = "key_repeat_flg";
    public static final String PREF_SHUFFLEFLG = "key_shuffle_flg";
    public static final String PREF_REGISTDATE = "pref.registdate";
    public static final String PREF_FIRST_YEAR = "pref.first_year";
    public static final String PREF_LAST_YEAR = "pref.last_year";
    public static final String PREF_BRANKFILTER = "pref.brankfilter";
    public static final String PREF_LIMIT = "pref.limit";

    public static final String KEY_OAUTH_TOKEN = "oauth_token";
    public static final String KEY_OAUTH_TOKEN_SECRET = "oauth_token_secret";
    public static final String CONSUMER_KEY = "JxljN0afbQlS9sjKvwrxnA";
    public static final String CONSUMER_SECRET = "T3xg4XvdpLsZpOUxZXjhYxFXYsifqXg7u0zKaqknY";
    public static final String CALLBACK_URL = "http://twitter.com/oauth/authorize";
    public static final String KEY_OAUTH_VERIFIER = "oauth_verifier";

    public static final String TAB_ALBUMS = "Albums";
    public static final String TAB_ARTIST = "Artists";
    public static final String TAB_GENRES = "Genres";
    public static final String TAB_PLAYLIST = "Playlist";
    public static final String TAB_FAVORITE = "Favorite";
    public static final String TAB_VIDEOS = "Videos";
    public static final String TAB_ORDER = "Order";
    public static final String TAB_MEDIA = "Media";

    public static final String TAG_CACHE = "tag_cache";
    public static final String TAG_TITLE = "tag_title";

    public static final String TAG_ALBUM = "tag_album";
    public static final String TAG_ARTIST = "tag_artist";
    public static final String TAG_GENRES = "tag_genres";
    public static final String TAG_PLAYLIST = "tag_playlist";
    public static final String TAG_FAVORITE = "tag_favorite";
    public static final String TAG_VIDEO = "tag_video";
    public static final String TAG_ORDER = "tag_order";
    public static final String TAG_MEDIA = "tag_media";
    public static final String[] TAG_ALL = { TAG_ALBUM, TAG_ARTIST, TAG_GENRES,
            TAG_PLAYLIST, TAG_FAVORITE, TAG_VIDEO, TAG_ORDER, TAG_MEDIA };

    public static final int EVT_ADVIEW_VISIVLE = 1;
    public static final int EVT_ADVIEW_INVISIVLE = 2;
    public static final int EVT_PEOGRESS_VISIBLE = 3;
    public static final int EVT_PEOGRESS_START = 4;
    public static final int EVT_PEOGRESS_STOP = 5;
    public static final int EVT_PEOGRESS_MOVE = 6;
    public static final int EVT_PEOGRESS_SET = 7;
    public static final int EVT_PEOGRESS_TOAST = 8;
    public static final int EVT_LIST_SETCURSOR = 9;
    public static final int EVT_PEOGRESS_TITLE = 10;

    public static final int REQUEST_ALBUMART = 0;
    public static final int REQUEST_TWEET = 1;
    public static final int REQUEST_CONFIG = 2;
    public static final int REQUEST_AMPACHE = 3;
    public static final int REQUEST_DROPBOX = 4;
    public static final int REQUEST_PLAYVIEW = 5;

    public static final int RESULT_NONE = 1 << 0;
    public static final int RESULT_CLEARLIST = 1 << 1;
    public static final int RESULT_INVALIDATE = 1 << 2;
}
